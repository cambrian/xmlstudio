// IceRichEditView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceRichEditView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceRichEditView

IMPLEMENT_DYNCREATE(CIceRichEditView, CRichEditView)

CIceRichEditView::CIceRichEditView()
{
}

CIceRichEditView::~CIceRichEditView()
{
}

BEGIN_MESSAGE_MAP(CIceRichEditView, CRichEditView)
	//{{AFX_MSG_MAP(CIceRichEditView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceRichEditView diagnostics

#ifdef _DEBUG
void CIceRichEditView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CIceRichEditView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIceRichEditView message handlers
