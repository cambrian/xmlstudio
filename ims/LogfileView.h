#if !defined(AFX_LOGFILEVIEW_H__1FA13984_5304_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_LOGFILEVIEW_H__1FA13984_5304_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// LogfileView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogfileView view

class CLogfileView : public CRichEditView
{
protected: // create from serialization only
	CLogfileView();
	DECLARE_DYNCREATE(CLogfileView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogfileView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLogfileView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CLogfileView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGFILEVIEW_H__1FA13984_5304_11D2_852E_00A024E0E339__INCLUDED_)
