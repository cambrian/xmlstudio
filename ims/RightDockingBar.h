#if !defined(AFX_RIGHTDOCKINGBAR_H__73C694C0_557E_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_RIGHTDOCKINGBAR_H__73C694C0_557E_11D2_852E_00A024E0E339__INCLUDED_

#include "IceDockingSingleView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RightDockingBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRightDockingBar window

class CRightDockingBar : public CIceDockingSingleView
{
// Construction
public:
	CRightDockingBar();

// Attributes
public:

// Operations
public:

// Overridables
    virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRightDockingBar)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRightDockingBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CRightDockingBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RIGHTDOCKINGBAR_H__73C694C0_557E_11D2_852E_00A024E0E339__INCLUDED_)
