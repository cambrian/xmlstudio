// ScriptView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ScriptView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CScriptView

IMPLEMENT_DYNCREATE(CScriptView, CIceShellView)

CScriptView::CScriptView()
{
}

CScriptView::~CScriptView()
{
}

BEGIN_MESSAGE_MAP(CScriptView, CIceShellView)
	//{{AFX_MSG_MAP(CScriptView)
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScriptView diagnostics

#ifdef _DEBUG
void CScriptView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CScriptView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CScriptView message handlers

void CScriptView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	// TODO:
	// if( == VK_ENTER) or end of program (^J ? or something like that)
	// execute the command...

	CIceShellView::OnChar(nChar, nRepCnt, nFlags);
}
