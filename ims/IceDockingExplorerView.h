#if !defined(AFX_ICEDOCKINGEXPLORERVIEW_H__F07F67E0_5528_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEDOCKINGEXPLORERVIEW_H__F07F67E0_5528_11D2_852E_00A024E0E339__INCLUDED_

#include "IceDockingControlBar.h"
#include "IceExplorerView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceDockingExplorerView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceDockingExplorerView window

class CIceDockingExplorerView : public CIceDockingControlBar
{
// Construction
public:
	CIceDockingExplorerView();

// Attributes
public:

protected:
	CIceExplorerView	m_ExplorerView;
	CImageList			m_ImageList;
    CFont				m_font;
	CFont				m_fontHeader;

	COLORREF m_clrBtnHilight;
	COLORREF m_clrBtnShadow;
	int		m_iPaneCaptionY;

	CSize		m_sizeStoreHorz;
	CSize		m_sizeStoreVert;


private:
	CIceExplorerView    *m_pControl;


// Operations
public:
	void PositionControls();

	BOOL ShowPane(BOOL bShow = TRUE);
	BOOL PaneIsVisible() {	return IsHorzDocked() ? (m_sizeHorz.cy > 0) : (m_sizeVert.cx > 0); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceDockingExplorerView)
    virtual BOOL Create(CWnd* pParentWnd, CSize sizeDefault, UINT nID, DWORD dwStyle = WS_CHILD | WS_VISIBLE | CBRS_TOP);
    virtual BOOL Create(LPCTSTR lpszWindowName, CWnd* pParentWnd, CSize sizeDefault, BOOL bHasGripper, UINT nID, DWORD dwStyle = WS_CHILD | WS_VISIBLE | CBRS_TOP);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceDockingExplorerView();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceDockingExplorerView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnNcPaint();
	afx_msg void OnPaint();
	afx_msg void OnCaptionDrag(UINT nFlags, CPoint point);
	afx_msg void OnCaptionDblClk(UINT nFlags, CPoint point);
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnWindowPosChanged(WINDOWPOS FAR* lpwndpos);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEDOCKINGEXPLORERVIEW_H__F07F67E0_5528_11D2_852E_00A024E0E339__INCLUDED_)
