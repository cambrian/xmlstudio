#if !defined(AFX_ICEEXPLORERVIEW_H__F8C3EA42_551C_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEEXPLORERVIEW_H__F8C3EA42_551C_11D2_852E_00A024E0E339__INCLUDED_

#include "IceButton.h"
#include "IceStatic.h"
#include "IceTreeCtrl.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceExplorerView.h : header file
//

#define IDC_CAPTION					(5100)
#define IDC_BEVEL					(5101)
#define IDC_BEVELL					(5102)
#define IDC_BEVELR					(5103)
#define IDC_LINE					(5104)
#define IDC_BTN_HIDE				(5105)

#define BTNX		16
#define BTNY		12
#define BORDER		 2
#define IDC_TREEMENUFRAME	1119

#define ICEEXPLORERVIEW_CLASSNAME		_T("IceExplorerViewClass")

/////////////////////////////////////////////////////////////////////////////
// CIceExplorerView window class (!!!! this is NOT CView-derived class despite its name !!!!)

class CIceExplorerView : public CWnd
{
public:
	CIceExplorerView(); 
	virtual ~CIceExplorerView();
	DECLARE_DYNCREATE(CIceExplorerView)

// Attributes
public:
	CIceStatic  m_stcCaption;
	CFont		m_captionFont;
	CStatic		m_stcBevel;
	CStatic		m_stcBevelLeft;
	CStatic		m_stcBevelRight;
	CStatic     m_stcLine;
	CIceTreeCtrl	m_tree;
	CIceButton	m_btn;

protected:
	BOOL bSideBevels;
	BOOL bFrameBevel;
	CToolTipCtrl m_ToolTip;

	BOOL bHeader;   // show caption and hide button

// Operations
public:
	void RegisterClass();
	BOOL Create(const RECT& rect, CWnd* parent, UINT nID,
                DWORD dwStyle = WS_CHILD | WS_TABSTOP | WS_VISIBLE);
	BOOL SubclassDlgItem(UINT nID, CWnd* parent); // use in CDialog/CFormView

	CIceTreeCtrl*  GetIceTreeCtrl() {return &m_tree;}

	void Hide(BOOL hide = TRUE);
	void ShowSideBevels(BOOL SideBevels = TRUE) { bSideBevels = SideBevels;}
	void ShowFrameBevel(BOOL FrameBevel = TRUE) { bFrameBevel = FrameBevel;}
	void CalcLayout(BOOL bParent = FALSE);

	void ShowHeader(BOOL Header = TRUE) { bHeader = Header;}

private:
	void Initialize();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceExplorerView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
protected:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceExplorerView)
	afx_msg void OnButtonHide();
	afx_msg void OnCaptionDrag();
	afx_msg void OnCaptionDblClk();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEEXPLORERVIEW_H__F8C3EA42_551C_11D2_852E_00A024E0E339__INCLUDED_)
