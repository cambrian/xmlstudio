// ChildSpltFrm.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ChildSpltFrm.h"

#include "imsDoc.h"
#include "imsView.h"
#include "imsTagView.h"
#include "imsTextView.h"
//#include "PaletView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildSpltFrame

IMPLEMENT_DYNCREATE(CChildSpltFrame, CIceMDIChildWnd)

CChildSpltFrame::CChildSpltFrame()
{
	//iceMenu.LoadToolBarResource(IDR_MAINFRAME);
	iceMenu.AddToolBarResource(IDR_SYSTEMBAR);
	iceMenu.AddToolBarResource(IDR_RESERVOIR);
	iceMenu.AddToolBarResource(IDR_MRUFILE);

	m_bSplitPalette = TRUE;
}

CChildSpltFrame::~CChildSpltFrame()
{
}

BOOL CChildSpltFrame::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CIceMDIChildWnd::PreCreateWindow(cs);
}


BEGIN_MESSAGE_MAP(CChildSpltFrame, CIceMDIChildWnd)
	//{{AFX_MSG_MAP(CChildSpltFrame)
	ON_COMMAND(ID_VIEW_SPLIT_PALETTE, OnViewSplitPalette)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SPLIT_PALETTE, OnUpdateViewSplitPalette)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildSpltFrame diagnostics

#ifdef _DEBUG
void CChildSpltFrame::AssertValid() const
{
	CIceMDIChildWnd::AssertValid();
}

void CChildSpltFrame::Dump(CDumpContext& dc) const
{
	CIceMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildSpltFrame message handlers

void CChildSpltFrame::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class

	//nCmdShow = SW_SHOWMAXIMIZED;
	CIceMDIChildWnd::ActivateFrame(nCmdShow);
}

BOOL CChildSpltFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	int cxRight = 100;  // right pane
	int cyTop = 100;    // top pane inside the right pane

    CRect rect;
    GetClientRect(&rect);
	int cX = rect.right - rect.left;
	int cY = rect.bottom - rect.top;
	int cxLeft = cX - cxRight;
	int cyBottom = cY - cyTop;

	// create a splitter with 1 row, 2 columns
	if(!m_wndLRSplitter.CreateStatic(this, 1, 2,
        WS_CHILD | WS_VISIBLE | WS_BORDER))
	{
		TRACE0("Failed to create split bar ");
		return FALSE;    // failed to create
	}
    m_wndLRSplitter.SetColumnInfo(1, cxRight, 0);   // second column pane size = CXPALETWIDTH

	// add the second splitter in the right-hand side pane 
	m_wndTBSplitter.CreateStatic(
		&m_wndLRSplitter,      // our parent window is the first splitter
		2, 1,               // the new splitter is 2 rows, 1 column
        WS_CHILD | WS_VISIBLE | WS_BORDER,  // style, WS_BORDER is needed
		m_wndLRSplitter.IdFromRowCol(0, 1));
    m_wndTBSplitter.SetRowInfo(0, cyTop, 0);

    // now create the 3 views inside the splitter
	m_wndLRSplitter.CreateView(0, 0,
        RUNTIME_CLASS(CImsView), CSize(cxLeft, cY), pContext);
	m_wndTBSplitter.CreateView(0, 0,
		RUNTIME_CLASS(CImsTagView), CSize(cxRight, cyTop), pContext);
	m_wndTBSplitter.CreateView(1, 0,
		RUNTIME_CLASS(CImsTextView), CSize(cxRight, cyBottom), pContext);

	// activate the Main-View window (CImsView)
	SetActiveView((CView*)m_wndLRSplitter.GetPane(0,0));

	return TRUE;

	//return CIceMDIChildWnd::OnCreateClient(lpcs, pContext);
}

void CChildSpltFrame::OnViewSplitPalette() 
{
	// TODO: Add your command handler code here
	
	if(m_bSplitPalette)
	{
		//m_wndLRSplitter.DeleteColumn(1);
		m_bSplitPalette = FALSE;
	}
	else
	{
		m_bSplitPalette = TRUE;
	}
}

void CChildSpltFrame::OnUpdateViewSplitPalette(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(m_bSplitPalette);
}
