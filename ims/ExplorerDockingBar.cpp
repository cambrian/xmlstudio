// ExplorerDockingBar.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ExplorerDockingBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define NUM_ITEMS	9
#define NUM_SUB		3

static _TCHAR *_gszItem[NUM_ITEMS] = 
{
	_T("Fifi"),		_T("Babs Bunny"),	_T("Buster Bunny"),
	_T("Concorde"), _T("Cyote"),		_T("Dizzy Devil"), 
	_T("Elmyra"),	_T("Hampton"),		_T("Plucky Duck")
};

/////////////////////////////////////////////////////////////////////////////
// CExplorerDockingBar

CExplorerDockingBar::CExplorerDockingBar()
{
    m_sizeMin = CSize(32, 32);
    m_sizeHorz = CSize(400, 200);
    m_sizeVert = CSize(200, 400);
    m_sizeFloat = CSize(200, 400);
    m_bTracking = FALSE;
    m_bInRecalcNC = FALSE;
    m_cxEdge = 5;
    m_bDragShowContent = FALSE;
}

CExplorerDockingBar::~CExplorerDockingBar()
{
}


BEGIN_MESSAGE_MAP(CExplorerDockingBar, CIceDockingExplorerView)
	//{{AFX_MSG_MAP(CExplorerDockingBar)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
//
void CExplorerDockingBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}


/////////////////////////////////////////////////////////////////////////////
// CExplorerDockingBar message handlers

int CExplorerDockingBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingExplorerView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	// Change IceExplorerView's default look
	//*//m_ExplorerView.GetIceTreeCtrl()->SetItemHeight(24);  // bitmap size: 16
	//*//m_ExplorerView.GetIceTreeCtrl()->SetBkColor(GetSysColor (COLOR_3DFACE));
	m_ExplorerView.ShowSideBevels();
	m_ExplorerView.ShowHeader();

	// Change the caption
	m_ExplorerView.m_stcCaption.SetWindowText("Workspace Menu");

	// ImageList
	//m_ImageList.Create(IDB_TINYTOON, 32, 1, RGB(255,0,255));
	m_ImageList.Create(IDB_WORKSPACE, 16, 1, RGB(255,0,255)); // bitmap background: purple
    
    // Attach ImageList to TreeView
    if (m_ImageList)
        m_ExplorerView.GetIceTreeCtrl()->SetImageList((HIMAGELIST)m_ImageList,TVSIL_NORMAL);


	TVITEM tvi, tvi1;
	TVINSERTSTRUCT tvis;
	HTREEITEM hPrev;
	
	tvi.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_CHILDREN;
	tvi.hItem = NULL;
	tvi.cchTextMax = 64;     
	tvi.pszText = _T("Tiny Toons");
	tvi.iImage = 1;
	tvi.iSelectedImage = 0;

	tvis.hParent = TVI_ROOT;
	tvis.hInsertAfter = TVI_LAST;
	tvis.item = tvi;

	hPrev = m_ExplorerView.GetIceTreeCtrl()->InsertItem(&tvis);
	
	int i;
	for(i = 0; i < NUM_SUB; i++)
	{
		tvi1.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
		tvi1.cchTextMax = 64; 
		tvi1.pszText = _gszItem[i];
		tvi1.iImage = 3;
		tvi1.iSelectedImage = 2;
	
		tvis.hParent = hPrev;
		tvis.hInsertAfter = TVI_LAST;
		tvis.item = tvi1;
		m_ExplorerView.GetIceTreeCtrl()->InsertItem(&tvis);
	}

	tvi.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_CHILDREN;
	tvi.hItem = NULL;
	tvi.pszText = _T("Tiny Toons 2");
	tvi.iImage = 1;
	tvi.iSelectedImage = 0;

	tvis.hParent = TVI_ROOT;
	tvis.hInsertAfter = TVI_LAST;
	tvis.item = tvi;

	hPrev = m_ExplorerView.GetIceTreeCtrl()->InsertItem(&tvis);
	

	for(i = 0; i < NUM_SUB; i++)
	{
		tvi1.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
		tvi1.pszText = _gszItem[3+i];
		tvi1.iImage = 3;
		tvi1.iSelectedImage = 2;
	
		tvis.hParent = hPrev;
		tvis.hInsertAfter = TVI_LAST;
		tvis.item = tvi1;
		m_ExplorerView.GetIceTreeCtrl()->InsertItem(&tvis);
	}

	tvi.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_CHILDREN;
	tvi.hItem = NULL;
	tvi.pszText = _T("Tiny Toons 3");
	tvi.iImage = 1;
	tvi.iSelectedImage = 0;

	tvis.hParent = TVI_ROOT;
	tvis.hInsertAfter = TVI_LAST;
	tvis.item = tvi;

	hPrev = m_ExplorerView.GetIceTreeCtrl()->InsertItem(&tvis);


	CString s;
	for(i = 0; i < NUM_SUB; i++)
	{
		s.Format("Bookmark No: %i", i+1);

		tvi1.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
		tvi1.pszText = s.GetBuffer(64);
		tvi.hItem = NULL;
		tvi1.iImage = 3;
		tvi1.iSelectedImage = 2;
	
		tvis.hParent = TVI_ROOT;
		tvis.hInsertAfter = TVI_LAST;
		tvis.item = tvi1;
		m_ExplorerView.GetIceTreeCtrl()->InsertItem(&tvis);

		s.ReleaseBuffer();
	}


	return 0;
}

void CExplorerDockingBar::OnInitialUpdate() 
{
	CIceDockingExplorerView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	
}

