#if !defined(AFX_TEXDOC_H__BB9136A4_5797_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_TEXDOC_H__BB9136A4_5797_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TexDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTexDoc document

class CTexDoc : public COleServerDoc
{
protected:
	CTexDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CTexDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTexDoc)
	public:
	virtual void OnFinalRelease();
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	protected:
	virtual BOOL OnNewDocument();
	virtual COleServerItem* OnGetEmbeddedItem();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTexDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CTexDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CTexDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXDOC_H__BB9136A4_5797_11D2_852E_00A024E0E339__INCLUDED_)
