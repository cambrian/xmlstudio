@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by IMS.HPJ. >"hlp\ims.hm"
echo. >>"hlp\ims.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\ims.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\ims.hm"
echo. >>"hlp\ims.hm"
echo // Prompts (IDP_*) >>"hlp\ims.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\ims.hm"
echo. >>"hlp\ims.hm"
echo // Resources (IDR_*) >>"hlp\ims.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\ims.hm"
echo. >>"hlp\ims.hm"
echo // Dialogs (IDD_*) >>"hlp\ims.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\ims.hm"
echo. >>"hlp\ims.hm"
echo // Frame Controls (IDW_*) >>"hlp\ims.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\ims.hm"
REM -- Make help for Project IMS


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\ims.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\ims.hlp" goto :Error
if not exist "hlp\ims.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\ims.hlp" Debug
if exist Debug\nul copy "hlp\ims.cnt" Debug
if exist Release\nul copy "hlp\ims.hlp" Release
if exist Release\nul copy "hlp\ims.cnt" Release
echo.
goto :done

:Error
echo hlp\ims.hpj(1) : error: Problem encountered creating help file

:done
echo.
