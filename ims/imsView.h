// imsView.h : interface of the CImsView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMSVIEW_H__D4AD6C92_51ED_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_IMSVIEW_H__D4AD6C92_51ED_11D2_852E_00A024E0E339__INCLUDED_

#include "IceListView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CImsCntrItem;

class CImsView : public CIceListView
{
protected: // create from serialization only
	CImsView();
	DECLARE_DYNCREATE(CImsView)

// Attributes
public:
	CImsDoc* GetDocument();
	// m_pSelection holds the selection to the current CImsCntrItem.
	// For many applications, such a member variable isn't adequate to
	//  represent a selection, such as a multiple selection or a selection
	//  of objects that are not CImsCntrItem objects.  This selection
	//  mechanism is provided just to help you get started.

	// TODO: replace this selection mechanism with one appropriate to your app.
	CImsCntrItem* m_pSelection;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual BOOL IsSelected(const CObject* pDocItem) const;// Container support
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CImsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CImsView)
	afx_msg void OnDestroy();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnInsertObject();
	afx_msg void OnCancelEditCntr();
	afx_msg void OnCancelEditSrvr();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CImsDoc* CImsView::GetDocument()
   { return (CImsDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMSVIEW_H__D4AD6C92_51ED_11D2_852E_00A024E0E339__INCLUDED_)
