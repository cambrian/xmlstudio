#if !defined(AFX_PARSEVIEW_H__1FA13986_5304_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_PARSEVIEW_H__1FA13986_5304_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ParseView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CParseView view

class CParseView : public CRichEditView
{
protected: // create from serialization only
	CParseView();
	DECLARE_DYNCREATE(CParseView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParseView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CParseView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CParseView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARSEVIEW_H__1FA13986_5304_11D2_852E_00A024E0E339__INCLUDED_)
