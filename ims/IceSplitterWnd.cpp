// IceSplitterWnd.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceSplitterWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceSplitterWnd

CIceSplitterWnd::CIceSplitterWnd()
{
}

CIceSplitterWnd::~CIceSplitterWnd()
{
}


BEGIN_MESSAGE_MAP(CIceSplitterWnd, CSplitterWnd)
	//{{AFX_MSG_MAP(CIceSplitterWnd)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CIceSplitterWnd message handlers

void CIceSplitterWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CSplitterWnd::OnMouseMove(nFlags, point);
}

void CIceSplitterWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CSplitterWnd::OnLButtonDown(nFlags, point);
}

BOOL CIceSplitterWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CSplitterWnd::OnSetCursor(pWnd, nHitTest, message);
}

void CIceSplitterWnd::OnSize(UINT nType, int cx, int cy) 
{
	CSplitterWnd::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}
