// IceParseView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceParseView.h"

#include "expat\xmlparse.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceParseView

IMPLEMENT_DYNCREATE(CIceParseView, CIceListView)

CIceParseView::CIceParseView()
{
}

CIceParseView::~CIceParseView()
{
}


BEGIN_MESSAGE_MAP(CIceParseView, CIceListView)
	//{{AFX_MSG_MAP(CIceParseView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceParseView drawing

void CIceParseView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CIceParseView diagnostics

#ifdef _DEBUG
void CIceParseView::AssertValid() const
{
	CIceListView::AssertValid();
}

void CIceParseView::Dump(CDumpContext& dc) const
{
	CIceListView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// Parser helper functions

void _startElementHandler(void *userData, const char *name, const char **atts)
{
	DWORD*  pAddress = (DWORD*) userData;
	CListNode* pNode = (CListNode*) *pAddress;

	CListNode* pChild;
	if(pNode->GetIndentLevel() == 0)  // we are adding the document root
	{
		LPNODEINFO lpNodeInfoDoc = new NODEINFO;
		lpNodeInfoDoc->m_nodeType = LNT_DOCUMENT;
		CString strDoc(name);
		lpNodeInfoDoc->m_strItemName = strDoc;
		pChild = pNode->InsertItem(lpNodeInfoDoc);
	}
	else
	{
		// Insert element
		LPNODEINFO lpNodeInfoElement = new NODEINFO;
		lpNodeInfoElement->m_nodeType = LNT_ELEMENT;
		CString strElement(name);
		lpNodeInfoElement->m_strItemName = strElement;
		pChild = pNode->InsertItem(lpNodeInfoElement);
	}

	// Insert attributes
	for(int i=0;atts[i]!=NULL;i+=2)
	{
		LPNODEINFO lpNodeInfo = new NODEINFO;
		CString strAttr(atts[i]);
		CString strValue(atts[i+1]);
		lpNodeInfo->m_nodeType = LNT_ATTR;
		lpNodeInfo->m_strItemName = strAttr;
		lpNodeInfo->m_SubItems.Add(strValue);
		(void) pChild->InsertItem(lpNodeInfo);
	}

	*pAddress = (DWORD) pChild;
}

void _endElementHandler(void *userData, const char *name)
{
	DWORD*  pAddress = (DWORD*) userData;
	CListNode* pNode = (CListNode*) *pAddress;

	if(pNode->GetIndentLevel() > 0)
		pNode = pNode->GetParent();

	*pAddress = (DWORD) pNode;
}

void _characterDataHandler(void *userData, const char *s, int len)
{
	DWORD*  pAddress = (DWORD*) userData;
	CListNode* pNode = (CListNode*) *pAddress;

	// Insert text
	LPNODEINFO lpNodeInfoText = new NODEINFO;
	lpNodeInfoText->m_nodeType = LNT_TEXT;
	CString strText("text");
	lpNodeInfoText->m_strItemName=strText;
	CString strData(s,len);
	lpNodeInfoText->m_SubItems.Add(strData);
	//strData.TrimLeft();    // ???  This is not right!!!
	//strData.TrimRight();   // ???  This is not right!!!
	//if(!strData.IsEmpty()) // ???  This is not right!!!
	{
		(void) pNode->InsertItem(lpNodeInfoText);
	}

	//*pAddress = (DWORD) pNode;
}



/////////////////////////////////////////////////////////////////////////////
// CIceParseView message handlers

int CIceParseView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceListView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CListCtrl& m_List = GetListCtrl();

	LV_COLUMN   lvColumn;
	TCHAR szString1[2][20] = {_T("Markup"), 
	                          _T("Content")}; 
	//initialize the columns
	lvColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvColumn.fmt = LVCFMT_LEFT;

	lvColumn.cx = 150;
	lvColumn.pszText = szString1[0];
	m_List.InsertColumn(0,&lvColumn);
	lvColumn.cx = 250;
	lvColumn.pszText = szString1[1];
	m_List.InsertColumn(1,&lvColumn);
	
	LV_ITEM lvItem;		
	lvItem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_INDENT | LVIF_PARAM;
	lvItem.pszText = _T("Root");
	lvItem.iImage = 0;
	lvItem.iItem = 0;
	lvItem.lParam=(LPARAM)m_pRoot;
	lvItem.iIndent=0;
	lvItem.iSubItem = 0;
	m_List.InsertItem(&lvItem);
	
	/*
	///////////////////////////////////////
	for(int i=0; i<4; i++)
	{
		LPNODEINFO lpNodeInfo1 = new NODEINFO;
		CString strItem;
		strItem.Format("Item %d",i);
		lpNodeInfo1->m_strItemName=strItem;
		CListNode* pParent = m_pRoot->InsertItem(m_pRoot, lpNodeInfo1);
		if(i%2)
		{
			CListNode* pParent1=NULL;
			CListNode* pParent2=NULL;
			for(int x=0; x<5; x++)
			{
				LPNODEINFO lpNodeInfo7 = new NODEINFO;
				CString strItem;
				strItem.Format("Item %d",x);
				lpNodeInfo7->m_strItemName=strItem;
				pParent1=m_pRoot->InsertItem(pParent,lpNodeInfo7);
			}

			for(int y=0; y<3; y++)
			{
				LPNODEINFO lpNodeInfo7 = new NODEINFO;
				CString strItem;
				strItem.Format("Item %d",y);
				lpNodeInfo7->m_strItemName=strItem;
				pParent2=m_pRoot->InsertItem(pParent1,lpNodeInfo7);
			}

			for(int z=0; z<3; z++)
			{
				LPNODEINFO lpNodeInfo7 = new NODEINFO;
				CString strItem;
				strItem.Format("Item %d",z);
				lpNodeInfo7->m_strItemName=strItem;
				m_pRoot->InsertItem(pParent2, lpNodeInfo7);
			}
		}
	}
	////////////////////
	*/

	m_pRoot->Collapse(m_pRoot);
	m_pRoot->Expand(m_pRoot,0);

	return 0;
}



void CIceParseView::Serialize(CArchive& ar) 
{
	if (ar.IsStoring())
	{	// storing code

		CString strOut;

		CListNode*  pNode = m_pRoot->GetNext(m_pRoot, FALSE);
		CListNode*  pNextNode = pNode;;
		do 
		{
			pNode = pNextNode;

			LPNODEINFO lpNodeInfo = pNode->GetData();
			if(lpNodeInfo->m_nodeType == LNT_PROLOG)
			{
				strOut = lpNodeInfo->m_strItemName + "\n"; 
				ar.WriteString(strOut);
			}
			else if(lpNodeInfo->m_nodeType == LNT_DOCUMENT)
			{
				strOut = lpNodeInfo->m_strItemName + "\n"; 
				ar.WriteString(strOut);
			}
			else if(lpNodeInfo->m_nodeType == LNT_ELEMENT)
			{
				strOut = "<" + lpNodeInfo->m_strItemName + ">" + "\n"; 
				ar.WriteString(strOut);
			}

			pNextNode = pNode->GetNext(pNode, FALSE);
		} while(pNextNode != NULL && pNode != pNextNode);


	}	// storing code
	else
	{	// loading code

		const int nBufMax = 10000;   // TEPORARY!!!!!!!!!!1
		char InBuff[nBufMax];
		XML_Parser parser = ::XML_ParserCreate(NULL);
		
		DWORD  nodeAddress = (DWORD) m_pRoot;
		::XML_SetUserData(parser, &nodeAddress);

		::XML_SetElementHandler(parser, _startElementHandler, _endElementHandler);
		::XML_SetCharacterDataHandler(parser, _characterDataHandler);
		
		
		// [1] Add Prolog
		LPNODEINFO lpNodeInfoPro = new NODEINFO;
		lpNodeInfoPro->m_nodeType = LNT_PROLOG;
		CString strPro("Prolog");
		lpNodeInfoPro->m_strItemName=strPro;
		CListNode* pProlog = m_pRoot->InsertItem(m_pRoot, lpNodeInfoPro);


		// [2] document parsing
		BOOL done;
		do 
		{
			int nBuf = ar.Read(InBuff,nBufMax-1);
			done = nBuf < nBufMax-1;
			InBuff[nBuf] = '\0';

			if (!::XML_Parse(parser, InBuff, nBuf, done)) 
			{
				CString  strError;
				strError.Format("%s at line %d",
					::XML_ErrorString(::XML_GetErrorCode(parser)),
					::XML_GetCurrentLineNumber(parser));
				AfxMessageBox(strError);
				return;
			}
		} while (!done);

		// [3] Add Epilog
		LPNODEINFO lpNodeInfoEpi = new NODEINFO;
		lpNodeInfoEpi->m_nodeType = LNT_EPILOG;
		CString strEpi("Epilog");
		lpNodeInfoEpi->m_strItemName=strEpi;
		CListNode* pEpilog = m_pRoot->InsertItem(m_pRoot, lpNodeInfoEpi);
		
		
		::XML_ParserFree(parser);

	}	// loading code
}



