#if !defined(AFX_ICETREECTRL_H__FDC49F00_526D_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICETREECTRL_H__FDC49F00_526D_11D2_852E_00A024E0E339__INCLUDED_

#include "CoolTreeCtrl.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceTreeCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceTreeCtrl window

//class CIceTreeCtrl : public CTreeCtrl
class CIceTreeCtrl : public CCoolTreeCtrl
{
// Construction
public:
	CIceTreeCtrl();

// Attributes
public:

protected:
	COLORREF	m_clr3DFace;
	COLORREF	m_clr3DShadow;
	COLORREF	m_clr3DDkShadow;
	COLORREF	m_clr3DLight;
	COLORREF	m_clr3DHilight;
	COLORREF	m_clrSelBkColor;
	COLORREF	m_clrSelTextColor;

	CPoint	m_point;

// Operations
public:

	void SetSelBkColor(COLORREF clrSelBkColor);
	void SetSelTextColor(COLORREF clrSelTextColor);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceTreeCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceTreeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceTreeCtrl)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICETREECTRL_H__FDC49F00_526D_11D2_852E_00A024E0E339__INCLUDED_)
