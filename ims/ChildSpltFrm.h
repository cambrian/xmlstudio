#if !defined(AFX_CHILDSPLTFRM_H__15B9B764_56F3_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_CHILDSPLTFRM_H__15B9B764_56F3_11D2_852E_00A024E0E339__INCLUDED_

#include "IceMDIChildWnd.h"
#include "ThinSplitterWnd.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ChildSpltFrm.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChildSpltFrame frame

class CChildSpltFrame : public CIceMDIChildWnd
{
	DECLARE_DYNCREATE(CChildSpltFrame)
protected:
	CChildSpltFrame();           // protected constructor used by dynamic creation

// Attributes
public:

protected:
	CThinSplitterWnd  m_wndLRSplitter;
	CThinSplitterWnd  m_wndTBSplitter;

private:
	BOOL    m_bSplitPalette;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChildSpltFrame)
	public:
	virtual void ActivateFrame(int nCmdShow = -1);
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CChildSpltFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CChildSpltFrame)
	afx_msg void OnViewSplitPalette();
	afx_msg void OnUpdateViewSplitPalette(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDSPLTFRM_H__15B9B764_56F3_11D2_852E_00A024E0E339__INCLUDED_)
