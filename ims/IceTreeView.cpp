// IceTreeView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceTreeView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceTreeView

IMPLEMENT_DYNCREATE(CIceTreeView, CTreeView)

CIceTreeView::CIceTreeView()
{
	m_clr3DFace			= ::GetSysColor(COLOR_3DFACE);
	m_clr3DShadow		= ::GetSysColor(COLOR_3DSHADOW);
	m_clr3DDkShadow		= ::GetSysColor(COLOR_3DDKSHADOW);
	m_clr3DLight		= ::GetSysColor(COLOR_3DLIGHT);
	m_clr3DHilight		= ::GetSysColor(COLOR_3DHILIGHT);
	m_clrSelTextColor	= ::GetSysColor(COLOR_BTNTEXT);
	m_clrSelBkColor		= m_clr3DHilight;
}

CIceTreeView::~CIceTreeView()
{
}


BEGIN_MESSAGE_MAP(CIceTreeView, CTreeView)
	//{{AFX_MSG_MAP(CIceTreeView)
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
	ON_NOTIFY_REFLECT(TVN_SELCHANGING, OnSelchanging)
	ON_NOTIFY_REFLECT(TVN_ENDLABELEDIT, OnEndlabeledit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceTreeView drawing

void CIceTreeView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CIceTreeView diagnostics

#ifdef _DEBUG
void CIceTreeView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CIceTreeView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIceTreeView message handlers

int CIceTreeView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CTreeView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	GetTreeCtrl().ModifyStyle(0, 
		TVS_HASBUTTONS|TVS_LINESATROOT|TVS_HASLINES|TVS_SHOWSELALWAYS);
		//NM_CUSTOMDRAW | TVS_EDITLABELS | TVS_SHOWSELALWAYS);
		//TVS_EDITLABELS | TVS_SHOWSELALWAYS);

	return 0;
}

void CIceTreeView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	UINT flags;
	HTREEITEM tiSelected = GetTreeCtrl().HitTest(point, &flags);
	
	if( (flags & TVHT_ONITEMRIGHT)  || (flags & TVHT_ONITEMINDENT) ||
		(flags & TVHT_ONITEM)		|| (flags & TVHT_ONITEMBUTTON) ) 
	{
		;
	}

	CTreeView::OnLButtonDown(nFlags, point);
}

void CIceTreeView::OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	HTREEITEM htiOld = pNMTreeView->itemOld.hItem;
	HTREEITEM htiNew = pNMTreeView->itemNew.hItem;

	if ((htiOld == htiNew) || GetTreeCtrl().GetItemState(htiNew, TVIS_EXPANDED) & TVIS_EXPANDED)
		return;

	HTREEITEM htiRoot = NULL;
	HTREEITEM htiChild = NULL;
	htiRoot = GetTreeCtrl().GetNextItem(htiRoot, TVGN_ROOT);
	while (htiRoot != NULL)
	{
		if (GetTreeCtrl().ItemHasChildren(htiRoot) && (GetTreeCtrl().ItemHasChildren(htiNew)))
			GetTreeCtrl().Expand(htiRoot, TVE_COLLAPSE);
			
		htiRoot = GetTreeCtrl().GetNextItem(htiRoot, TVGN_NEXT);
		
	}
	
	if (GetTreeCtrl().ItemHasChildren(htiNew))
		GetTreeCtrl().Expand(htiNew, TVE_EXPAND );

	*pResult = 0;
}

void CIceTreeView::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here

	TV_ITEM	*ptvItem = &pTVDispInfo->item;
	GetTreeCtrl().SetItem(ptvItem);

	*pResult = 0;
}

void CIceTreeView::OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult) 
{

	NMTVCUSTOMDRAW *pTVCD = (NMTVCUSTOMDRAW*)pNMHDR;
	
	CDC* pDC = CDC::FromHandle(pTVCD->nmcd.hdc);

	// The rect for the cell gives correct left and right values.
	CRect rect = pTVCD->nmcd.rc;
	
	// By default set the return value to do the default behavior.
	*pResult = 0;

	
	switch( pTVCD->nmcd.dwDrawStage )
	{
	// First stage (for the whole control)
	case  CDDS_PREPAINT: 
		{		
			*pResult = CDRF_NOTIFYITEMDRAW;
		}
		break;

	// Stage three (called for each subitem of the focused item)
	case CDDS_ITEMPREPAINT:  //*//| CDDS_SUBITEM: 
		{
			//*//*pResult = CDRF_NOTIFYSUBITEMDRAW | CDRF_NOTIFYPOSTPAINT;
			*pResult = CDRF_NOTIFYPOSTPAINT;
		}
		break;

	// Stage four (called for each subitem of the focused item)
	case CDDS_ITEMPOSTPAINT: //*//| CDDS_SUBITEM: 
		{
			*pResult = CDRF_SKIPDEFAULT;	
		}
		break;
	
	default: 
		// Stage two handled here. (called for each item)
		if (pTVCD->nmcd.uItemState & CDIS_HOT)
		{
			pDC->Draw3dRect( &rect, m_clr3DHilight, m_clr3DShadow );
			pDC->SetBkColor(pDC->GetBkColor());
			pDC->SetTextColor(RGB(0,0,255));

			// Tell the control that to draw it again.
			*pResult = CDRF_NOTIFYPOSTPAINT;

		}

		if (pTVCD->nmcd.uItemState & CDIS_SELECTED)
		{
			*pResult = CDRF_SKIPDEFAULT;
		}

		if( (pTVCD->nmcd.uItemState & CDIS_FOCUS) )
		{
			pDC->Draw3dRect( &rect, m_clr3DShadow, m_clr3DHilight );
			pDC->SetBkColor(m_clrSelBkColor);
			pDC->SetTextColor(m_clrSelTextColor);

			*pResult = CDRF_NOTIFYPOSTPAINT;
		}	
		
		if( (pTVCD->nmcd.uItemState & CDIS_CHECKED) )
		{
			*pResult = CDRF_DODEFAULT;
		}	
		
		break;
	}
}
