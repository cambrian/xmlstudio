// SrvrItem.h : interface of the CImsSrvrItem class
//

#if !defined(AFX_SRVRITEM_H__D4AD6C95_51ED_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_SRVRITEM_H__D4AD6C95_51ED_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CImsSrvrItem : public CDocObjectServerItem
{
	DECLARE_DYNAMIC(CImsSrvrItem)

// Constructors
public:
	CImsSrvrItem(CImsDoc* pContainerDoc);

// Attributes
	CImsDoc* GetDocument() const
		{ return (CImsDoc*)CDocObjectServerItem::GetDocument(); }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsSrvrItem)
	public:
	virtual BOOL OnDraw(CDC* pDC, CSize& rSize);
	virtual BOOL OnGetExtent(DVASPECT dwDrawAspect, CSize& rSize);
	//}}AFX_VIRTUAL

// Implementation
public:
	~CImsSrvrItem();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SRVRITEM_H__D4AD6C95_51ED_11D2_852E_00A024E0E339__INCLUDED_)
