// IceExplorerView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceExplorerView.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceExplorerView

IMPLEMENT_DYNCREATE(CIceExplorerView, CWnd)

CIceExplorerView::CIceExplorerView()
{
	RegisterClass();
	bSideBevels = FALSE;
	bFrameBevel = FALSE;

	bHeader = TRUE;  /////////////
}

CIceExplorerView::~CIceExplorerView()
{
}

void CIceExplorerView::RegisterClass()
{
	WNDCLASSEX	wc;
	HINSTANCE hInst = AfxGetInstanceHandle();

	if (!(::GetClassInfoEx(hInst, ICEEXPLORERVIEW_CLASSNAME, &wc)))
    {
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_DBLCLKS|CS_VREDRAW|CS_HREDRAW|CS_CLASSDC; //CS_GLOBALCLASS|CS_BYTEALIGNCLIENT;
		wc.lpfnWndProc = ::DefWindowProc;				// Message processing code
		wc.cbClsExtra = 0;								// No extra bytes needed
		wc.cbWndExtra = 0;								// No DLGWINDOWEXTRA needed
		wc.hInstance = hInst;							// Instance handle
		wc.hIcon = NULL;								// No icon
		wc.hCursor = ::LoadCursor(NULL, IDC_ARROW);		// Use Arrow cursor 
		wc.hbrBackground = NULL;						// (HBRUSH) (COLOR_3DFACE+1);
		wc.lpszMenuName = NULL;							// No menus
		wc.lpszClassName = ICEEXPLORERVIEW_CLASSNAME;		// Class name
		wc.hIconSm = NULL;								// No Icon associated to the class
	}

	if (!::RegisterClassEx(&wc))					// If registration failed, subsequent dialogs will fail
	{
		ASSERT(FALSE);
	}
}


void CIceExplorerView::Initialize()
{
	// creates all the objects in frame -
	// caption, tree, button

	CRect rc;

	if (bSideBevels || !bFrameBevel)
	{
		if (!m_stcBevelLeft.Create("", WS_VISIBLE|SS_ETCHEDHORZ,
									rc, this, IDC_BEVELL ))
		{
			return;
		}

		if (!m_stcBevelRight.Create("", WS_VISIBLE|SS_ETCHEDHORZ,
									rc, this, IDC_BEVELR ))
		{
			return;
		}
	}

	if (!bSideBevels || bFrameBevel)
	{
		if (!m_stcBevel.Create("", WS_VISIBLE|SS_ETCHEDFRAME,
									rc, this, IDC_BEVEL ))
		{
			return;
		}

	}

	if (!m_stcLine.Create("", WS_VISIBLE|SS_ETCHEDHORZ,
								rc, this, IDC_LINE ))
	{
		return;
	}
	
	// create the caption
	if (!m_stcCaption.Create("Tree Menu", WS_VISIBLE|SS_CENTER|SS_CENTERIMAGE,
								rc, this, IDC_CAPTION ))
	{
		return;
	}

	// create the button
	if (!m_btn.Create("x", WS_VISIBLE|BS_OWNERDRAW|BS_CENTER|BS_VCENTER,
							rc, this, IDC_BTN_HIDE))
	{
		return;
	}

	// a nicer font
	NONCLIENTMETRICS ncm;
    ncm.cbSize = sizeof(NONCLIENTMETRICS);
    VERIFY(SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0));
    m_captionFont.CreateFontIndirect(&(ncm.lfMessageFont));

	m_stcCaption.SetFont(&m_captionFont);


	if (!m_tree.Create(WS_VISIBLE|WS_CHILD|TVS_TRACKSELECT|TVS_HASBUTTONS, //| TVS_EDITLABELS|TVS_NOSCROLL|TVS_FULLROWSELECT,
						   rc, this, IDC_TREEMENUFRAME))
	{
		return;
	}

	// and a tooltip for the hide button
	m_ToolTip.Create(this);	
    m_ToolTip.AddTool(&m_btn, _T("Hide"));
	m_ToolTip.AddTool(&m_stcCaption, _T("Drag caption to dock anywhere"));

	CalcLayout();  // Yes, we want align to parent
}


// TODO:  If parent (IceDockingExplorerView) is floating,
//        remove caption and the hide button
void CIceExplorerView::CalcLayout(BOOL bParent)
{
	CRect rcWnd, rcLine, rcBtn, rcBevel, rcBevelLeft, rcBevelRight;
	
	if ( bParent)
	{
		CWnd* pwnd = GetParent();
		ASSERT(pwnd);
		pwnd->GetClientRect(&rcWnd);
	}
	else
		GetClientRect(&rcWnd);

//	rcWnd.SetRect(rcWnd.left, rcWnd.top+1, rcWnd.right, rcWnd.bottom-2);
	CRect rcCaption;
	
	int iCpt = ::GetSystemMetrics(SM_CYMENU);

	if (bSideBevels && ! bFrameBevel)
	{
		rcBevelLeft.SetRect(rcWnd.left, rcWnd.top, rcWnd.left+2, rcWnd.bottom);
		m_stcBevelLeft.MoveWindow(rcBevelLeft);

		rcBevelRight.SetRect(rcWnd.right-2, rcWnd.top, rcWnd.right, rcWnd.bottom);
		m_stcBevelRight.MoveWindow(rcBevelRight);
	}

	if (!bSideBevels && bFrameBevel)
	{
		rcBevel.SetRect(rcWnd.left, rcWnd.top, rcWnd.right, rcWnd.bottom);
		m_stcBevel.MoveWindow(rcBevel);
	}

	rcLine.SetRect(rcWnd.left+1, rcWnd.top+iCpt+2*BORDER, rcWnd.right, rcWnd.top+iCpt+3*BORDER);
	m_stcLine.MoveWindow(rcLine);
	
	if(bHeader)
	{
		rcCaption.SetRect(rcWnd.left+BORDER, rcWnd.top+BORDER, rcWnd.right-BTNX-4*BORDER, rcWnd.top+iCpt+2*BORDER);
		m_stcCaption.MoveWindow(rcCaption);

		rcBtn.SetRect(rcWnd.right-BTNX-4*BORDER, rcWnd.top+2*BORDER, rcWnd.right-2*BORDER, rcCaption.bottom-BORDER);
		m_btn.MoveWindow(rcBtn);
	}
	else  // zero-size rectangles
	{
		rcCaption.SetRect(rcWnd.left+BORDER, rcWnd.top+BORDER, rcWnd.right-BTNX-4*BORDER, rcWnd.top+BORDER);
		m_stcCaption.MoveWindow(rcCaption);

		rcBtn.SetRect(rcWnd.right-BTNX-4*BORDER, rcWnd.top+2*BORDER, rcWnd.right-2*BORDER, rcWnd.top+2*BORDER);
		m_btn.MoveWindow(rcBtn);
	}

	CRect rcTree;
	rcTree.left = rcWnd.left+BORDER;
	rcTree.top = rcCaption.bottom+BORDER;
	rcTree.right = rcWnd.right-BORDER;
	rcTree.bottom = rcWnd.bottom-2*BORDER;

	m_tree.MoveWindow(rcTree);

}

BOOL CIceExplorerView::SubclassDlgItem(UINT nID, CWnd* parent)
{
	if (!CWnd::SubclassDlgItem(nID, parent)) 
		return FALSE;

	Initialize();  // ?????????????

	return TRUE;
}


BEGIN_MESSAGE_MAP(CIceExplorerView, CWnd)
	//{{AFX_MSG_MAP(CIceExplorerView)
	ON_WM_SIZE()
	ON_COMMAND(IDC_BTN_HIDE, OnButtonHide)
	ON_MESSAGE(IEVN_CAPTIONDRAG, OnCaptionDrag)
	ON_MESSAGE(IEVN_CAPTIONDBLCLK, OnCaptionDblClk)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CIceExplorerView diagnostics

#ifdef _DEBUG
void CIceExplorerView::AssertValid() const
{
	CWnd::AssertValid();
}

void CIceExplorerView::Dump(CDumpContext& dc) const
{
	CWnd::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIceExplorerView message handlers

BOOL CIceExplorerView::Create(const RECT& rect, CWnd* pParentWnd, UINT nID, DWORD dwStyle)
{
    ASSERT(pParentWnd->GetSafeHwnd());

	///////////////////////////////?????????????????????????
    if (!CWnd::Create(ICEEXPLORERVIEW_CLASSNAME, NULL, dwStyle, rect, pParentWnd, nID)) 
        return FALSE;

	Initialize();
	return TRUE;
}

BOOL CIceExplorerView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

BOOL CIceExplorerView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class

    m_ToolTip.RelayEvent(pMsg);
	
	return CWnd::PreTranslateMessage(pMsg);
}

void CIceExplorerView::OnSize(UINT nType, int cx, int cy) 
{
	CWnd::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}


void CIceExplorerView::OnButtonHide()
{
	Hide();
}

void CIceExplorerView::OnCaptionDrag()
{
	// send IEVN_CAPTIONDRAG to parent and handle there
	CWnd* pParent = GetParent();
	ASSERT_VALID(pParent);
	if (pParent != NULL)
		pParent->SendMessage(IEVN_CAPTIONDRAG);
}

void CIceExplorerView::OnCaptionDblClk()
{
	// send IEVN_CAPTIONDBLCLK to parent and handle there
	CWnd* pParent = GetParent();
	ASSERT_VALID(pParent);
	if (pParent != NULL)
		pParent->SendMessage(IEVN_CAPTIONDBLCLK);
}

// Hide(FALSE) let show the control again
void CIceExplorerView::Hide(BOOL hide /*TRUE*/)
{
	//CMainFrame* pMainWnd = (CMainFrame*) AfxGetMainWnd();  
	CMainFrame* pMainWnd = (CMainFrame*) AfxGetApp()->m_pMainWnd;
	ASSERT_VALID(pMainWnd);
	if (pMainWnd != NULL)
		pMainWnd->OnBarCheck(ID_VIEW_WORKSPACE_PANE);
}
