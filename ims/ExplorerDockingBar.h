#if !defined(AFX_EXPLORERDOCKINGBAR_H__F07F67E1_5528_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_EXPLORERDOCKINGBAR_H__F07F67E1_5528_11D2_852E_00A024E0E339__INCLUDED_

#include "IceDockingExplorerView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ExplorerDockingBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CExplorerDockingBar window

class CExplorerDockingBar : public CIceDockingExplorerView
{
// Construction
public:
	CExplorerDockingBar();

// Attributes
public:

// Operations
public:


// Overridables
    virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExplorerDockingBar)
	public:
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CExplorerDockingBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CExplorerDockingBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPLORERDOCKINGBAR_H__F07F67E1_5528_11D2_852E_00A024E0E339__INCLUDED_)
