#if !defined(AFX_ICELISTVIEW_H__40CCB920_51FD_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICELISTVIEW_H__40CCB920_51FD_11D2_852E_00A024E0E339__INCLUDED_

#include "ListNode.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceListView view

class CIceListView : public CListView
{
protected:
	CIceListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CIceListView)

// Attributes
public:
	CListNode *m_pRoot;

protected:
	CImageList m_image;
	int m_cxImage,m_cyImage;
	int m_CurSubItem;
	int IndexToOrder(int iIndex);

// Operations
public:
	CEdit* EditLabelEx(int nItem, int nCol);
	void MakeColumnVisible(int nCol);
	void DrawFocusCell(CDC *pDC, int nItem, int iSubItem);
	void InsertNode(const CString& strItem);

protected:
	LPCTSTR MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset);

// Overrides
public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceListView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CIceListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceListView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeydown(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);	
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDeleteitem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICELISTVIEW_H__40CCB920_51FD_11D2_852E_00A024E0E339__INCLUDED_)
