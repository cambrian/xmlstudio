// LogfileView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "LogfileView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLogfileView

IMPLEMENT_DYNCREATE(CLogfileView, CRichEditView)

CLogfileView::CLogfileView()
{
}

CLogfileView::~CLogfileView()
{
}

BEGIN_MESSAGE_MAP(CLogfileView, CRichEditView)
	//{{AFX_MSG_MAP(CLogfileView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogfileView diagnostics

#ifdef _DEBUG
void CLogfileView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CLogfileView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLogfileView message handlers
