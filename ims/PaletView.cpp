// PaletView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "PaletView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPaletView

IMPLEMENT_DYNCREATE(CPaletView, CFormView)

CPaletView::CPaletView()
	: CFormView(CPaletView::IDD)
{
	//{{AFX_DATA_INIT(CPaletView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPaletView::~CPaletView()
{
}

void CPaletView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPaletView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPaletView, CFormView)
	//{{AFX_MSG_MAP(CPaletView)
	ON_WM_SIZE()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPaletView diagnostics

#ifdef _DEBUG
void CPaletView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPaletView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPaletView message handlers

void CPaletView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	
	m_iceListCtrl.SubclassDlgItem(IDC_PALETVIEW_LISTCTRL,this);
	m_iceRichEdit.SubclassDlgItem(IDC_PALETVIEW_RICHEDIT,this);


    LV_COLUMN  lvc;
    lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT; // | LVCF_SUBITEM;
    lvc.iSubItem = 0;

    lvc.pszText = "Name";
    lvc.fmt = LVCFMT_LEFT;
    lvc.cx = 50;
    m_iceListCtrl.InsertColumn(0,&lvc);

    lvc.pszText = "Occupation";
    lvc.fmt = LVCFMT_LEFT;
    lvc.cx = 80;
    m_iceListCtrl.InsertColumn(1,&lvc);

    LV_ITEM  lvi;
    lvi.mask = LVIF_TEXT | LVIF_STATE;
    lvi.iItem = 0;
    lvi.iSubItem = 0;

	lvi.pszText = _T("abc");
	m_iceListCtrl.InsertItem(&lvi);
	m_iceListCtrl.SetItemText(0,1,_T("doctor"));

	lvi.pszText = _T("def");
	m_iceListCtrl.InsertItem(&lvi);
	m_iceListCtrl.SetItemText(0,1,_T("engineer"));


}

void CPaletView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

	int cyGap = 3;
	int cyList = int(0.5*cy) - cyGap;
	//int cyRich = cy - cyList;

	if(m_iceListCtrl.m_hWnd)
		m_iceListCtrl.MoveWindow(0,0,cx,cyList);
	if(m_iceRichEdit.m_hWnd)
	    m_iceRichEdit.MoveWindow(0,cyList+cyGap,cx,cy);
}

int CPaletView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void CPaletView::OnDraw(CDC* pDC) 
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
	
}

BOOL CPaletView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	// ????????????????????
	cs.style &= ~WS_VSCROLL;
	cs.style &= ~WS_HSCROLL;

	return CFormView::PreCreateWindow(cs);
}
