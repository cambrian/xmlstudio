#if !defined(AFX_SCRIPTVIEW_H__15B9B761_56F3_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_SCRIPTVIEW_H__15B9B761_56F3_11D2_852E_00A024E0E339__INCLUDED_

#include "IceShellView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ScriptView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CScriptView view

class CScriptView : public CIceShellView
{
protected: // create from serialization only
	CScriptView();
	DECLARE_DYNCREATE(CScriptView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScriptView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CScriptView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CScriptView)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCRIPTVIEW_H__15B9B761_56F3_11D2_852E_00A024E0E339__INCLUDED_)
