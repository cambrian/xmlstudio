#if !defined(AFX_LEFTDOCKINGBAR_H__56D8A943_521B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_LEFTDOCKINGBAR_H__56D8A943_521B_11D2_852E_00A024E0E339__INCLUDED_

#include "IceDockingSingleView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// LeftDockingBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLeftDockingBar window

class CLeftDockingBar : public CIceDockingSingleView
{
// Construction
public:
	CLeftDockingBar();

// Attributes
public:

protected:


// Operations
public:


// Overridables
    virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeftDockingBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLeftDockingBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CLeftDockingBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEFTDOCKINGBAR_H__56D8A943_521B_11D2_852E_00A024E0E339__INCLUDED_)
