// TreeMenuView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "TreeMenuView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTreeMenuView

IMPLEMENT_DYNCREATE(CTreeMenuView, CIceTreeView)

CTreeMenuView::CTreeMenuView()
{
}

CTreeMenuView::~CTreeMenuView()
{
}


BEGIN_MESSAGE_MAP(CTreeMenuView, CIceTreeView)
	//{{AFX_MSG_MAP(CTreeMenuView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTreeMenuView drawing

void CTreeMenuView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CTreeMenuView diagnostics

#ifdef _DEBUG
void CTreeMenuView::AssertValid() const
{
	CTreeView::AssertValid();
}

void CTreeMenuView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTreeMenuView message handlers

int CTreeMenuView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceTreeView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	//GetTreeCtrl().ModifyStyle(0, 
	//	TVS_HASBUTTONS|TVS_LINESATROOT|TVS_HASLINES|TVS_SHOWSELALWAYS);

	
	return 0;
}

void CTreeMenuView::OnInitialUpdate() 
{
	CIceTreeView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	
	HTREEITEM hRootItem = GetTreeCtrl().InsertItem("Files",0,0);
	
	for (int i=0; i<5; i++)
	{
		CString strItem;
		strItem.Format("File %d", i);
		GetTreeCtrl().InsertItem(strItem,i,i,hRootItem);
	}
	
	GetTreeCtrl().SelectItem(hRootItem);
	GetTreeCtrl().Expand(hRootItem, TVE_EXPAND);

}
