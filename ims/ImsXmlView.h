#if !defined(AFX_IMSXMLVIEW_H__BB9136AC_5797_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_IMSXMLVIEW_H__BB9136AC_5797_11D2_852E_00A024E0E339__INCLUDED_

#include "IceParseView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ImsXmlView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImsXmlView view

class CImsXmlView : public CIceParseView
{
protected:
	CImsXmlView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CImsXmlView)

// Attributes
public:
	CXmlDoc*  GetDocument();

private:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsXmlView)
	public:
	virtual void Serialize(CArchive& ar);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CImsXmlView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CImsXmlView)
	afx_msg void OnViewSource();
	afx_msg void OnUpdateViewSource(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CXmlDoc* CImsXmlView::GetDocument()
   { return (CXmlDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMSXMLVIEW_H__BB9136AC_5797_11D2_852E_00A024E0E339__INCLUDED_)
