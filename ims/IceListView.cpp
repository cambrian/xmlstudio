// IceListView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceListView.h"
#include "ListEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceListView

IMPLEMENT_DYNCREATE(CIceListView, CListView)

CIceListView::CIceListView()
{
	m_CurSubItem = 0;
	m_pRoot=NULL;
	m_cxImage=m_cyImage=0;
}

CIceListView::~CIceListView()
{
	if(m_pRoot!=NULL)
		delete m_pRoot;
}


BEGIN_MESSAGE_MAP(CIceListView, CListView)
	//{{AFX_MSG_MAP(CIceListView)
	ON_WM_CREATE()
	ON_WM_VSCROLL()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(LVN_KEYDOWN, OnKeydown)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnEndlabeledit)
	ON_WM_HSCROLL()
	ON_WM_MEASUREITEM_REFLECT()
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnclick)
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(LVN_DELETEITEM, OnDeleteitem)
	ON_NOTIFY_REFLECT(LVN_BEGINDRAG, OnBegindrag)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


////////////////////////////////////////////////////////////////////
//

BOOL CIceListView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	cs.style |= LVS_REPORT | LVS_SHAREIMAGELISTS
		| LVS_OWNERDRAWFIXED | LVS_SHOWSELALWAYS;
	//cs.style |= LVS_SINGLESEL;  // ?????????????????

	return CListView::PreCreateWindow(cs);
}


/////////////////////////////////////////////////////////////////////////////
// CIceListView drawing

void CIceListView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here

}

/////////////////////////////////////////////////////////////////////////////
// CIceListView diagnostics

#ifdef _DEBUG
void CIceListView::AssertValid() const
{
	CListView::AssertValid();
}

void CIceListView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG



/////////////////////////////////////////////////////////////////////////////
// Helper functions

#define OFFSET_FIRST	2 
#define OFFSET_OTHER	6
void CIceListView::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	//don't you just hate those big ownerdraw functions     
	if (lpDrawItemStruct->CtlType != ODT_LISTVIEW)
        return;

	if(lpDrawItemStruct->itemAction == ODA_DRAWENTIRE)
	{
		LV_ITEM lvi;
		static _TCHAR szBuff[MAX_PATH];
		LPCTSTR pszText;
	
		int nItem = lpDrawItemStruct->itemID;
		CRect rcItem(lpDrawItemStruct->rcItem);
		
		CListCtrl& m_List = GetListCtrl();

		lvi.mask= LVIF_TEXT | LVIF_IMAGE | LVIF_STATE | LVIF_INDENT | LVIF_PARAM; 
		lvi.iItem = nItem;
		lvi.iSubItem=0;
		lvi.pszText=szBuff;
		lvi.cchTextMax=sizeof(szBuff);
		lvi.stateMask=0xFFFF;		
		m_List.GetItem(&lvi);

		CListNode *pSelItem = (CListNode*)lpDrawItemStruct->itemData;
		int nIndent = lvi.iIndent;

		CRect rcLabel;
		m_List.GetItemRect(nItem, rcLabel, LVIR_LABEL);
		
		CDC* pDC = CDC::FromHandle(lpDrawItemStruct->hDC);
		ASSERT(pDC);
		

		CRect rcClipBox;
		pDC->GetClipBox(rcClipBox);

		COLORREF crBackground, crText;
		
		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			// Set the text background and foreground colors
			crBackground = GetSysColor (COLOR_HIGHLIGHT);
			crText = GetSysColor (COLOR_HIGHLIGHTTEXT);
		}
		else
		{
			// Set the text background and foreground colors to the standard window
			// colors
			crBackground = GetSysColor (COLOR_WINDOW);
			crText = GetSysColor (COLOR_WINDOWTEXT);
		}

		CRect rc;
		m_List.GetItemRect(nItem, rc, LVIR_BOUNDS);
		m_pRoot->DrawTreeItem(pDC, pSelItem, nItem, CSize(m_cxImage,m_cyImage), rcItem.top, rcLabel, rc);
		//Get Image
		LV_ITEM lvi1;
		lvi1.mask= LVIF_IMAGE; 
		lvi1.iItem = nItem;
		lvi1.iSubItem=0;
		m_List.GetItem(&lvi1);

		HIMAGELIST himl=(HIMAGELIST)::SendMessage(m_List.m_hWnd, LVM_GETIMAGELIST, (WPARAM)(int)(LVSIL_SMALL), 0L);
		CRect rcIcon;
		m_List.GetItemRect(nItem, rcIcon, LVIR_ICON);
		//Draw Current image
		ImageList_Draw(himl, lvi1.iImage, pDC->m_hDC, rc.left + pSelItem->GetIndentLevel() * m_cxImage, rcIcon.top, ILD_TRANSPARENT);
		//Draw selection bar
		pDC->SetBkColor(crBackground);
		CRect rcClip = lpDrawItemStruct->rcItem;
		rcClip.left += pSelItem->GetIndentLevel() * m_cxImage + m_cxImage+2;
		//rcClip.bottom-=2;// when small font...don't know why..
		ExtTextOut(pDC->GetSafeHdc(), 0, 0, ETO_OPAQUE, rcClip, NULL, 0, NULL);
		/*  // HY: no highlighting!
		//draw the focus cell in first col
		if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
			if(m_CurSubItem==0)
			{
				CRect rect;
				m_List.GetItemRect(0,rect,LVIR_LABEL); ///
				CRect rcClip = lpDrawItemStruct->rcItem;
				rcClip.right=rect.right;
				rcClip.left += pSelItem->GetIndentLevel() * m_cxImage + m_cxImage+2;
				rcClip.bottom-=2;
				//rcClip.OffsetRect(0,-2);
				CBrush br(RGB(192,192,192));
				pDC->FillRect(rcClip,&br);
			}
		}
		*/
		//draw 1. item	
		m_List.GetItemRect(nItem, rcItem, LVIR_LABEL);
		pszText = MakeShortString(pDC, szBuff, rcItem.right - rcItem.left,2*OFFSET_FIRST);
		rcLabel = rcItem;
		rcLabel.left+=OFFSET_FIRST;
		rcLabel.right-=OFFSET_FIRST;

		pDC->SetBkColor (crBackground);
		pDC->SetTextColor (crText);
		pDC->DrawText(pszText,-1, rcLabel,DT_LEFT| DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER | DT_EXTERNALLEADING);
		//pDC->DrawText(pszText,-1, rcLabel,DT_LEFT | DT_END_ELLIPSIS | DT_NOPREFIX | DT_VCENTER | DT_EXTERNALLEADING);
		//draw subitems
		LV_COLUMN lvc;
		lvc.mask = LVCF_FMT | LVCF_WIDTH;
		for(int nColumn=1; m_List.GetColumn(nColumn,&lvc); nColumn++)
		{
			rcItem.left=rcItem.right;
			rcItem.right+=lvc.cx;
			if (rcItem.left < rcClipBox.right && rcItem.right > rcClipBox.left && rcItem.right > rcItem.left)
			{
				pDC->SetBkColor (crBackground);
				pDC->SetTextColor (crText);

				int nRetLen = m_List.GetItemText(nItem, nColumn, szBuff, sizeof(szBuff));
				if(nRetLen==0) continue;
				pszText=MakeShortString(pDC,szBuff,rcItem.right-rcItem.left,2*OFFSET_OTHER);

				UINT nJustify=DT_LEFT;

				if(pszText==szBuff)
				{
					switch(lvc.fmt & LVCFMT_JUSTIFYMASK)
					{
					case LVCFMT_RIGHT:
						nJustify=DT_RIGHT;
						break;
					case LVCFMT_CENTER:
						nJustify=DT_CENTER;
						break;
					default:
						break;
					}
				}
				rcLabel=rcItem;
				rcLabel.left+=OFFSET_OTHER;
				rcLabel.right-=OFFSET_OTHER;

				//  // HY: no highlighting!
				////////////////////////////////
				if (lpDrawItemStruct->itemState & ODS_SELECTED)
					DrawFocusCell(pDC, lpDrawItemStruct->itemID, nColumn);
				////////////////////////////////
				//
				//pDC->DrawText(pszText,-1,rcLabel,nJustify | DT_SINGLELINE | DT_NOPREFIX | DT_NOCLIP | DT_VCENTER|DT_EXTERNALLEADING);
				pDC->DrawText(pszText,-1,rcLabel,nJustify | DT_END_ELLIPSIS | DT_NOPREFIX | DT_VCENTER|DT_EXTERNALLEADING);
			}
		}//for
	}//ODA_DRAWENTIRE
}
	

void CIceListView::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    if (lpMeasureItemStruct->CtlType != ODT_LISTVIEW)
        return;
	lpMeasureItemStruct->itemHeight = m_cyImage+1;
}


//the basic rutine making the ... thing snatched it from some tedious code example some where in MSDN call odlist
LPCTSTR CIceListView::MakeShortString(CDC* pDC, LPCTSTR lpszLong, int nColumnLen, int nOffset)
{
	static const _TCHAR szThreeDots[]=_T("...");
	static _TCHAR szShort[MAX_PATH];

	int nStringLen=lstrlen(lpszLong);
	if(nStringLen==0)
		return(lpszLong);

	if(!strstr(lpszLong,"\r\n"))
	{
		if(pDC->GetTextExtent(lpszLong,nStringLen).cx+nOffset<=nColumnLen)
			return(lpszLong);
		else
			lstrcpy(szShort,lpszLong);
	}
	else
	{
		CString  strLong = lpszLong;
		int i = strLong.Find("\r\n");
		if(i==0)
		{
			lstrcpy(szShort,szThreeDots);
			return szShort;
		}
		strLong = strLong.Left(i);
		char* pLong = strLong.GetBuffer(MAX_PATH);

		strncpy(szShort,pLong,i);
		strLong.ReleaseBuffer();
		nStringLen = i+1;  /// ???????????
	}

	int nAddLen=pDC->GetTextExtent(szThreeDots,sizeof(szThreeDots)).cx;

	for(int i=nStringLen-1; i>0; i--)
	{
		szShort[i]=0;
		if(pDC->GetTextExtent(szShort,i).cx+nOffset+nAddLen<=nColumnLen)
			break;
	}

	lstrcat(szShort,szThreeDots);
	return(szShort);
}



CEdit* CIceListView::EditLabelEx(int nItem, int nCol)
{
	CListCtrl& m_List = GetListCtrl();	  
	// Make sure that the item is visible
	if( !m_List.EnsureVisible(nItem, TRUE)) 
		return NULL;
	// Make sure that nCol is valid
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();

	if( nCol >= nColumnCount || m_List.GetColumnWidth(nCol) < 5)
		return NULL;
	// Get the column offset	
	int offset = 0;	
	for( int i = 0; i < nCol; i++)
	   offset += m_List.GetColumnWidth(i);	

	CRect rect;
	m_List.GetItemRect( nItem, &rect, LVIR_BOUNDS );
	// Now scroll if we need to expose the column	
	CRect rcClient;
	m_List.GetClientRect(&rcClient);
	if( offset + rect.left < 0 || offset + rect.left > rcClient.right)	
	{
		CSize size;		
		size.cx = offset + rect.left;
		size.cy = 0;
		m_List.Scroll(size);//call base
		rect.left -= size.cx;
	}	

	// Get Column alignment	
	LV_COLUMN lvcol;
	lvcol.mask = LVCF_FMT;
	m_List.GetColumn(nCol, &lvcol);

	DWORD dwStyle ;
	if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		dwStyle = ES_LEFT;
	else if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		dwStyle = ES_RIGHT;
	else 
		dwStyle = ES_CENTER;	

	rect.left += offset+4;
	rect.right = rect.left + m_List.GetColumnWidth(nCol) - 3 ;
	if(rect.right > rcClient.right) 
	   rect.right = rcClient.right;

	dwStyle |= WS_BORDER | WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL;
	dwStyle |= ES_MULTILINE | ES_AUTOVSCROLL | WS_VSCROLL; 

	CEdit *pEdit = new CListEditCtrl(nItem, nCol, m_List.GetItemText(nItem, nCol));
	pEdit->Create(dwStyle, rect, this, IDC_LISTVIEWEDITLABEL);	
	return pEdit;
}

void CIceListView::MakeColumnVisible(int nCol)
{
	if(nCol < 0)
		return;

	CListCtrl& m_List = GetListCtrl();	  
	// Get the order array to total the column offset.
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColCount = pHeader->GetItemCount();
	ASSERT( nCol < nColCount);
	int *pOrderarray = new int[nColCount];
	Header_GetOrderArray(pHeader->m_hWnd, nColCount, pOrderarray);
	// Get the column offset
	int offset = 0;
	for(int i = 0; pOrderarray[i] != nCol; i++)
		offset += m_List.GetColumnWidth(pOrderarray[i]);

	int colwidth = m_List.GetColumnWidth(nCol);
	delete[] pOrderarray;

	CRect rect;
	m_List.GetItemRect(0, &rect, LVIR_BOUNDS);
	// Now scroll if we need to expose the column
	CRect rcClient;
	m_List.GetClientRect(&rcClient);
	if(offset + rect.left < 0 || offset + colwidth + rect.left > rcClient.right)
	{
		CSize size;
		size.cx = offset + rect.left;
		size.cy = 0;
		m_List.Scroll( size );
		rect.left -= size.cx;
		m_List.InvalidateRect(NULL);
		m_List.UpdateWindow();
	}
}

int CIceListView::IndexToOrder( int iIndex )
{
	// This translates a column index value to a column order value.
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColCount = pHeader->GetItemCount();
	int *pOrderarray = new int[nColCount];
	Header_GetOrderArray(pHeader->m_hWnd, nColCount, pOrderarray);
	for(int i=0; i<nColCount; i++)
	{
		if(pOrderarray[i] == iIndex )
		{
			delete[] pOrderarray;
			return i;
		}
	}
	delete[] pOrderarray;
	return -1;
}



void CIceListView::DrawFocusCell(CDC *pDC, int nItem, int iSubItem)
{
	
	if(iSubItem==m_CurSubItem)
	{
		CListCtrl& m_List = GetListCtrl();	  
		// Make sure that nCol is valid
		int nCol=iSubItem;
		// Get the column offset	
		int offset = 0;	
		for( int i = 0; i < nCol; i++)
		   offset += m_List.GetColumnWidth(i);	

		CRect rect;
		m_List.GetItemRect(nItem, &rect, LVIR_BOUNDS);
		// Get Column alignment	
		LV_COLUMN lvcol;
		lvcol.mask = LVCF_FMT;
		m_List.GetColumn(nCol, &lvcol);

		DWORD dwStyle ;
		if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
			dwStyle = ES_LEFT;
		else if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
			dwStyle = ES_RIGHT;
		else 
			dwStyle = ES_CENTER;	

		rect.left += offset;
		rect.right = rect.left + m_List.GetColumnWidth(nCol);
		//rect.bottom-=2;// when small font...don't know why...please help here okay
		CBrush br(RGB(192,192,192));
		if(iSubItem==0)
			m_List.GetItemRect(iSubItem,rect,LVIR_LABEL);
		pDC->FillRect(rect,&br);
	}
}


//insert item in listview
void CIceListView::InsertNode(const CString& strItem) 
{
	CListCtrl& m_List = GetListCtrl();
	int nIndex = m_List.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
	if(nIndex!=-1)
	{
		CListNode *pSelItem = (CListNode*)m_List.GetItemData(nIndex);
		if(pSelItem!=NULL)
		{
			LPNODEINFO lp = new NODEINFO;
			lp->m_strItemName = strItem;
			//well if the node being inserted is collapsed ..expand it
			//otherwise the InsertItem may fail..will be fixed in next version
			m_List.SetRedraw(0);
			if (pSelItem != NULL && pSelItem->HasChildren())
			{
				if(pSelItem->IsHidden())
				{
					pSelItem->DoTheOpenCloseThing(pSelItem, nIndex+1, pSelItem->GetIndentLevel());
				}	
			}		
			m_pRoot->InsertItem(pSelItem, lp);
			m_List.SetRedraw(1);
			m_List.InvalidateRect(NULL);
			m_List.UpdateWindow();
		}		
	}
	else  // no selection
	{
		// do nothing....
	}
}



/////////////////////////////////////////////////////////////////////////////
// CIceListView message handlers

void CIceListView::OnInitialUpdate() 
{
	CListView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class

	UpdateWindow();
	
}

BOOL CIceListView::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if(pMsg->message == WM_KEYDOWN)
	{
		if( this == GetFocus())
			{
			switch( pMsg->wParam )
			{
				case VK_LEFT:
					{
						// Decrement the order number.
						m_CurSubItem--;
						if(m_CurSubItem < 0) 
							m_CurSubItem = 0;
						else{
								CListCtrl& m_List = GetListCtrl();	  
								CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
								// Make the column visible.
								// We have to take into account that the header may be reordered.
								MakeColumnVisible( Header_OrderToIndex( pHeader->m_hWnd, m_CurSubItem));
								// Invalidate the item.
								int iItem = m_List.GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);
								if( iItem != -1 )
								{
									CRect rcBounds;
									m_List.GetItemRect(iItem, rcBounds, LVIR_BOUNDS);
									m_List.InvalidateRect(&rcBounds);
									m_List.UpdateWindow();
								}
						}
					}
					return TRUE;
				case VK_RIGHT:
					{
						// Increment the order number.
						m_CurSubItem++;
						CHeaderCtrl* pHeader = (CHeaderCtrl*) GetDlgItem(0);
						int nColumnCount = pHeader->GetItemCount();
						// Don't go beyond the last column.
						if( m_CurSubItem > nColumnCount -1 ) 
							m_CurSubItem = nColumnCount-1;
						else
						{
							CListCtrl& m_List = GetListCtrl();	  
							MakeColumnVisible(Header_OrderToIndex( pHeader->m_hWnd, m_CurSubItem));
							 
							int iItem = m_List.GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);
							// Invalidate the item.
							if( iItem != -1 )
							{
								CRect rcBounds;
								m_List.GetItemRect(iItem, rcBounds, LVIR_BOUNDS);
								m_List.InvalidateRect(&rcBounds);
								m_List.UpdateWindow();
							}
						}
					}
					return TRUE;

				case VK_RETURN:
					{
						CListCtrl& m_List = GetListCtrl();	  
						int iItem = m_List.GetNextItem( -1, LVNI_ALL | LVNI_SELECTED);
						if( m_CurSubItem != -1 && iItem != -1 )
						{
							CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
							int iSubItem = Header_OrderToIndex(pHeader->m_hWnd, m_CurSubItem);
							if(iSubItem==0)
							{
								CRect rcClip;
								m_List.GetItemRect(iItem, rcClip,LVIR_LABEL);
								rcClip.bottom-=2;
								DWORD dwStyle = WS_BORDER | WS_CHILD | WS_VISIBLE 
									//| ES_AUTOHSCROLL | ES_MULTILINE | ES_AUTOVSCROLL | ES_LEFT;
									| ES_AUTOHSCROLL | ES_LEFT;
								CEdit *pEdit = new CListEditCtrl(iItem, iSubItem, m_List.GetItemText(iItem, iSubItem));
								pEdit->Create(dwStyle, rcClip, this, 0x103);	
							}
							else
								EditLabelEx(iItem, iSubItem);	
						}
					}
					break;
				default:
					break;
			}
		}
	}

	return CListView::PreTranslateMessage(pMsg);
}

int CIceListView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CListView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	CListCtrl& m_List = GetListCtrl();
	//DWORD dwStyles = LVS_EX_GRIDLINES|LVS_EX_FLATSB;//|LVS_EX_HEADERDRAGDROP 
	DWORD dwStyles = LVS_EX_GRIDLINES;
	//dwStyles |= LVS_EX_HEADERDRAGDROP;   // do not use this!!!
	dwStyles &= ~(LVS_EDITLABELS);  // ??????
	//ListView_SetExtendedListViewStyleEx(m_List.m_hWnd, dwStyles, dwStyles);
	ListView_SetExtendedListViewStyle(m_List.m_hWnd, dwStyles);
	//associate imagelist with listviewctrl	
	m_image.Create(IDB_LISTITEM,16,1,RGB(255, 0, 255)); // bitmap background: purple
	m_List.SetImageList(&m_image,LVSIL_SMALL);

	HIMAGELIST himl;
	himl = (HIMAGELIST)::SendMessage(m_List.m_hWnd, LVM_GETIMAGELIST, (WPARAM)(int)(LVSIL_SMALL), 0L);
	if (himl)
	{
		ImageList_GetIconSize(himl, &m_cxImage, &m_cyImage);
	}

	//create root ptr	
	m_pRoot =  CListNode::Create(this);


	return 0;
}

void CIceListView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default

	//its not meself
	if( GetFocus() != this) 
		SetFocus();
	
	CListView::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CIceListView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: Add your message handler code here and/or call default

	//its not meself
	if( GetFocus() != this) 
		SetFocus();
	
	CListView::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CIceListView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	LVHITTESTINFO ht;
	ht.pt = point;

	CListCtrl& m_List = GetListCtrl();	  
	// Test for which subitem was clicked.
	ListView_SubItemHitTest(m_List.m_hWnd, &ht);
	// Store the old column number and set the new column value.
	int oldsubitem = m_CurSubItem;

	m_CurSubItem = IndexToOrder(ht.iSubItem);
	
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	// Make the column fully visible.
	// We have to take into account that the columns may be reordered
	MakeColumnVisible(Header_OrderToIndex(pHeader->m_hWnd, m_CurSubItem));
	// Store old state of the item.
	int state = m_List.GetItemState(ht.iItem, LVNI_SELECTED);//LVIS_FOCUSED);
	// Call default left button click is here just before we might bail.
	// Also updates the state of the item.
	CListView::OnLButtonDown(nFlags, point);
	CRect rc;
	m_List.GetItemRect(ht.iItem, rc, LVIR_BOUNDS);
	m_List.InvalidateRect(rc);
	m_List.UpdateWindow();
	if( !state || m_CurSubItem == -1 || oldsubitem != m_CurSubItem ) return;

	//CListView::OnLButtonDown(nFlags, point);
}

void CIceListView::OnKeydown(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_KEYDOWN* pLVKeyDow = (LV_KEYDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	switch(pLVKeyDow->wVKey)
	{
		case VK_DELETE: 
		{
			CListCtrl& m_List = GetListCtrl();
			int nItem = m_List.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
			if(nItem!=-1)
			{
				CListNode*pSelItem=(CListNode*)m_List.GetItemData(nItem);
				if(pSelItem!=NULL)
				{
					m_List.SetRedraw(0);
					m_List.DeleteItem(nItem);//delete cur item in listview
					//delete/hide all children in pSelItem
					pSelItem->HideChildren(pSelItem, TRUE, nItem);
					//delete all internal nodes
					m_pRoot->Delete(pSelItem);
					m_pRoot->UpdateTree();
					m_List.SetItemState(nItem-1, LVIS_SELECTED, LVIS_SELECTED);		
			
					if(nItem-1<0)//no items in list
					{
						m_List.SetRedraw(1);
						return;
					}
					CListNode*pSelItem = (CListNode*)m_List.GetItemData(nItem-1);
					if(pSelItem!=NULL)
					if(!pSelItem->HasChildren())
					{
						LV_ITEM lv;
						lv.mask =  LVIF_IMAGE;
						lv.iItem = nItem-1;
						lv.iSubItem = 0;
						m_List.GetItem(&lv);
						if(lv.iImage!=2)
						{
							lv.iItem = nItem-1;
							lv.iSubItem = 0;
							lv.iImage = 2;//doc icon
							m_List.SetItem(&lv);
						}
					}	
					m_List.SetRedraw(1);
					m_List.InvalidateRect(NULL);
					m_List.UpdateWindow();
				}
			}
		}
		break;

		case VK_ADD:{
						CListCtrl& m_List = GetListCtrl();
						int nIndex = m_List.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
						if(nIndex!=-1)
						{
							CListNode *pSelItem = (CListNode*)m_List.GetItemData(nIndex);
							m_pRoot->Expand(pSelItem, nIndex);
						}
					}
					break;

		case VK_SUBTRACT:{
							CListCtrl& m_List = GetListCtrl();
							int nIndex = m_List.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
							if(nIndex!=-1)
							{
								CListNode *pSelItem = (CListNode*)m_List.GetItemData(nIndex);
								m_pRoot->Collapse(pSelItem);
							}
						}break;
		default :break;
	}

	*pResult = 0;
}

void CIceListView::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here

	CListCtrl& m_List = GetListCtrl();
	int nIndex = m_List.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED); 
	if(nIndex!=-1)
	{
		CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
		int iSubItem = Header_OrderToIndex(pHeader->m_hWnd, m_CurSubItem);
		if(iSubItem==0)
		{
			CListNode *pSelItem = (CListNode*)m_List.GetItemData(nIndex);
			if (pSelItem != NULL && pSelItem->HasChildren())
			{
				m_List.SetRedraw(0);
				pSelItem->DoTheOpenCloseThing(pSelItem, nIndex+1, pSelItem->GetIndentLevel());
				m_pRoot->UpdateTree();
				m_List.SetRedraw(1);
				CRect rc;
				m_List.GetItemRect(nIndex,rc,LVIR_BOUNDS);
				m_List.InvalidateRect(rc);
				m_List.UpdateWindow();
			}
		}
		else
		{
			EditLabelEx(nIndex, iSubItem);
		}
	}
	
	*pResult = 0;
}

void CIceListView::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO  *plvDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here

 	LV_ITEM		 *plvItem = &plvDispInfo->item;
	if (plvItem->pszText != NULL)
	{
		CListCtrl& m_List = GetListCtrl();	  
		CListNode*pSelItem=(CListNode*)m_List.GetItemData(plvItem->iItem);
		if(pSelItem!=NULL)
		{
			LPNODEINFO lp = pSelItem->GetData();
			int nSubItem = plvItem->iSubItem-1; 
			CString str = (CString)plvItem->pszText;
			if(lp!=NULL)
			{
				if(nSubItem==-1)//label
				{
					lp->m_strItemName=str;
				}
				else 
				{
					if(lp->m_SubItems.GetSize())
						lp->m_SubItems.SetAt(nSubItem,str);
					else {
						for(int i=0;i<4;i++)
						{
							if(i==nSubItem)
								lp->m_SubItems.Add(str);
							else
								lp->m_SubItems.Add(_T(""));
						}
					}
				}
			   pSelItem->UpdateData(lp);
			}
			m_List.SetItemText(plvItem->iItem, plvItem->iSubItem, plvItem->pszText);
		}
	}

	*pResult = 0;
}


void CIceListView::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CIceListView::OnSize(UINT nType, int cx, int cy) 
{
	CListView::OnSize(nType, cx, cy);
	
	//its not meself
	if( GetFocus() != this) 
		SetFocus();
}


void CIceListView::OnDeleteitem(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CIceListView::OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}
