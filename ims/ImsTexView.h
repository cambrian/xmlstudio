#if !defined(AFX_IMSTEXVIEW_H__BB9136AD_5797_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_IMSTEXVIEW_H__BB9136AD_5797_11D2_852E_00A024E0E339__INCLUDED_

#include "IceListView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ImsTexView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImsTexView view

class CImsTexView : public CIceListView
{
protected:
	CImsTexView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CImsTexView)

// Attributes
public:
	CTexDoc*  GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsTexView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CImsTexView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CImsTexView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CTexDoc* CImsTexView::GetDocument()
   { return (CTexDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMSTEXVIEW_H__BB9136AD_5797_11D2_852E_00A024E0E339__INCLUDED_)
