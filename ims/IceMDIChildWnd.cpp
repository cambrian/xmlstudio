// IceMDIChildWnd.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceMDIChildWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceMDIChildWnd

IMPLEMENT_DYNCREATE(CIceMDIChildWnd, CMDIChildWnd)

CIceMDIChildWnd::CIceMDIChildWnd()
{
	iceMenu.LoadToolBarResource(IDR_MAINFRAME);
	//iceMenu.AddToolBarResource(IDR_SYSTEMBAR);
}

CIceMDIChildWnd::~CIceMDIChildWnd()
{
}


BEGIN_MESSAGE_MAP(CIceMDIChildWnd, CMDIChildWnd)
	//{{AFX_MSG_MAP(CIceMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_INITMENUPOPUP()
	ON_WM_MENUCHAR()
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceMDIChildWnd message handlers


void CIceMDIChildWnd::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (!iceMenu.DrawItem(lpDrawItemStruct))
		CMDIChildWnd::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CIceMDIChildWnd::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	if (!iceMenu.MeasureItem(lpMeasureItemStruct))
		CMDIChildWnd::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

void CIceMDIChildWnd::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu) 
{
	CMDIChildWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
	iceMenu.RemapMenu(pPopupMenu);	
}

LRESULT CIceMDIChildWnd::OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu) 
{
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL && pSysMenu->GetSafeHmenu() == pMenu->GetSafeHmenu())
	{
		LRESULT lRes = 0;
		iceMenu.FindKeyboardShortcut(nChar, nFlags, pMenu, lRes);
		return lRes;
	}
	return CMDIChildWnd::OnMenuChar(nChar, nFlags, pMenu);
}

int CIceMDIChildWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

void CIceMDIChildWnd::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	// set window size here...
	// if current active child is maximized, maximize this one too

	CMDIChildWnd::ActivateFrame(nCmdShow);
}
