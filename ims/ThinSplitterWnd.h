#if !defined(AFX_THINSPLITTERWND_H__AB6E6D21_5322_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_THINSPLITTERWND_H__AB6E6D21_5322_11D2_852E_00A024E0E339__INCLUDED_

#include "IceSplitterWnd.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ThinSplitterWnd.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// CThinSplitterWnd window

class CThinSplitterWnd : public CIceSplitterWnd
{
// Construction
public:
	CThinSplitterWnd();

// Attributes
public:

private:
	int  m_cxCur;   // width of *second* pane in 2x1
	int  m_cyCur;   // height of *first* pane in 1x2

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThinSplitterWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CThinSplitterWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CThinSplitterWnd)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THINSPLITTERWND_H__AB6E6D21_5322_11D2_852E_00A024E0E339__INCLUDED_)
