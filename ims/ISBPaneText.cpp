// ISBPaneText.cpp: implementation of the ISBPaneText class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ims.h"
#include "ISBPaneText.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ISBPaneText::ISBPaneText()
{
}

ISBPaneText::ISBPaneText(LPCSTR text, int ix)
{
	ASSERT(CIceStatusBar::aktBar);

	index = ix;
	CIceStatusBar::aktBar->SavePane(ix);
	CIceStatusBar::aktBar->SetMode(ix, XSB_TEXT | CIceStatusBar::aktBar->GetMode(ix) & XSB_ALIGN);

	COLORREF fg = CIceStatusBar::aktBar->GetFgColor(ix);
	COLORREF bk = CIceStatusBar::aktBar->GetBkColor(ix);
	CIceStatusBar::aktBar->SetFgColor(ix, fg, fg);		// Gleiche Fg- und Bk-Farbe f�r
	CIceStatusBar::aktBar->SetBkColor(ix, bk, bk);		// On- und Off-Modus

	CIceStatusBar::aktBar->SetText(ix, text, text);	// Gleicher Text in beiden Modi
}


ISBPaneText::~ISBPaneText()
{
	ASSERT(CIceStatusBar::aktBar);

	CIceStatusBar::aktBar->RestorePane(index);
}
