#if !defined(AFX_XSLVIEW_H__BB9136B0_5797_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_XSLVIEW_H__BB9136B0_5797_11D2_852E_00A024E0E339__INCLUDED_

#include "IceRichEditView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// XslView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CXslView view

class CXslView : public CIceRichEditView
{
protected: // create from serialization only
	CXslView();
	DECLARE_DYNCREATE(CXslView)

// Attributes
public:
	CXslDoc*  GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXslView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CXslView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CXslView)
	afx_msg void OnViewSource();
	afx_msg void OnUpdateViewSource(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CXslDoc* CXslView::GetDocument()
   { return (CXslDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XSLVIEW_H__BB9136B0_5797_11D2_852E_00A024E0E339__INCLUDED_)
