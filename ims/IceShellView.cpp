// IceShellView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceShellView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceShellView

IMPLEMENT_DYNCREATE(CIceShellView, CRichEditView)

CIceShellView::CIceShellView()
{
}

CIceShellView::~CIceShellView()
{
}

BEGIN_MESSAGE_MAP(CIceShellView, CRichEditView)
	//{{AFX_MSG_MAP(CIceShellView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceShellView diagnostics

#ifdef _DEBUG
void CIceShellView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CIceShellView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIceShellView message handlers

BOOL CIceShellView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// word-wrap should not be used
	//cs.style |= ES_AUTOHSCROLL;  // but this one doesn't work! why???
	
	return CRichEditView::PreCreateWindow(cs);
}

void CIceShellView::OnInitialUpdate() 
{
	CRichEditView::OnInitialUpdate();
		
	// why does this not work???????????????????
	//DWORD  dwStyle = ES_AUTOHSCROLL;
	//dwStyle |= WS_VSCROLL;
    //dwStyle |= WS_VISIBLE | WS_CHILD;
	//ModifyStyle(0,dwStyle,SWP_NOMOVE);
}

int CIceShellView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CRichEditView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}
