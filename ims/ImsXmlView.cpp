// ImsXmlView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"

#include "XmlDoc.h"
#include "ImsXmlView.h"
#include "XmlView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImsXmlView

IMPLEMENT_DYNCREATE(CImsXmlView, CIceParseView)

CImsXmlView::CImsXmlView()
{
}

CImsXmlView::~CImsXmlView()
{
}


BEGIN_MESSAGE_MAP(CImsXmlView, CIceParseView)
	//{{AFX_MSG_MAP(CImsXmlView)
	ON_COMMAND(ID_VIEW_SOURCE, OnViewSource)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SOURCE, OnUpdateViewSource)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnclick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImsXmlView drawing

void CImsXmlView::OnDraw(CDC* pDC)
{
	CXmlDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CImsXmlView diagnostics

#ifdef _DEBUG
void CImsXmlView::AssertValid() const
{
	CIceParseView::AssertValid();
}

void CImsXmlView::Dump(CDumpContext& dc) const
{
	CIceParseView::Dump(dc);
}

CXmlDoc* CImsXmlView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CXmlDoc)));
	return (CXmlDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImsXmlView message handlers

void CImsXmlView::OnViewSource() 
{
	CXmlDoc*  pDoc = GetDocument();
	if(pDoc->m_bSourcePresent)
	{
		// Raise the source view on top
		for(POSITION pos=pDoc->GetFirstViewPosition();pos!=NULL;)
		{
			CView* pView = pDoc->GetNextView(pos);
			if (pView->IsKindOf(RUNTIME_CLASS(CXmlView)))
			{
				pView->GetParentFrame()->SetWindowPos(NULL,
                0, 0, 0, 0,
				SWP_NOSIZE | SWP_NOMOVE);
				break;
			}
		}
	}
	else
	{
		// create a new source view
		CFrameWnd* pActiveFrame = GetParentFrame();
		CString    strActiveTitle;
		pActiveFrame->GetWindowText(strActiveTitle);

		//CDocTemplate* pTemplate = pDoc->GetDocTemplate();
		CDocTemplate* pTemplate = ((CImsApp*) AfxGetApp())->m_pXmlDocTemplate;
		ASSERT_VALID(pTemplate);
		CFrameWnd* pNewFrame = pTemplate->CreateNewFrame(pDoc, pActiveFrame);
		if (pNewFrame == NULL)
		{
			TRACE0("Warning: failed to create new frame.\n");
			return;     // command failed
		}

		pTemplate->InitialUpdateFrame(pNewFrame, pDoc);
		pDoc->m_bSourcePresent = TRUE;

		// Set window title
		/*
		CString strBaseName, strExt;
		if (!pTemplate->GetDocString(strBaseName, CDocTemplate::docName)
		  || !pTemplate->GetDocString(strExt, CDocTemplate::filterExt))   {
		  AfxThrowUserException(); 
		}
		CString strNewTitle = strBaseName + strExt + " (Source)";
		*/
		CString strNewTitle = strActiveTitle + " (Source)";
		pNewFrame->SetWindowText((LPCTSTR) strNewTitle);
	}
}

void CImsXmlView::OnUpdateViewSource(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CImsXmlView::OnDestroy() 
{
	CIceParseView::OnDestroy();
	
	int nCount = 0;
	CXmlDoc*  pDoc = GetDocument();
	for(POSITION pos=pDoc->GetFirstViewPosition();pos!=NULL;)
	{
		CView* pView = pDoc->GetNextView(pos);
		if (pView->IsKindOf(RUNTIME_CLASS(CImsXmlView)))
		{
			nCount++;
		}
	}
	
	// If this is the last tree view, destory its source view together
	if(nCount<=1 && pDoc->m_bSourcePresent)
	{
		for(POSITION pos=pDoc->GetFirstViewPosition();pos!=NULL;)
		{
			CView* pView = pDoc->GetNextView(pos);
			if (pView->IsKindOf(RUNTIME_CLASS(CXmlView)))
			{
				pView->GetParentFrame()->DestroyWindow();
				break;
			}
		}
	}
}

int CImsXmlView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceParseView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//CListCtrl& m_List = GetListCtrl();

	return 0;
}

void CImsXmlView::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CListCtrl& m_List = GetListCtrl();
    int nCol =  pNMListView->iSubItem;

	if(nCol==0)
	{
		// temporary....
		InsertNode("hi");
	}

	*pResult = 0;
}

void CImsXmlView::Serialize(CArchive& ar) 
{
	// Call the parent!
	CIceParseView::Serialize(ar);
}
