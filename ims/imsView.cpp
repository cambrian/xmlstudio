// imsView.cpp : implementation of the CImsView class
//

#include "stdafx.h"
#include "ims.h"

#include "imsDoc.h"
#include "CntrItem.h"
#include "imsView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImsView

IMPLEMENT_DYNCREATE(CImsView, CIceListView)

BEGIN_MESSAGE_MAP(CImsView, CIceListView)
	//{{AFX_MSG_MAP(CImsView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_COMMAND(ID_OLE_INSERT_NEW, OnInsertObject)
	ON_COMMAND(ID_CANCEL_EDIT_CNTR, OnCancelEditCntr)
	ON_COMMAND(ID_CANCEL_EDIT_SRVR, OnCancelEditSrvr)
	ON_WM_CREATE()
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnclick)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CIceListView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CIceListView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CIceListView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImsView construction/destruction

CImsView::CImsView()
{
	m_pSelection = NULL;
	// TODO: add construction code here

}

CImsView::~CImsView()
{
}

BOOL CImsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CIceListView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CImsView drawing

void CImsView::OnDraw(CDC* pDC)
{
	CImsDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);



	// TODO: add draw code for native data here
	// TODO: also draw all OLE items in the document

	// Draw the selection at an arbitrary position.  This code should be
	//  removed once your real drawing code is implemented.  This position
	//  corresponds exactly to the rectangle returned by CImsCntrItem,
	//  to give the effect of in-place editing.

	// TODO: remove this code when final draw code is complete.

	if (m_pSelection == NULL)
	{
		POSITION pos = pDoc->GetStartPosition();
		m_pSelection = (CImsCntrItem*)pDoc->GetNextClientItem(pos);
	}
	if (m_pSelection != NULL)
		m_pSelection->Draw(pDC, CRect(10, 10, 210, 210));
}

void CImsView::OnInitialUpdate()
{
	CIceListView::OnInitialUpdate();

	// TODO: You may populate your ListView with items by directly accessing
	//  its list control through a call to GetListCtrl().


	// TODO: remove this code when final selection model code is written
	m_pSelection = NULL;    // initialize selection

}

/////////////////////////////////////////////////////////////////////////////
// CImsView printing

BOOL CImsView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CImsView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CImsView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CImsView::OnDestroy()
{
	// Deactivate the item on destruction; this is important
	// when a splitter view is being used.
   CIceListView::OnDestroy();
   COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
   if (pActiveItem != NULL && pActiveItem->GetActiveView() == this)
   {
      pActiveItem->Deactivate();
      ASSERT(GetDocument()->GetInPlaceActiveItem(this) == NULL);
   }
}


/////////////////////////////////////////////////////////////////////////////
// OLE Client support and commands

BOOL CImsView::IsSelected(const CObject* pDocItem) const
{
	// The implementation below is adequate if your selection consists of
	//  only CImsCntrItem objects.  To handle different selection
	//  mechanisms, the implementation here should be replaced.

	// TODO: implement this function that tests for a selected OLE client item

	return pDocItem == m_pSelection;
}

void CImsView::OnInsertObject()
{
	// Invoke the standard Insert Object dialog box to obtain information
	//  for new CImsCntrItem object.
	COleInsertDialog dlg;
	if (dlg.DoModal() != IDOK)
		return;

	BeginWaitCursor();

	CImsCntrItem* pItem = NULL;
	TRY
	{
		// Create new item connected to this document.
		CImsDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		pItem = new CImsCntrItem(pDoc);
		ASSERT_VALID(pItem);

		// Initialize the item from the dialog data.
		if (!dlg.CreateItem(pItem))
			AfxThrowMemoryException();  // any exception will do
		ASSERT_VALID(pItem);

		// If item created from class list (not from file) then launch
		//  the server to edit the item.
		if (dlg.GetSelectionType() == COleInsertDialog::createNewItem)
			pItem->DoVerb(OLEIVERB_SHOW, this);

		ASSERT_VALID(pItem);

		// As an arbitrary user interface design, this sets the selection
		//  to the last item inserted.

		// TODO: reimplement selection as appropriate for your application

		m_pSelection = pItem;   // set selection to last inserted item
		pDoc->UpdateAllViews(NULL);
	}
	CATCH(CException, e)
	{
		if (pItem != NULL)
		{
			ASSERT_VALID(pItem);
			pItem->Delete();
		}
		AfxMessageBox(IDP_FAILED_TO_CREATE);
	}
	END_CATCH

	EndWaitCursor();
}

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the container (not the server) causes the deactivation.
void CImsView::OnCancelEditCntr()
{
	// Close any in-place active item on this view.
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != NULL)
	{
		pActiveItem->Close();
	}
	ASSERT(GetDocument()->GetInPlaceActiveItem(this) == NULL);
}

// Special handling of OnSetFocus and OnSize are required for a container
//  when an object is being edited in-place.
void CImsView::OnSetFocus(CWnd* pOldWnd)
{
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != NULL &&
		pActiveItem->GetItemState() == COleClientItem::activeUIState)
	{
		// need to set focus to this item if it is in the same view
		CWnd* pWnd = pActiveItem->GetInPlaceWindow();
		if (pWnd != NULL)
		{
			pWnd->SetFocus();   // don't call the base class
			return;
		}
	}

	CIceListView::OnSetFocus(pOldWnd);
}

void CImsView::OnSize(UINT nType, int cx, int cy)
{
	CIceListView::OnSize(nType, cx, cy);
	COleClientItem* pActiveItem = GetDocument()->GetInPlaceActiveItem(this);
	if (pActiveItem != NULL)
		pActiveItem->SetItemRects();
}

/////////////////////////////////////////////////////////////////////////////
// OLE Server support

// The following command handler provides the standard keyboard
//  user interface to cancel an in-place editing session.  Here,
//  the server (not the container) causes the deactivation.
void CImsView::OnCancelEditSrvr()
{
	GetDocument()->OnDeactivateUI(FALSE);
}

/////////////////////////////////////////////////////////////////////////////
// CImsView diagnostics

#ifdef _DEBUG
void CImsView::AssertValid() const
{
	CIceListView::AssertValid();
}

void CImsView::Dump(CDumpContext& dc) const
{
	CIceListView::Dump(dc);
}

CImsDoc* CImsView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CImsDoc)));
	return (CImsDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImsView message handlers

int CImsView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceListView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	/*
	GetListCtrl().ModifyStyle(0, 
		LVS_REPORT|LVS_SHOWSELALWAYS);
	*/

	CListCtrl& m_List = GetListCtrl();

	LV_COLUMN   lvColumn;
	TCHAR szString1[2][20] = {_T("Markup"), 
	                          _T("Content")}; 
	//initialize the columns
	lvColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvColumn.fmt = LVCFMT_LEFT;

	lvColumn.cx = 150;
	lvColumn.pszText = szString1[0];
	m_List.InsertColumn(0,&lvColumn);
	lvColumn.cx = 200;
	lvColumn.pszText = szString1[1];
	m_List.InsertColumn(1,&lvColumn);
	
	LV_ITEM lvItem;		
	lvItem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_INDENT | LVIF_PARAM;
	lvItem.pszText = _T("Root");
	lvItem.iImage = 0;
	lvItem.iItem = 0;
	lvItem.lParam=(LPARAM)m_pRoot;
	lvItem.iIndent=0;
	lvItem.iSubItem = 0;
	m_List.InsertItem(&lvItem);
	
	///////////////////////////////////////
	for(int i=0; i<4; i++)
	{
		LPNODEINFO lpNodeInfo1 = new NODEINFO;
		CString strItem;
		strItem.Format("Item %d",i);
		lpNodeInfo1->m_strItemName=strItem;
		CListNode* pParent = m_pRoot->InsertItem(m_pRoot, lpNodeInfo1);
		if(i%2)
		{
			CListNode* pParent1=NULL;
			CListNode* pParent2=NULL;
			for(int x=0; x<5; x++)
			{
				LPNODEINFO lpNodeInfo7 = new NODEINFO;
				CString strItem;
				strItem.Format("Item %d",x);
				lpNodeInfo7->m_strItemName=strItem;
				pParent1=m_pRoot->InsertItem(pParent,lpNodeInfo7);
			}

			for(int y=0; y<3; y++)
			{
				LPNODEINFO lpNodeInfo7 = new NODEINFO;
				CString strItem;
				strItem.Format("Item %d",y);
				lpNodeInfo7->m_strItemName=strItem;
				pParent2=m_pRoot->InsertItem(pParent1,lpNodeInfo7);
			}

			for(int z=0; z<3; z++)
			{
				LPNODEINFO lpNodeInfo7 = new NODEINFO;
				CString strItem;
				strItem.Format("Item %d",z);
				lpNodeInfo7->m_strItemName=strItem;
				m_pRoot->InsertItem(pParent2, lpNodeInfo7);
			}
		}
	}
	////////////////////

	m_pRoot->Collapse(m_pRoot);
	m_pRoot->Expand(m_pRoot,0);


	return 0;
}

void CImsView::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	CListCtrl& m_List = GetListCtrl();
    int nCol =  pNMListView->iSubItem;

	if(nCol==0)
	{
		InsertNode("hi");
	}

	*pResult = 0;
}
