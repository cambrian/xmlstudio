// ISBPaneInfo.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ISBPaneInfo.h"
#include "IceStatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// ISBPaneInfo

ISBPaneInfo::ISBPaneInfo()
{
	progress	= NULL;
	hScrollPos	= 0;
	vScrollPos	= 0;

	SetDefault();
}

ISBPaneInfo::ISBPaneInfo(const ISBPaneInfo& paneInfo)
{
	for (int i = 0; i < 2; i++)
	{
		fgColor[i]	= paneInfo.fgColor[i];
		bkColor[i]	= paneInfo.bkColor[i];
		string[i]	= paneInfo.string[i];
	}

	font			= paneInfo.font;
	mode			= paneInfo.mode;
	progress		= NULL;
	hScrollPos		= paneInfo.hScrollPos;
	vScrollPos		= paneInfo.vScrollPos;
}

ISBPaneInfo::~ISBPaneInfo()
{
	if (progress)	delete progress;
	progress = NULL; 
}

ISBPaneInfo ISBPaneInfo::operator=(const ISBPaneInfo& paneInfo)
{
	for (int i = 0; i < 2; i++)
	{
		fgColor[i]	= paneInfo.fgColor[i];
		bkColor[i]	= paneInfo.bkColor[i];
		string[i]	= paneInfo.string[i];
	}

	font			= paneInfo.font;
	mode			= paneInfo.mode;
	hScrollPos		= paneInfo.hScrollPos;
	vScrollPos		= paneInfo.vScrollPos;

	if (progress)	delete progress;
	progress = NULL; 

	return *this;
}

void ISBPaneInfo::SetDefault()
{
	bkColor[1]	= COLORREF(GetSysColor(COLOR_MENU));
	bkColor[0]	= bkColor[1];

	fgColor[1]	= GetSysColor(COLOR_MENUTEXT);
	fgColor[0]	= RGB(GetRValue(bkColor[1])/2, GetGValue(bkColor[1])/2, GetBValue(bkColor[1])/2);

	mode		= XSB_TEXT | DT_LEFT;
	SetFont(CFont::FromHandle((HFONT) GetStockObject(ANSI_VAR_FONT)));

	if (progress)	delete progress;
	progress = NULL; 
}

void ISBPaneInfo::SetFont(LPCSTR name, int size)
{
	CFont pointFont;
	pointFont.CreatePointFont(size, name);
	pointFont.GetLogFont(&font);
}

void ISBPaneInfo::SetMode(int newMode)
{
	if ((mode = newMode) & XSB_PROGRESS)
	{
		if (!progress)
		{
			UINT style = WS_VISIBLE | WS_CHILD;
			if (mode & XSB_SMOOTH) style |= PBS_SMOOTH;

			progress = new CProgressCtrl();
			progress->Create(style, CRect(0,0,0,0), CIceStatusBar::aktBar, 1);
		}
	}
	else if (progress)
	{
		delete progress;
		progress = NULL; 
	}
}

void ISBPaneInfo::SetFgColor(COLORREF on, COLORREF off)
{
	fgColor[1] = on;

	if (off != -1)
		fgColor[0] = off;
	else
	{
		COLORREF bk = GetSysColor(COLOR_MENU);
		fgColor[0] = RGB(GetRValue(bk)/2, GetGValue(bk)/2, GetBValue(bk)/2);
	}
}

void ISBPaneInfo::SetBkColor(COLORREF on, COLORREF off)
{
	bkColor[1] = on;
	if (off != -1)	bkColor[0] = off;
	else			bkColor[0] = GetSysColor(COLOR_MENU);
}

void ISBPaneInfo::SetBitmap(LPCSTR newOnBitmap, LPCSTR newOffBitmap)
{
	string[1]	= newOnBitmap;
	string[0]	= newOffBitmap;
}

void ISBPaneInfo::SetText(LPCSTR newOnText, LPCSTR newOffText)
{
	string[1]	= newOnText;
	string[0]	= newOffText;
}

void ISBPaneInfo::SetNumber(int newOnNumber, int newOffNumber)
{
	string[1].Format("%d", newOnNumber);
	string[0].Format("%d", newOffNumber);
}

void ISBPaneInfo::HScroll(CRect& rect, int maxWidth, int nullValue)
{
	if (++hScrollPos > maxWidth)	hScrollPos = nullValue;
	rect.left -= hScrollPos;
}

void ISBPaneInfo::VScroll(CRect& rect, int maxHeight, int nullValue)
{
	if (++vScrollPos > maxHeight)	vScrollPos = nullValue;
	rect.top -= vScrollPos;
}

#ifdef _DEBUG
	void ISBPaneInfo::Dump(CDumpContext& dc) const
	{
		dc	<< "FgColor / BkColor / Value:\t" 
			<< fgColor[1] << " - " << fgColor[0] << " / "
			<< bkColor[1] << " - " << bkColor[0] << " / "
			<< string[1]  << " - " << string[0]  << "\n"
			<< "Mode:\t"  << mode  << "\n";
	}

	void ISBPaneInfo::AssertValid() const
	{
		ASSERT((progress == NULL) || _CrtIsValidHeapPointer(progress));
	}

	CDumpContext& AFXAPI operator<<(CDumpContext& dc, ISBPaneInfo& paneInfo)
	{
		paneInfo.Dump(dc);
		return dc;
	}
#endif // _DEBUG
