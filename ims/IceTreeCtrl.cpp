// IceTreeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceTreeCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceTreeCtrl

CIceTreeCtrl::CIceTreeCtrl()
{
	m_clr3DFace			= ::GetSysColor(COLOR_3DFACE);
	m_clr3DShadow		= ::GetSysColor(COLOR_3DSHADOW);
	m_clr3DDkShadow		= ::GetSysColor(COLOR_3DDKSHADOW);
	m_clr3DLight		= ::GetSysColor(COLOR_3DLIGHT);
	m_clr3DHilight		= ::GetSysColor(COLOR_3DHILIGHT);
	m_clrSelTextColor	= ::GetSysColor(COLOR_BTNTEXT);
	m_clrSelBkColor		= m_clr3DHilight;
}

CIceTreeCtrl::~CIceTreeCtrl()
{
}

void CIceTreeCtrl::SetSelBkColor(COLORREF clrSelBkColor) 
{ 
	m_clrSelBkColor = clrSelBkColor;
}

void CIceTreeCtrl::SetSelTextColor(COLORREF clrSelTextColor) 
{ 
	m_clrSelTextColor = clrSelTextColor;
}


//BEGIN_MESSAGE_MAP(CIceTreeCtrl, CTreeCtrl)
BEGIN_MESSAGE_MAP(CIceTreeCtrl, CCoolTreeCtrl)
	//{{AFX_MSG_MAP(CIceTreeCtrl)
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(TVN_SELCHANGING, OnSelchanging)
	ON_NOTIFY_REFLECT(TVN_ENDLABELEDIT, OnEndlabeledit)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceTreeCtrl message handlers

int CIceTreeCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	//if (CTreeCtrl::OnCreate(lpCreateStruct) == -1)
	if (CCoolTreeCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void CIceTreeCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	UINT flags;
	HTREEITEM tiSelected = HitTest(point, &flags);
	if( (flags & TVHT_ONITEMRIGHT)  || (flags & TVHT_ONITEMINDENT) ||
		(flags & TVHT_ONITEM)		|| (flags & TVHT_ONITEMBUTTON) ) 
	{
		// ????
	}


	CCoolTreeCtrl::OnLButtonDown(nFlags, point);
	//CTreeCtrl::OnLButtonDown(nFlags, point);


	// testing....
	if(!ItemHasChildren(tiSelected))
		AfxMessageBox(GetItemText(tiSelected));
	// testing....

}

void CIceTreeCtrl::OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here

	HTREEITEM htiOld = pNMTreeView->itemOld.hItem;
	HTREEITEM htiNew = pNMTreeView->itemNew.hItem;

	if ((htiOld == htiNew) || GetItemState(htiNew, TVIS_EXPANDED) & TVIS_EXPANDED)
		return;

	HTREEITEM htiRoot = NULL;
	HTREEITEM htiChild = NULL;
	htiRoot = GetNextItem(htiRoot, TVGN_ROOT);
	while (htiRoot != NULL)
	{
		if (ItemHasChildren(htiRoot) && (ItemHasChildren(htiNew)))
			Expand(htiRoot, TVE_COLLAPSE);
			
		htiRoot = GetNextItem(htiRoot, TVGN_NEXT);
		
	}
	
	if (ItemHasChildren(htiNew))
		Expand(htiNew, TVE_EXPAND );


	*pResult = 0;
}

void CIceTreeCtrl::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TV_DISPINFO* pTVDispInfo = (TV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here

	TV_ITEM	*ptvItem = &pTVDispInfo->item;
	SetItem(ptvItem);

	*pResult = 0;
}

void CIceTreeCtrl::OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult) 
{

	NMTVCUSTOMDRAW *pTVCD = (NMTVCUSTOMDRAW*)pNMHDR;
	
	CDC* pDC = CDC::FromHandle(pTVCD->nmcd.hdc);

	// The rect for the cell gives correct left and right values.
	CRect rect = pTVCD->nmcd.rc;
	
	// By default set the return value to do the default behavior.
	*pResult = 0;

	
	switch( pTVCD->nmcd.dwDrawStage )
	{
	// First stage (for the whole control)
	case  CDDS_PREPAINT: 
		{		
			*pResult = CDRF_NOTIFYITEMDRAW;
		}
		break;

	// Stage three (called for each subitem of the focused item)
	case CDDS_ITEMPREPAINT: //*//| CDDS_SUBITEM: 
		{
			//*//*pResult = CDRF_NOTIFYSUBITEMDRAW | CDRF_NOTIFYPOSTPAINT;
			*pResult = CDRF_NOTIFYPOSTPAINT;
		}
		break;

	// Stage four (called for each subitem of the focused item)
	case CDDS_ITEMPOSTPAINT: //*//| CDDS_SUBITEM: 
		{
			*pResult = CDRF_SKIPDEFAULT;	
		}
		break;
	
	default: 
		// Stage two handled here. (called for each item)
		if (pTVCD->nmcd.uItemState & CDIS_HOT)
		{
			pDC->Draw3dRect( &rect, m_clr3DHilight, m_clr3DShadow );
			pDC->SetBkColor(pDC->GetBkColor());
			pDC->SetTextColor(RGB(0,0,255));

			// Tell the control that to draw it again.
			*pResult = CDRF_NOTIFYPOSTPAINT;

		}

		if (pTVCD->nmcd.uItemState & CDIS_SELECTED)
		{
			*pResult = CDRF_SKIPDEFAULT;
		}

		if( (pTVCD->nmcd.uItemState & CDIS_FOCUS) )
		{
			pDC->Draw3dRect( &rect, m_clr3DShadow, m_clr3DHilight );
			pDC->SetBkColor(m_clrSelBkColor);
			pDC->SetTextColor(m_clrSelTextColor);

			*pResult = CDRF_NOTIFYPOSTPAINT;
		}	
		
		if( (pTVCD->nmcd.uItemState & CDIS_CHECKED) )
		{
			*pResult = CDRF_DODEFAULT;
		}	
		
		break;
	}
}

void CIceTreeCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	UINT flags;
	HTREEITEM tiSelected = HitTest(point, &flags);
	if( (flags & TVHT_ONITEMRIGHT)  || (flags & TVHT_ONITEMINDENT) ||
		(flags & TVHT_ONITEM)		|| (flags & TVHT_ONITEMBUTTON) ) 
	{
		// ????
	}

	// testing....
	//if(!ItemHasChildren(tiSelected))
	//	AfxMessageBox(GetItemText(tiSelected));
	// this has some problems....

	CCoolTreeCtrl::OnLButtonDblClk(nFlags, point);
	//CTreeCtrl::OnLButtonDblClk(nFlags, point);
}
