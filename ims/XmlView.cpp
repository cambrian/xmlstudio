// XmlView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"

#include "XmlDoc.h"
#include "XmlView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXmlView

IMPLEMENT_DYNCREATE(CXmlView, CIceRichEditView)

CXmlView::CXmlView()
{
}

CXmlView::~CXmlView()
{
}

BEGIN_MESSAGE_MAP(CXmlView, CIceRichEditView)
	//{{AFX_MSG_MAP(CXmlView)
	ON_COMMAND(ID_VIEW_SOURCE, OnViewSource)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SOURCE, OnUpdateViewSource)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXmlView diagnostics

#ifdef _DEBUG
void CXmlView::AssertValid() const
{
	CIceRichEditView::AssertValid();
}

void CXmlView::Dump(CDumpContext& dc) const
{
	CIceRichEditView::Dump(dc);
}

CXmlDoc* CXmlView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CXmlDoc)));
	return (CXmlDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXmlView message handlers

void CXmlView::OnViewSource() 
{
	GetParentFrame()->DestroyWindow();
}

void CXmlView::OnUpdateViewSource(CCmdUI* pCmdUI) 
{
	CMenu* pMenu = pCmdUI->m_pMenu;
	if (pMenu != NULL)
    {
		CString  strMenu("Hide Source");
        pMenu->ModifyMenu(pCmdUI->m_nIndex, MF_BYPOSITION, ID_VIEW_SOURCE, strMenu);
	}

	pCmdUI->Enable(TRUE);
}

void CXmlView::OnDestroy() 
{
	CIceRichEditView::OnDestroy();
	
	// TODO: Add your message handler code here

	CXmlDoc* pDoc = GetDocument();
	if(pDoc)
		pDoc->m_bSourcePresent = FALSE;	
}
