// ListNode.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ListNode.h"
#include "IceListView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListNode
/////////////////////////////////////////////////////////////////////////////////
//
//  Okay my little helper class for the internal structure in the CListView
//  This class will maintain a "tree" like linked list of nodes.
//  I am using it in conjunktion with a CListView, indenting items
//  in Column 0..will create the "illusion" of having a CtreeCtrl
//	in column 0.
//   
//
IMPLEMENT_DYNCREATE(CListNode,CObject);


CListNode::~CListNode()
{
	Empty();
}




// pretty important ...init the root pointer
CListNode* CListNode::Create(CIceListView *pView)
{
	CListNode* pRoot = NULL;
	TRY
	{
		pRoot = new CListNode();
		pRoot->CreateRoot(pView);
	}
	CATCH(CException, e)
	{
		delete pRoot;
		pRoot = NULL;
	}
	END_CATCH
	CListCtrl& m_List = pView->GetListCtrl();		
	pRoot->SetListCtrl(&m_List);
	return pRoot;
}



void CListNode::CreateRoot(CIceListView *pView)
{
	m_lpNodeInfo=NULL;
    Empty(); 
	m_pIceListView = pView;
	m_lpNodeInfo = new NODEINFO;
	m_lpNodeInfo->m_strItemName=_T("Root");
	m_lpNodeInfo->m_SubItems.RemoveAll();
	m_nIndex=0;
	m_nIndent=0;
	m_pParent=NULL;
	m_pList=NULL;
}



BOOL CListNode::Delete(CListNode* pNode)
{
	POSITION pos = m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		POSITION posPrev = pos;
		CListNode *pChild = (CListNode*)m_listChild.GetNext(pos);
		if (pChild == pNode)
		{
			m_listChild.RemoveAt(posPrev);
			if(pNode->m_lpNodeInfo!=NULL)
				delete pNode->m_lpNodeInfo;
			delete pNode;
			return TRUE;
		}
		if (pChild->Delete(pNode) == TRUE)
			return TRUE;
	}
	return FALSE;
}



CListNode* CListNode::FindNode(LPCTSTR pszItemName)
{
	// return this node if it matches!
	if (GetItemName() == pszItemName)
		return this;

	POSITION pos = m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		CListNode* pChild = (CListNode*)m_listChild.GetNext(pos);
		if (pChild->GetItemName() == pszItemName)
			return pChild;
		// recurse down the tree
		pChild = pChild->FindNode(pszItemName);
		if (pChild != NULL)
			return pChild;
	}
	return NULL;
}



void CListNode::Empty()
{
	// delete child nodes
	POSITION pos = m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		CListNode* pItem =(CListNode*)m_listChild.GetNext(pos);
		if(pItem!=NULL)
		{
			if(pItem->m_lpNodeInfo!=NULL)
				delete pItem->m_lpNodeInfo;
			delete pItem;
		}
	}
	m_listChild.RemoveAll();
}



BOOL CListNode::IsChild(const CListNode* pChild) const
{
	if (pChild == this)
		return TRUE;
	POSITION pos = m_listChild.GetHeadPosition();
	while (pos != NULL)
	{
		CListNode* pNode = (CListNode*)m_listChild.GetNext(pos);
		if (pNode->IsChild(pChild))
			return TRUE;
	}
	return FALSE;
}


const CString& CListNode::GetItemName()
{
	return m_lpNodeInfo->m_strItemName;
}


////////////////////////////////////////////////////////////////
// Create new child nodes
CListNode* CListNode::AddChild(LPNODEINFO lpNodeInfo)
{
	CListNode* pNew = NULL;
	TRY
	{
		pNew = new CListNode();
		ASSERT(lpNodeInfo!=NULL);
		// TODO: 
		//this is of course bogus code .. should be initalized from out side this function
		/*
		for(int i=0;i < 4;i++) //#4 here is num columns in listctrl
		{
			CString str;
			str.Format("subItem %d of %s",i,lpNodeInfo->m_strItemName);
			lpNodeInfo->m_SubItems.Add(str);
		}
		*/
		pNew->m_lpNodeInfo = lpNodeInfo;
		pNew->m_pIceListView=m_pIceListView;
		pNew->m_nIndent = m_nIndent+1;
		pNew->SetParent(this);
		// add as the last child
		m_listChild.AddTail(pNew);
	}
	CATCH (CException, e)
	{
		delete pNew;
		pNew = NULL;
	}
	END_CATCH
	return pNew;
}


CListNode* CListNode::GetNext(CListNode* pNode, BOOL bInit)
{
	static BOOL bFound;
	if (bInit)
		bFound = FALSE;
		
	if (pNode == this)
		bFound = TRUE;

	if (!m_bHideChildren)
	{
		POSITION pos = m_listChild.GetHeadPosition();
		while (pos != NULL)
		{
			CListNode* pChild = (CListNode*)m_listChild.GetNext(pos);
			if (bFound)
				return pChild;
			pChild = pChild->GetNext(pNode, FALSE);
			if (pChild != NULL)
				return pChild;
		}
	}
	// if reached top and last level return original
	if (bInit)
		return pNode;
	else
		return NULL;
}



CListNode* CListNode::GetPrev(CListNode* pNode,BOOL bInit)
{
	static CListNode* pPrev;
	if (bInit)
		pPrev = this;

	if (pNode == this)
		return pPrev;

	pPrev = this;

	if (!m_bHideChildren)
	{
		POSITION pos = m_listChild.GetHeadPosition();
		while (pos != NULL)
		{
			CListNode* pCur = (CListNode*)m_listChild.GetNext(pos);
			CListNode* pChild = pCur->GetPrev(pNode, FALSE);
			if (pChild != NULL)
				return pChild;
		}
	}
	if (bInit)
		return pPrev;
	else
		return NULL;
}


// TODO:  Write an overloaded function which inserts Item with subitems!!!!

//insert item and return its pointer.
CListNode* CListNode::InsertItem(LPNODEINFO lpInfo)
{
	CListNode* pChild = m_pIceListView->m_pRoot->InsertItem(this, lpInfo);
	return   pChild;
}

//insert item and return new parent pointer.
CListNode* CListNode::InsertItem(CListNode *pParent, LPNODEINFO lpInfo)
{
	
	CListCtrl *pList = m_pIceListView->m_pRoot->GetListCtrl();
	CListNode *pItem = pParent->AddChild(lpInfo);
	//Get listview index for this new node
	int nIndex = m_pIceListView->m_pRoot->NodeToIndex(pItem);			
	//now set the image for the previus item
	CListNode *pTry = m_pIceListView->m_pRoot->GetPrev(pItem);
	if(pTry!=NULL)
	{
		LV_ITEM lvItem;
		if(pTry->HasChildren())
		{
			if(pTry->m_bHideChildren)
				lvItem.iImage = 1;//close icon
			else
				lvItem.iImage = 0;//open icon
		}
		else
			lvItem.iImage = 2;//doc icon

		lvItem.mask =  LVIF_IMAGE;
		lvItem.iItem = nIndex-1;//previous index of course
		lvItem.iSubItem = 0;
		pList->SetItem(&lvItem);
	}

	CString str = pItem->m_lpNodeInfo->m_strItemName;
	LV_ITEM     lvItem;
	lvItem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_INDENT | LVIF_PARAM;
	lvItem.pszText = str.GetBuffer(1); 
	lvItem.iImage = 2;//make it a doc icon hence it will alway be the last item to be inserted
					  //if another child comes along this icon will be changed in the above code
	//insert item
	lvItem.iItem = nIndex;
	lvItem.iSubItem = 0;
	lvItem.lParam=(LPARAM)pItem;
	lvItem.iIndent = pItem->m_nIndent;
	pList->InsertItem(&lvItem);
	//Get subitems
	int nSize = pItem->m_lpNodeInfo->m_SubItems.GetSize();
	for(int i=0; i< nSize;i++)
	{
	   CString str=pItem->m_lpNodeInfo->m_SubItems.GetAt(i);
	   lvItem.mask = LVIF_TEXT | LVIF_IMAGE;
	   lvItem.iSubItem = i+1;
	   lvItem.pszText=str.GetBuffer(1);
	   pList->SetItem(&lvItem);
	}
	pList->EnsureVisible(pList->GetItemCount(),0);
	m_pIceListView->m_pRoot->UpdateTree();
	return pItem;
}	




void CListNode::UpdateTree()
{
	CListCtrl *pList = m_pIceListView->m_pRoot->GetListCtrl();
	int nItems=pList->GetItemCount();
	for(int nItem=0; nItem < nItems; nItem++)
	{
		CListNode* pItem=(CListNode*)pList->GetItemData(nItem);
		pItem->m_nIndex = nItem;
	}
}



int CListNode::NodeToIndex(CListNode *pNode)
{
	CListNode *pItem=m_pIceListView->m_pRoot;
	int i=0;
	for(;;)
	{
		CListNode *p = m_pIceListView->m_pRoot->GetNext(pItem);	  
		i++;
		if(p==pNode)//found
			return i;
		if(p==pItem)//the end
			return 0;
		else
			pItem=p;//next
	}
	return 0;
}


//Draws current Item of the TreeOutline
void CListNode::DrawTreeItem(CDC* pDC, CListNode* pSelItem, int nListItem, const CSize& imageSize, int yDown, const  CRect& rcLabel, const CRect& rcBounds)
{
    CPen psPen(PS_SOLID, 1, RGB(192,192,192));
    CPen* pOldPen = pDC->SelectObject(&psPen);
	int iIndent = pSelItem->GetIndentLevel();
	int nHalfImage=(imageSize.cx/2);
	while(m_pIceListView->m_pRoot != pSelItem)
	{
		CListNode* pParent = pSelItem->GetParent();
		POSITION pos = pParent->m_listChild.GetTailPosition();
		while(pos!=NULL)
		{
			CListNode *pLastChild = (CListNode*)pParent->m_listChild.GetPrev(pos);
			int nIndex = pLastChild->m_nIndex;				
			//no drawing outside the 1st columns right
			int xLine = rcBounds.left + pLastChild->GetIndentLevel() * imageSize.cx - nHalfImage;
			if(xLine >= rcLabel.right)
				break;

			if(nIndex == nListItem && (pLastChild->GetIndentLevel()==iIndent))
			{
				//draw '-
				pDC->MoveTo(xLine, yDown);
				pDC->LineTo(xLine, yDown+nHalfImage);
				// -
				pDC->MoveTo(xLine, yDown+nHalfImage);
				pDC->LineTo(xLine + nHalfImage, yDown+nHalfImage);
				break;
			}
			else
			if(nIndex > nListItem && (pLastChild->GetIndentLevel()==iIndent))
			{
				//draw |-
				pDC->MoveTo(xLine, yDown+nHalfImage);
				pDC->LineTo(xLine + nHalfImage, yDown + nHalfImage);
				//-
				pDC->MoveTo(xLine, yDown);
				pDC->LineTo(xLine, yDown + imageSize.cy + 1);
				break;
			}
			else
			if(nIndex > nListItem && (pLastChild->GetIndentLevel() < iIndent))
			{
				//draw |
				pDC->MoveTo(xLine, yDown);
				pDC->LineTo(xLine, yDown + imageSize.cy+2);
				break;
			}
		}			
		pSelItem = pParent;//next
	}
	pDC->SelectObject(pOldPen);
}


CListNode *CListNode::Search(CListNode *pNode)
{
	CListNode *pItem=m_pIceListView->m_pRoot;
	for(;;)
	{
		CListNode *p = m_pIceListView->m_pRoot->GetNext(pItem);	  
		if(p==pNode)
			return p;
		if(p==pItem)
			return NULL;
		else
			pItem=p;
	}
	return NULL;
}

//walk all over the place setting the hide/show flag of the nodes.
//it also deletes items from the listviewctrl.
void CListNode::HideChildren(CListNode *pItem, BOOL bHide,int nItem)
{
	CListCtrl *pList = m_pIceListView->m_pRoot->GetListCtrl();
	if(!pItem->m_bHideChildren)
	if (pItem->HasChildren())
	{
		pItem->m_bHideChildren = bHide;
		POSITION pos = pItem->m_listChild.GetHeadPosition();
		while (pos != NULL)
		{
			HideChildren((CListNode *)pItem->m_listChild.GetNext(pos),bHide,nItem+1);
			pList->DeleteItem(nItem);//delete items on the way back from the stack
		}
	}
}




//guess what													  
void CListNode::DoTheOpenCloseThing(CListNode *pSelItem, int nIndex, int iIndent)
{
	CListCtrl *pList = m_pIceListView->m_pRoot->GetListCtrl();	
	if (pSelItem->m_bHideChildren)//expand one level down
	{	
		if (pSelItem->HasChildren())
		{
			LV_ITEM lvItem;
			lvItem.mask =  LVIF_IMAGE | LVIF_INDENT;
			lvItem.iImage = 0;//open icon
			lvItem.iItem = nIndex-1;
			lvItem.iSubItem = 0;
			lvItem.lParam=(LPARAM)pSelItem;
			lvItem.iIndent = iIndent;
			pList->SetItem(&lvItem);

			pSelItem->m_bHideChildren = FALSE;
			//expand children
			POSITION pos = pSelItem->m_listChild.GetHeadPosition();
			while(pos != NULL)
			{
				CListNode* pNextNode = (CListNode*)pSelItem->m_listChild.GetNext(pos);
				CString str = pNextNode->m_lpNodeInfo->m_strItemName;

				LV_ITEM lvItem;
				lvItem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_INDENT | LVIF_PARAM;
				lvItem.pszText =str.GetBuffer(1); 
				if(pNextNode->HasChildren())
				{
					if(pNextNode->m_bHideChildren)
						lvItem.iImage = 1;
					else
						lvItem.iImage = 0;
				}
				else
					lvItem.iImage = 2;

				lvItem.iItem = nIndex;
				lvItem.iSubItem = 0;
				lvItem.lParam=(LPARAM)pNextNode;
				lvItem.iIndent = iIndent+1;
				pList->InsertItem(&lvItem);
				//get subitems
				int nSize = pNextNode->m_lpNodeInfo->m_SubItems.GetSize();
				for(int i=0; i< nSize;i++)
				{
				   CString str=pNextNode->m_lpNodeInfo->m_SubItems.GetAt(i);
				   lvItem.mask = LVIF_TEXT | LVIF_IMAGE;
				   lvItem.iSubItem = i+1;
				   lvItem.pszText=str.GetBuffer(1);
				   pList->SetItem(&lvItem);
				}
				nIndex++;
			}
		}
	}
	else {
		//collapse child
		LV_ITEM lvItem;
		lvItem.mask =  LVIF_IMAGE | LVIF_INDENT;
		lvItem.iImage = 1;//close icon
		lvItem.iItem = nIndex-1;
		lvItem.iSubItem = 0;
		lvItem.lParam=(LPARAM)pSelItem;
		lvItem.iIndent = iIndent;
		pList->SetItem(&lvItem);
		HideChildren(pSelItem, TRUE, nIndex);
	}		
	
}


void CListNode::Collapse(CListNode *pItem)
{
	CListCtrl *pList = m_pIceListView->m_pRoot->GetListCtrl();	
	if(pItem != NULL && pItem->HasChildren())
	{
		pList->SetRedraw(0);
		int nIndex = m_pIceListView->m_pRoot->NodeToIndex(pItem);			
		LV_ITEM lv;
		lv.mask =  LVIF_IMAGE;
		lv.iImage = 1;//close icon
		lv.iItem = nIndex;
		lv.iSubItem = 0;
		pList->SetItem(&lv);
		m_pIceListView->m_pRoot->HideChildren(pItem, TRUE, nIndex+1);
		m_pIceListView->m_pRoot->UpdateTree();
		pList->SetRedraw(1);
	}
}



void CListNode::Expand(CListNode* pSelItem, int nIndex)
{
	CListCtrl *pList = m_pIceListView->m_pRoot->GetListCtrl();
	if(pSelItem->HasChildren() && pSelItem->m_bHideChildren)
	{
		LV_ITEM lvItem;
		lvItem.mask =  LVIF_IMAGE | LVIF_INDENT;
		lvItem.iImage = 0;//open icon
		lvItem.iItem = nIndex;
		lvItem.iSubItem = 0;
		lvItem.lParam=(LPARAM)pSelItem;
		lvItem.iIndent = pSelItem->GetIndentLevel();
		pList->SetItem(&lvItem);

		pSelItem->m_bHideChildren = FALSE;
		//expand children
		POSITION pos = pSelItem->m_listChild.GetHeadPosition();
		while(pos != NULL)
		{
			CListNode* pNextNode = (CListNode*)pSelItem->m_listChild.GetNext(pos);
			CString str = pNextNode->m_lpNodeInfo->m_strItemName;

			LV_ITEM lvItem;
			lvItem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_INDENT | LVIF_PARAM;
			lvItem.pszText =str.GetBuffer(1); 
			if(pNextNode->HasChildren())
			{
				if(pNextNode->m_bHideChildren)
					lvItem.iImage = 1;
				else
					lvItem.iImage = 0;
			}
			else
				lvItem.iImage = 2;

			lvItem.iItem = nIndex + 1;
			lvItem.iSubItem = 0;
			lvItem.lParam=(LPARAM)pNextNode;
			lvItem.iIndent = pSelItem->GetIndentLevel()+1;
			pList->InsertItem(&lvItem);
			//get subitems
			int nSize = pNextNode->m_lpNodeInfo->m_SubItems.GetSize();
			for(int i=0; i< nSize;i++)
			{
			   CString str=pNextNode->m_lpNodeInfo->m_SubItems.GetAt(i);
			   lvItem.mask = LVIF_TEXT | LVIF_IMAGE;
			   lvItem.iSubItem = i+1;
			   lvItem.pszText=str.GetBuffer(1);
			   pList->SetItem(&lvItem);
			}
			nIndex++;
		}
	}
	m_pIceListView->m_pRoot->UpdateTree();
}


void CListNode::UpdateData(LPNODEINFO lpInfo)
{
	if(m_lpNodeInfo!=NULL)
		m_lpNodeInfo=lpInfo;
}
