// TexView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"

#include "TexDoc.h"
#include "TexView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTexView

IMPLEMENT_DYNCREATE(CTexView, CIceRichEditView)

CTexView::CTexView()
{
}

CTexView::~CTexView()
{
}

BEGIN_MESSAGE_MAP(CTexView, CIceRichEditView)
	//{{AFX_MSG_MAP(CTexView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTexView diagnostics

#ifdef _DEBUG
void CTexView::AssertValid() const
{
	CIceRichEditView::AssertValid();
}

void CTexView::Dump(CDumpContext& dc) const
{
	CIceRichEditView::Dump(dc);
}

CTexDoc* CTexView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTexDoc)));
	return (CTexDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTexView message handlers

BOOL CTexView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CIceRichEditView::PreCreateWindow(cs);
}

int CTexView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceRichEditView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}
