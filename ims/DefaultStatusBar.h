#if !defined(AFX_DEFAULTSTATUSBAR_H__E0DF03E2_531B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_DEFAULTSTATUSBAR_H__E0DF03E2_531B_11D2_852E_00A024E0E339__INCLUDED_

//#include "XStatusBar.h"
#include "IceStatusBar.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DefaultStatusBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDefaultStatusBar window

//class CDefaultStatusBar : public XStatusBar
class CDefaultStatusBar : public CIceStatusBar
{
// Construction
public:
	CDefaultStatusBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDefaultStatusBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDefaultStatusBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDefaultStatusBar)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEFAULTSTATUSBAR_H__E0DF03E2_531B_11D2_852E_00A024E0E339__INCLUDED_)
