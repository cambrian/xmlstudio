// ImsXslView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"

#include "XslDoc.h"
#include "ImsXslView.h"
#include "XslView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImsXslView

IMPLEMENT_DYNCREATE(CImsXslView, CIceListView)

CImsXslView::CImsXslView()
{
}

CImsXslView::~CImsXslView()
{
}


BEGIN_MESSAGE_MAP(CImsXslView, CIceListView)
	//{{AFX_MSG_MAP(CImsXslView)
	ON_COMMAND(ID_VIEW_SOURCE, OnViewSource)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SOURCE, OnUpdateViewSource)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImsXslView drawing

void CImsXslView::OnDraw(CDC* pDC)
{
	CXslDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CImsXslView diagnostics

#ifdef _DEBUG
void CImsXslView::AssertValid() const
{
	CIceListView::AssertValid();
}

void CImsXslView::Dump(CDumpContext& dc) const
{
	CIceListView::Dump(dc);
}

CXslDoc* CImsXslView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CXslDoc)));
	return (CXslDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImsXslView message handlers

void CImsXslView::OnViewSource() 
{
	CXslDoc*  pDoc = GetDocument();
	if(pDoc->m_bSourcePresent)
	{
		// Raise the source view on top
		for(POSITION pos=pDoc->GetFirstViewPosition();pos!=NULL;)
		{
			CView* pView = pDoc->GetNextView(pos);
			if (pView->IsKindOf(RUNTIME_CLASS(CXslView)))
			{
				pView->GetParentFrame()->SetWindowPos(NULL,
                0, 0, 0, 0,
				SWP_NOSIZE | SWP_NOMOVE);
				break;
			}
		}
	}
	else
	{
		// create a new source view
		CFrameWnd* pActiveFrame = GetParentFrame();
		CString    strActiveTitle;
		pActiveFrame->GetWindowText(strActiveTitle);

		//CDocTemplate* pTemplate = pDoc->GetDocTemplate();
		CDocTemplate* pTemplate = ((CImsApp*) AfxGetApp())->m_pXslDocTemplate;
		ASSERT_VALID(pTemplate);
		CFrameWnd* pNewFrame = pTemplate->CreateNewFrame(pDoc, pActiveFrame);
		if (pNewFrame == NULL)
		{
			TRACE0("Warning: failed to create new frame.\n");
			return;     // command failed
		}

		pTemplate->InitialUpdateFrame(pNewFrame, pDoc);
		pDoc->m_bSourcePresent = TRUE;

		// Set window title
		/*
		CString strBaseName, strExt;
		if (!pTemplate->GetDocString(strBaseName, CDocTemplate::docName)
		  || !pTemplate->GetDocString(strExt, CDocTemplate::filterExt))   {
		  AfxThrowUserException(); 
		}
		CString strNewTitle = strBaseName + strExt + " (Source)";
		*/
		CString strNewTitle = strActiveTitle + " (Source)";
		pNewFrame->SetWindowText((LPCTSTR) strNewTitle);
	}
}

void CImsXslView::OnUpdateViewSource(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CImsXslView::OnDestroy() 
{
	CIceListView::OnDestroy();
	
	int nCount = 0;
	CXslDoc*  pDoc = GetDocument();
	for(POSITION pos=pDoc->GetFirstViewPosition();pos!=NULL;)
	{
		CView* pView = pDoc->GetNextView(pos);
		if (pView->IsKindOf(RUNTIME_CLASS(CImsXslView)))
		{
			nCount++;
		}
	}
	
	// If this is the last tree view, destory source view together
	if(nCount<=1 && pDoc->m_bSourcePresent)
	{
		for(POSITION pos=pDoc->GetFirstViewPosition();pos!=NULL;)
		{
			CView* pView = pDoc->GetNextView(pos);
			if (pView->IsKindOf(RUNTIME_CLASS(CXslView)))
			{
				pView->GetParentFrame()->DestroyWindow();
				break;
			}
		}
	}
}

int CImsXslView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceListView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	CListCtrl& m_List = GetListCtrl();

	LV_COLUMN   lvColumn;
	TCHAR szString1[2][20] = {_T("Markup"), 
	                          _T("Content")}; 
	//initialize the columns
	lvColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvColumn.fmt = LVCFMT_LEFT;

	lvColumn.cx = 150;
	lvColumn.pszText = szString1[0];
	m_List.InsertColumn(0,&lvColumn);
	lvColumn.cx = 200;
	lvColumn.pszText = szString1[1];
	m_List.InsertColumn(1,&lvColumn);
	
	LV_ITEM lvItem;		
	lvItem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_INDENT | LVIF_PARAM;
	lvItem.pszText = _T("Root");
	lvItem.iImage = 0;
	lvItem.iItem = 0;
	lvItem.lParam=(LPARAM)m_pRoot;
	lvItem.iIndent=0;
	lvItem.iSubItem = 0;
	m_List.InsertItem(&lvItem);
	

	m_pRoot->Collapse(m_pRoot);
	//m_pRoot->Expand(m_pRoot,0);

	return 0;
}
