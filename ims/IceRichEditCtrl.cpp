// IceRichEditCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceRichEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceRichEditCtrl

CIceRichEditCtrl::CIceRichEditCtrl()
{
}

CIceRichEditCtrl::~CIceRichEditCtrl()
{
}


BEGIN_MESSAGE_MAP(CIceRichEditCtrl, CRichEditCtrl)
	//{{AFX_MSG_MAP(CIceRichEditCtrl)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceRichEditCtrl message handlers
