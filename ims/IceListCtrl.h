#if !defined(AFX_ICELISTCTRL_H__40CCB922_51FD_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICELISTCTRL_H__40CCB922_51FD_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceListCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceListCtrl window

class CIceListCtrl : public CListCtrl
{
// Construction
public:
	CIceListCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceListCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceListCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceListCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICELISTCTRL_H__40CCB922_51FD_11D2_852E_00A024E0E339__INCLUDED_)
