# Microsoft Developer Studio Project File - Name="ims" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=ims - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ims.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ims.mak" CFG="ims - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ims - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "ims - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ims - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D XMLTOKAPI=__declspec(dllimport) /D XMLPARSEAPI=__declspec(dllimport) /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x412 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 .\expat\xmltok.lib .\expat\xmlparse.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "ims - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D XMLTOKAPI=__declspec(dllimport) /D XMLPARSEAPI=__declspec(dllimport) /Yu"stdafx.h" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x412 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 .\expat\xmltok.lib .\expat\xmlparse.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ims - Win32 Release"
# Name "ims - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\BottomDockingBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildSpltFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\CntrItem.cpp
# End Source File
# Begin Source File

SOURCE=.\CoolTreeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\DefaultStatusBar.cpp
# End Source File
# Begin Source File

SOURCE=.\DtdDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\DtdView.cpp
# End Source File
# Begin Source File

SOURCE=.\ExplorerDockingBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IceButton.cpp
# End Source File
# Begin Source File

SOURCE=.\IceDockingControlBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IceDockingExplorerView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceDockingSingleView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceDockingTabbedView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceExplorerView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceListCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\IceListView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceMDIChildWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\IceMDIFrameWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\IceMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\IceParseView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceRichEditCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\IceRichEditView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceShellView.cpp
# End Source File
# Begin Source File

SOURCE=.\IceSplitterWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\IceStatic.cpp
# End Source File
# Begin Source File

SOURCE=.\IceStatusBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IceToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IceTreeCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\IceTreeView.cpp
# End Source File
# Begin Source File

SOURCE=.\ims.cpp
# End Source File
# Begin Source File

SOURCE=.\hlp\ims.hpj

!IF  "$(CFG)" == "ims - Win32 Release"

USERDEP__IMS_H="$(ProjDir)\hlp\AfxCore.rtf"	"$(ProjDir)\hlp\AfxPrint.rtf"	
# Begin Custom Build - Making help file...
OutDir=.\Release
ProjDir=.
TargetName=ims
InputPath=.\hlp\ims.hpj

"$(OutDir)\$(TargetName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	call "$(ProjDir)\makehelp.bat"

# End Custom Build

!ELSEIF  "$(CFG)" == "ims - Win32 Debug"

USERDEP__IMS_H="$(ProjDir)\hlp\AfxCore.rtf"	"$(ProjDir)\hlp\AfxPrint.rtf"	
# Begin Custom Build - Making help file...
OutDir=.\Debug
ProjDir=.
TargetName=ims
InputPath=.\hlp\ims.hpj

"$(OutDir)\$(TargetName).hlp" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	call "$(ProjDir)\makehelp.bat"

# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ims.odl
# End Source File
# Begin Source File

SOURCE=.\ims.rc
# End Source File
# Begin Source File

SOURCE=.\imsDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ImsDtdView.cpp
# End Source File
# Begin Source File

SOURCE=.\ImsTagView.cpp
# End Source File
# Begin Source File

SOURCE=.\ImsTextView.cpp
# End Source File
# Begin Source File

SOURCE=.\ImsTexView.cpp
# End Source File
# Begin Source File

SOURCE=.\imsView.cpp
# End Source File
# Begin Source File

SOURCE=.\ImsXmlView.cpp
# End Source File
# Begin Source File

SOURCE=.\ImsXslView.cpp
# End Source File
# Begin Source File

SOURCE=.\IpFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\ISBPaneInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\ISBPaneText.cpp
# End Source File
# Begin Source File

SOURCE=.\LeftDockingBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ListEditCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ListNode.cpp
# End Source File
# Begin Source File

SOURCE=.\LogfileView.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MainToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\OutputView.cpp
# End Source File
# Begin Source File

SOURCE=.\PaletView.cpp
# End Source File
# Begin Source File

SOURCE=.\ParseView.cpp
# End Source File
# Begin Source File

SOURCE=.\RightDockingBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ScriptView.cpp
# End Source File
# Begin Source File

SOURCE=.\SrvrItem.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TexDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\TexView.cpp
# End Source File
# Begin Source File

SOURCE=.\ThinSplitterWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\TreeMenuView.cpp
# End Source File
# Begin Source File

SOURCE=.\XmlDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\XmlView.cpp
# End Source File
# Begin Source File

SOURCE=.\XslDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\XslView.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BottomDockingBar.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\ChildSpltFrm.h
# End Source File
# Begin Source File

SOURCE=.\CntrItem.h
# End Source File
# Begin Source File

SOURCE=.\CoolTreeCtrl.h
# End Source File
# Begin Source File

SOURCE=.\DefaultStatusBar.h
# End Source File
# Begin Source File

SOURCE=.\DtdDoc.h
# End Source File
# Begin Source File

SOURCE=.\DtdView.h
# End Source File
# Begin Source File

SOURCE=.\ExplorerDockingBar.h
# End Source File
# Begin Source File

SOURCE=.\IceButton.h
# End Source File
# Begin Source File

SOURCE=.\IceDockingControlBar.h
# End Source File
# Begin Source File

SOURCE=.\IceDockingExplorerView.h
# End Source File
# Begin Source File

SOURCE=.\IceDockingSingleView.h
# End Source File
# Begin Source File

SOURCE=.\IceDockingTabbedView.h
# End Source File
# Begin Source File

SOURCE=.\IceExplorerView.h
# End Source File
# Begin Source File

SOURCE=.\IceListCtrl.h
# End Source File
# Begin Source File

SOURCE=.\IceListView.h
# End Source File
# Begin Source File

SOURCE=.\IceMDIChildWnd.h
# End Source File
# Begin Source File

SOURCE=.\IceMDIFrameWnd.h
# End Source File
# Begin Source File

SOURCE=.\IceMenu.h
# End Source File
# Begin Source File

SOURCE=.\IceParseView.h
# End Source File
# Begin Source File

SOURCE=.\IceRichEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\IceRichEditView.h
# End Source File
# Begin Source File

SOURCE=.\IceShellView.h
# End Source File
# Begin Source File

SOURCE=.\IceSplitterWnd.h
# End Source File
# Begin Source File

SOURCE=.\IceStatic.h
# End Source File
# Begin Source File

SOURCE=.\IceStatusBar.h
# End Source File
# Begin Source File

SOURCE=.\IceToolBar.h
# End Source File
# Begin Source File

SOURCE=.\IceTreeCtrl.h
# End Source File
# Begin Source File

SOURCE=.\IceTreeView.h
# End Source File
# Begin Source File

SOURCE=.\ims.h
# End Source File
# Begin Source File

SOURCE=.\imsDoc.h
# End Source File
# Begin Source File

SOURCE=.\ImsDtdView.h
# End Source File
# Begin Source File

SOURCE=.\ImsTagView.h
# End Source File
# Begin Source File

SOURCE=.\ImsTextView.h
# End Source File
# Begin Source File

SOURCE=.\ImsTexView.h
# End Source File
# Begin Source File

SOURCE=.\imsView.h
# End Source File
# Begin Source File

SOURCE=.\ImsXmlView.h
# End Source File
# Begin Source File

SOURCE=.\ImsXslView.h
# End Source File
# Begin Source File

SOURCE=.\IpFrame.h
# End Source File
# Begin Source File

SOURCE=.\ISBPaneInfo.h
# End Source File
# Begin Source File

SOURCE=.\ISBPaneText.h
# End Source File
# Begin Source File

SOURCE=.\LeftDockingBar.h
# End Source File
# Begin Source File

SOURCE=.\ListEditCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ListNode.h
# End Source File
# Begin Source File

SOURCE=.\LogfileView.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MainToolBar.h
# End Source File
# Begin Source File

SOURCE=.\OutputView.h
# End Source File
# Begin Source File

SOURCE=.\PaletView.h
# End Source File
# Begin Source File

SOURCE=.\ParseView.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RightDockingBar.h
# End Source File
# Begin Source File

SOURCE=.\ScriptView.h
# End Source File
# Begin Source File

SOURCE=.\SrvrItem.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TexDoc.h
# End Source File
# Begin Source File

SOURCE=.\TexView.h
# End Source File
# Begin Source File

SOURCE=.\ThinSplitterWnd.h
# End Source File
# Begin Source File

SOURCE=.\TreeMenuView.h
# End Source File
# Begin Source File

SOURCE=.\XmlDoc.h
# End Source File
# Begin Source File

SOURCE=.\XmlView.h
# End Source File
# Begin Source File

SOURCE=.\XslDoc.h
# End Source File
# Begin Source File

SOURCE=.\XslView.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\idr_altt.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_dtdt.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_imst.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_text.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_xslt.ico
# End Source File
# Begin Source File

SOURCE=.\res\ims.ico
# End Source File
# Begin Source File

SOURCE=.\res\ims.rc2
# End Source File
# Begin Source File

SOURCE=.\res\imsDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\IToolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ListItem.bmp
# End Source File
# Begin Source File

SOURCE=.\res\MenuChk.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mrufile.bmp
# End Source File
# Begin Source File

SOURCE=.\res\reservoi.bmp
# End Source File
# Begin Source File

SOURCE=.\res\sysbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ThumbTac.bmp
# End Source File
# Begin Source File

SOURCE=.\res\TinyToon.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\WrkSpace.bmp
# End Source File
# End Group
# Begin Group "Help Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\hlp\AfxCore.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxOleCl.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxOleSv.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AfxPrint.rtf
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\AppExit.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Bullet.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw2.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurArw4.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\CurHelp.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCopy.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditCut.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditPast.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\EditUndo.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileNew.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileOpen.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FilePrnt.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\FileSave.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpSBar.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\HlpTBar.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\ims.cnt
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\MakeHelp.bat
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecFirst.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecLast.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecNext.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\RecPrev.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmax.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\ScMenu.bmp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\hlp\Scmin.bmp
# PROP Exclude_From_Build 1
# End Source File
# End Group
# Begin Source File

SOURCE=.\ims.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
