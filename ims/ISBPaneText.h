// ISBPaneText.h: interface for the ISBPaneText class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ISBPANETEXT_H__74150183_553F_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ISBPANETEXT_H__74150183_553F_11D2_852E_00A024E0E339__INCLUDED_

#include "IceStatusBar.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class ISBPaneText  
{
protected:
	int	index;

public:
	ISBPaneText();
	ISBPaneText(LPCSTR text, int ix = 0);

	virtual ~ISBPaneText();

};

#endif // !defined(AFX_ISBPANETEXT_H__74150183_553F_11D2_852E_00A024E0E339__INCLUDED_)
