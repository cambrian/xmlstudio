#if !defined(AFX_PALETVIEW_H__40CCB923_51FD_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_PALETVIEW_H__40CCB923_51FD_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// PaletView.h : header file
//

#include "IceListCtrl.h"
#include "IceRichEditCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CPaletView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CPaletView : public CFormView
{
protected:
	CPaletView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPaletView)

// Form Data
public:
	//{{AFX_DATA(CPaletView)
	enum { IDD = IDD_PALETVIEW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

private:
	CIceListCtrl      m_iceListCtrl;
	CIceRichEditCtrl  m_iceRichEdit;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPaletView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnDraw(CDC* pDC);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPaletView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPaletView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PALETVIEW_H__40CCB923_51FD_11D2_852E_00A024E0E339__INCLUDED_)
