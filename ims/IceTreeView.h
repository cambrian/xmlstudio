#if !defined(AFX_ICETREEVIEW_H__1FA13982_5304_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICETREEVIEW_H__1FA13982_5304_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceTreeView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceTreeView view

class CIceTreeView : public CTreeView
{
protected:
	CIceTreeView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CIceTreeView)

// Attributes
public:

protected:
	COLORREF	m_clr3DFace;
	COLORREF	m_clr3DShadow;
	COLORREF	m_clr3DDkShadow;
	COLORREF	m_clr3DLight;
	COLORREF	m_clr3DHilight;
	COLORREF	m_clrSelBkColor;
	COLORREF	m_clrSelTextColor;

	CPoint	m_point;

// Operations
public:
	void SetSelBkColor(COLORREF clrSelBkColor) { m_clrSelBkColor = clrSelBkColor;}
	void SetSelTextColor(COLORREF clrSelTextColor) { m_clrSelTextColor = clrSelTextColor;}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceTreeView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CIceTreeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceTreeView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICETREEVIEW_H__1FA13982_5304_11D2_852E_00A024E0E339__INCLUDED_)
