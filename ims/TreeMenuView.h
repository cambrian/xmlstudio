#if !defined(AFX_TREEMENUVIEW_H__1FA13983_5304_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_TREEMENUVIEW_H__1FA13983_5304_11D2_852E_00A024E0E339__INCLUDED_

#include "IceTreeView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TreeMenuView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTreeMenuView view

class CTreeMenuView : public CIceTreeView
{
protected:
	CTreeMenuView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CTreeMenuView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTreeMenuView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CTreeMenuView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CTreeMenuView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TREEMENUVIEW_H__1FA13983_5304_11D2_852E_00A024E0E339__INCLUDED_)
