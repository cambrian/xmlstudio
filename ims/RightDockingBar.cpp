// RightDockingBar.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "RightDockingBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRightDockingBar

CRightDockingBar::CRightDockingBar()
{
	// ????????????????????????????????
    m_sizeMin = CSize(10, 10);
    m_sizeHorz = CSize(20, 20);
    m_sizeVert = CSize(20, 20);
    m_sizeFloat = CSize(200, 200);
    m_bTracking = FALSE;
    m_bInRecalcNC = FALSE;
    m_cxEdge = 3;  // = 5 ????????????
    m_bDragShowContent = FALSE;
}

CRightDockingBar::~CRightDockingBar()
{
}


BEGIN_MESSAGE_MAP(CRightDockingBar, CIceDockingSingleView)
	//{{AFX_MSG_MAP(CRightDockingBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
//
void CRightDockingBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}

/////////////////////////////////////////////////////////////////////////////
// CRightDockingBar message handlers

int CRightDockingBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingSingleView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void CRightDockingBar::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingSingleView::OnSize(nType, cx, cy);
		
	// TODO: Add your message handler code here
	
	int bottom = (IsHorzDocked() || IsFloating()) ? cy - 14 : cy - 18;

	// ////////
	CView* pView = GetView();
	if(pView)
	{
		pView->MoveWindow(7, 7, cx - 14, bottom);
	}
	// ////////
}

BOOL CRightDockingBar::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class

	// ???????????????????
	cs.style &= ~WS_VSCROLL;
	cs.style &= ~WS_HSCROLL;
	
	return CIceDockingSingleView::PreCreateWindow(cs);
}

void CRightDockingBar::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
{
	// TODO: Add your message handler code here and/or call default
	
	// ??????????????????????????????
	lpMMI->ptMinTrackSize.x = 10;
	lpMMI->ptMinTrackSize.y = 10;

	//CIceDockingSingleView::OnGetMinMaxInfo(lpMMI);
}
