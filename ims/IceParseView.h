#if !defined(AFX_ICEPARSEVIEW_H__FE59E2A0_5A9B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEPARSEVIEW_H__FE59E2A0_5A9B_11D2_852E_00A024E0E339__INCLUDED_

#include "IceListView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceParseView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceParseView view

class CIceParseView : public CIceListView
{
protected:
	CIceParseView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CIceParseView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceParseView)
	public:
	virtual void Serialize(CArchive& ar);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CIceParseView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceParseView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEPARSEVIEW_H__FE59E2A0_5A9B_11D2_852E_00A024E0E339__INCLUDED_)
