#if !defined(AFX_IMSTAGVIEW_H__EC5C3B60_56E6_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_IMSTAGVIEW_H__EC5C3B60_56E6_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ImsTagView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImsTagView view

class CImsTagView : public CListView
{
protected:
	CImsTagView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CImsTagView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsTagView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CImsTagView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CImsTagView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMSTAGVIEW_H__EC5C3B60_56E6_11D2_852E_00A024E0E339__INCLUDED_)
