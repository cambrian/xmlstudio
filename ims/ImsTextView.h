#if !defined(AFX_IMSTEXTVIEW_H__EC5C3B61_56E6_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_IMSTEXTVIEW_H__EC5C3B61_56E6_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ImsTextView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImsTextView view

class CImsTextView : public CRichEditView
{
protected: // create from serialization only
	CImsTextView();
	DECLARE_DYNCREATE(CImsTextView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsTextView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CImsTextView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CImsTextView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMSTEXTVIEW_H__EC5C3B61_56E6_11D2_852E_00A024E0E339__INCLUDED_)
