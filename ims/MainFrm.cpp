// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "ims.h"

#include "MainFrm.h"
//#include "TreeMenuView.h"
#include "PaletView.h"
#include "ParseView.h"
#include "ScriptView.h"
#include "OutputView.h"
#include "LogfileView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CIceMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CIceMDIFrameWnd)
	ON_COMMAND_EX(ID_VIEW_WORKSPACE_PANE, OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_WORKSPACE_PANE, OnUpdateControlBarMenu)
	ON_COMMAND_EX(ID_VIEW_PALETTE_PANE, OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PALETTE_PANE, OnUpdateControlBarMenu)
	ON_COMMAND_EX(ID_VIEW_OUTPUT_PANE, OnBarCheck)
	ON_UPDATE_COMMAND_UI(ID_VIEW_OUTPUT_PANE, OnUpdateControlBarMenu)
	//{{AFX_MSG_MAP(CMainFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	// Global help commands
	ON_COMMAND(ID_HELP_FINDER, CIceMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CIceMDIFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CIceMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CIceMDIFrameWnd::OnHelpFinder)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
	//iceMenu.LoadToolBarResource(IDR_MAINFRAME);
	iceMenu.AddToolBarResource(IDR_SYSTEMBAR);
	iceMenu.AddToolBarResource(IDR_RESERVOIR);
	iceMenu.AddToolBarResource(IDR_MRUFILE);
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CIceMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndMainToolBar.Create(this,
        WS_CHILD | WS_VISIBLE | CBRS_TOP, ID_VIEW_TOOLBAR) ||
		!m_wndMainToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
	//m_wndMainToolBar.ModifyStyle(0, TBSTYLE_FLAT); 

	/*
	if (!m_wndDefaultStatusBar.Create(this) ||
		!m_wndDefaultStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	*/
	if (!m_wndDefaultStatusBar.CreateStatusBar(this, indicators, sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}


	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndMainToolBar.SetBarStyle(m_wndMainToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndMainToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndMainToolBar);
    m_wndMainToolBar.SetWindowText(_T("Standard Tool Bar"));


	/////  Docking views
	// [1] Workspace menu
	/*
	if (!m_wndLeftDockBar.Create(_T("Workspace Window"), this, CSize(150, 250),
		TRUE, //bHasGripper
		ID_VIEW_WORKSPACE_PANE))
	{
		TRACE0("Failed to create workspace pane\n");
		return -1;      // fail to create
	}
	m_wndLeftDockBar.SetBarStyle(m_wndLeftDockBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndLeftDockBar.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndLeftDockBar, AFX_IDW_DOCKBAR_LEFT);
	// Insert a view window in the docking bar
	m_wndLeftDockBar.CreateView(RUNTIME_CLASS(CTreeMenuView));
	*/
	if (!m_wndExplorerDockBar.Create(_T("Workspace Window"), this, CSize(150, 250),
		FALSE, // bHasGripper should be always FALSE
		ID_VIEW_WORKSPACE_PANE))
	{
		TRACE0("Failed to create workspace pane\n");
		return -1;      // fail to create
	}
	m_wndExplorerDockBar.SetBarStyle(m_wndExplorerDockBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndExplorerDockBar.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndExplorerDockBar, AFX_IDW_DOCKBAR_LEFT);

	// [2] Palet view
	if (!m_wndRightDockBar.Create(_T("Palette Window"), this, CSize(150, 250),
		TRUE, //bHasGripper
		ID_VIEW_PALETTE_PANE))
	{
		TRACE0("Failed to create palette pane\n");
		return -1;      // fail to create
	}
	m_wndRightDockBar.SetBarStyle(m_wndRightDockBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndRightDockBar.EnableDocking(CBRS_ALIGN_LEFT | CBRS_ALIGN_RIGHT);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndRightDockBar, AFX_IDW_DOCKBAR_RIGHT);
	// Insert a view window in the docking bar
	m_wndRightDockBar.CreateView(RUNTIME_CLASS(CPaletView));

	// [3] Output tab windows
	if (!m_wndBottomDockBar.Create(_T("Output Window"), this, CSize(400, 100),
		FALSE, //bHasGripper 
		ID_VIEW_OUTPUT_PANE))
	{
		TRACE0("Failed to create output pane\n");
		return -1;      // fail to create
	}
	m_wndBottomDockBar.SetBarStyle(m_wndBottomDockBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndBottomDockBar.EnableDocking(CBRS_ALIGN_TOP | CBRS_ALIGN_BOTTOM);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndBottomDockBar, AFX_IDW_DOCKBAR_BOTTOM);
	// Add view windows in the docking bar
	m_wndBottomDockBar.AddView(_T("Parse"), RUNTIME_CLASS(CParseView));
	m_wndBottomDockBar.AddView(_T("Script"), RUNTIME_CLASS(CScriptView));
	m_wndBottomDockBar.AddView(_T("Output"), RUNTIME_CLASS(COutputView));
	m_wndBottomDockBar.AddView(_T("Log"), RUNTIME_CLASS(CLogfileView));

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CIceMDIFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CIceMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CIceMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::RecalcLayout(BOOL bNotify) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceMDIFrameWnd::RecalcLayout(bNotify);
}

void CMainFrame::ActivateFrame(int nCmdShow) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CIceMDIFrameWnd::ActivateFrame(nCmdShow);
}
