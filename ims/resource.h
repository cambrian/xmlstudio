//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ims.rc
//
#define IDR_XMLTYPE_SRVR_IP             4
#define IDR_IMSTYPE_SRVR_IP             4
#define IDR_XMLTYPE_SRVR_EMB            5
#define IDR_IMSTYPE_SRVR_EMB            5
#define IDR_XMLTYPE_CNTR_IP             6
#define IDR_IMSTYPE_CNTR_IP             6
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDP_FAILED_TO_CREATE            102
#define IDP_SOCKETS_INIT_FAILED         104
#define ID_VIEW_OUTPUT_PANE             104
#define IDR_MAINFRAME                   128
#define IDR_XMLTYPE                     129
#define IDD_PALETVIEW                   130
#define IDR_SYSTEMBAR                   130
#define IDR_TEXTYPE                     132
#define IDB_MENUCHECK                   133
#define IDB_THUMBTACK                   135
#define IDB_LISTITEM                    143
#define IDB_TINYTOON                    144
#define IDB_WORKSPACE                   145
#define IDR_IMSTYPE                     146
#define IDR_XSLTYPE                     147
#define IDR_DTDTYPE                     148
#define IDR_MRUFILE                     149
#define IDR_RESERVOIR                   150
#define IDC_PALETVIEW_LISTCTRL          1000
#define IDC_PALETVIEW_RICHEDIT          1001
#define ID_CANCEL_EDIT_CNTR             32768
#define ID_CANCEL_EDIT_SRVR             32769
#define ID_VIEW_WORKSPACE_PANE          32771
#define ID_VIEW_PALETTE_PANE            32789
#define ID_VIEW_SPLIT_PALETTE           32795
#define ID_VIEW_SOURCE                  32796

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        151
#define _APS_NEXT_COMMAND_VALUE         32799
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
