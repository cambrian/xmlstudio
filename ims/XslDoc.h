#if !defined(AFX_XSLDOC_H__BB9136A7_5797_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_XSLDOC_H__BB9136A7_5797_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// XslDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CXslDoc document

class CXslDoc : public COleServerDoc
{
protected:
	CXslDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CXslDoc)

// Attributes
public:
	BOOL   m_bSourcePresent;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXslDoc)
	public:
	virtual void OnFinalRelease();
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	protected:
	virtual BOOL OnNewDocument();
	virtual COleServerItem* OnGetEmbeddedItem();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CXslDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CXslDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CXslDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XSLDOC_H__BB9136A7_5797_11D2_852E_00A024E0E339__INCLUDED_)
