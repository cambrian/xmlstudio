// IceDockingSingleView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceDockingSingleView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceDockingSingleView

CIceDockingSingleView::CIceDockingSingleView()
{
	m_pView = NULL;
}

CIceDockingSingleView::~CIceDockingSingleView()
{
}


BEGIN_MESSAGE_MAP(CIceDockingSingleView, CIceDockingControlBar)
	//{{AFX_MSG_MAP(CIceDockingSingleView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



BOOL CIceDockingSingleView::CreateView(CRuntimeClass *pViewClass, CCreateContext *pContext)
{	
#ifdef _DEBUG
	ASSERT_VALID(this);
	ASSERT(pViewClass != NULL);
	ASSERT(pViewClass->IsDerivedFrom(RUNTIME_CLASS(CWnd)));
	ASSERT(AfxIsValidAddress(pViewClass, sizeof(CRuntimeClass), FALSE));
#endif

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
			AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{
		TRACE0("Out of memory creating a view.\n");
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL
		
    ASSERT_KINDOF(CWnd, pWnd);
	ASSERT(pWnd->m_hWnd == NULL);       // not yet created
	

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	CRect rect;
	// Create with the right size and position
	if (!pWnd->Create(NULL, NULL, dwStyle, rect, this, 0, pContext))
	{
		TRACE0("Warning: couldn't create client pane for view.\n");
		// pWnd will be cleaned up by PostNcDestroy
		return FALSE;
	}
	m_pView = (CView*) pWnd;

	// ????????????????????????????????
	//((CFrameWnd *)GetParent())->SetActiveView((CView *)m_pView);

	return TRUE;
}

CView* CIceDockingSingleView::GetView()
{
	return m_pView;
}


/////////////////////////////////////////////////////////////////////////////
// CIceDockingSingleView message handlers

int CIceDockingSingleView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void CIceDockingSingleView::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingControlBar::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

	int bottom = (IsHorzDocked() || IsFloating()) ? cy - 14 : cy - 18;
	if(m_pView != NULL)
		m_pView->MoveWindow(7, 7, cx - 14, bottom);
	
}
