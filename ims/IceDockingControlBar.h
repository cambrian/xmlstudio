#if !defined(AFX_ICEDOCKINGCONTROLBAR_H__56D8A942_521B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEDOCKINGCONTROLBAR_H__56D8A942_521B_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceDockingControlBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceDockingControlBar window

class CIceDockingControlBar : public CControlBar
{
// Construction
public:
	CIceDockingControlBar();

// Attributes
public:
    CSize m_sizeHorz;
    CSize m_sizeVert;
    CSize m_sizeFloat;

protected:
    CSize	m_sizeMin;
    CSize   m_sizeMax;
	CPoint	m_ptOld;
    CRect   m_rectBorder;
    BOOL    m_bTracking;
	BOOL	m_bDragShowContent;
	CSize	m_sizeOld;

    BOOL    m_bInRecalcNC;
    UINT    m_nDockBarID;
    int     m_cxEdge;
    BOOL    m_bHasGripper;
    int     m_cyGripper;
    CRect   m_rectGripper;

// Operations
public:
    BOOL IsHorzDocked() const;
    BOOL IsVertDocked() const;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceDockingControlBar)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
    virtual BOOL Create(LPCTSTR lpszWindowName, CWnd* pParentWnd, CSize sizeDefault, BOOL bHasGripper, UINT nID, DWORD dwStyle = WS_CHILD | WS_VISIBLE | CBRS_TOP);
    virtual CSize CalcFixedLayout(BOOL bStretch, BOOL bHorz);
    virtual CSize CalcDynamicLayout(int nLength, DWORD dwMode);
	//}}AFX_VIRTUAL


// Implementation
public:
	virtual ~CIceDockingControlBar();

protected:
    void StartTracking();
    void StopTracking(BOOL bAccept);
	void OnTrackUpdateSize(CPoint& point);
    void OnTrackInvertTracker();
	virtual CSize CalcMaxSize();
	virtual BOOL QueryDragFullWindows() const;

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceDockingControlBar)
	afx_msg void OnPaint();
	afx_msg void OnNcPaint();
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnWindowPosChanged(WINDOWPOS FAR* lpwndpos);
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp);
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEDOCKINGCONTROLBAR_H__56D8A942_521B_11D2_852E_00A024E0E339__INCLUDED_)
