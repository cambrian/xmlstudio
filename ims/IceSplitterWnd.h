#if !defined(AFX_ICESPLITTERWND_H__AB6E6D20_5322_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICESPLITTERWND_H__AB6E6D20_5322_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceSplitterWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceSplitterWnd window

class CIceSplitterWnd : public CSplitterWnd
{
// Construction
public:
	CIceSplitterWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceSplitterWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceSplitterWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceSplitterWnd)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICESPLITTERWND_H__AB6E6D20_5322_11D2_852E_00A024E0E339__INCLUDED_)
