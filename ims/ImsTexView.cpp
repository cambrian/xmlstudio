// ImsTexView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"

#include "TexDoc.h"
#include "ImsTexView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImsTexView

IMPLEMENT_DYNCREATE(CImsTexView, CIceListView)

CImsTexView::CImsTexView()
{
}

CImsTexView::~CImsTexView()
{
}


BEGIN_MESSAGE_MAP(CImsTexView, CIceListView)
	//{{AFX_MSG_MAP(CImsTexView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImsTexView drawing

void CImsTexView::OnDraw(CDC* pDC)
{
	CTexDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CImsTexView diagnostics

#ifdef _DEBUG
void CImsTexView::AssertValid() const
{
	CIceListView::AssertValid();
}

void CImsTexView::Dump(CDumpContext& dc) const
{
	CIceListView::Dump(dc);
}

CTexDoc* CImsTexView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTexDoc)));
	return (CTexDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImsTexView message handlers
