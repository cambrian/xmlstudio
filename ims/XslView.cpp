// XslView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"

#include "XslDoc.h"
#include "XslView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXslView

IMPLEMENT_DYNCREATE(CXslView, CIceRichEditView)

CXslView::CXslView()
{
}

CXslView::~CXslView()
{
}

BEGIN_MESSAGE_MAP(CXslView, CIceRichEditView)
	//{{AFX_MSG_MAP(CXslView)
	ON_COMMAND(ID_VIEW_SOURCE, OnViewSource)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SOURCE, OnUpdateViewSource)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXslView diagnostics

#ifdef _DEBUG
void CXslView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CXslView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

CXslDoc* CXslView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CXslDoc)));
	return (CXslDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXslView message handlers

void CXslView::OnViewSource() 
{
	GetParentFrame()->DestroyWindow();
}

void CXslView::OnUpdateViewSource(CCmdUI* pCmdUI) 
{
	CMenu* pMenu = pCmdUI->m_pMenu;
	if (pMenu != NULL)
    {
		CString  strMenu("Hide Source");
        pMenu->ModifyMenu(pCmdUI->m_nIndex, MF_BYPOSITION, ID_VIEW_SOURCE, strMenu);
	}

	pCmdUI->Enable(TRUE);
}

void CXslView::OnDestroy() 
{
	CIceRichEditView::OnDestroy();
	
	// TODO: Add your message handler code here

	CXslDoc* pDoc = GetDocument();
	if(pDoc)
		pDoc->m_bSourcePresent = FALSE;	
}
