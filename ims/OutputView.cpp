// OutputView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "OutputView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COutputView

IMPLEMENT_DYNCREATE(COutputView, CRichEditView)

COutputView::COutputView()
{
}

COutputView::~COutputView()
{
}

BEGIN_MESSAGE_MAP(COutputView, CRichEditView)
	//{{AFX_MSG_MAP(COutputView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COutputView diagnostics

#ifdef _DEBUG
void COutputView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void COutputView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// COutputView message handlers
