// ListEditCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ListEditCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListEditCtrl
BEGIN_MESSAGE_MAP(CListEditCtrl, CEdit)
	//{{AFX_MSG_MAP(CListEditCtrl)
	ON_WM_KILLFOCUS()
	ON_WM_NCDESTROY()
	ON_WM_CHAR()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListEditCtrl message handlers

CListEditCtrl::CListEditCtrl(int iItem, int iSubItem, CString sInitText):m_strInitText(sInitText)
{	
	m_iItem = iItem;
	m_iSubItem = iSubItem;
	m_bVK_ESCAPE = 0;
}

BOOL CListEditCtrl::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	// we don't want the first column to be multiline......
	//cs.style |= ES_MULTILINE | ES_AUTOVSCROLL | WS_VSCROLL;

	return CEdit::PreCreateWindow(cs);
}

int CListEditCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)		
		return -1;	

	CFont* font = GetParent()->GetFont();
	SetFont(font);
	SetWindowText(m_strInitText);
	SetFocus();
	CalcInPlaceSize();
	SetSel(0, 0);
	return 0;
}

// TODO:  make this multiline editing
// modify PreTranslateMessage() and OnChar() !!!!!!!!!!!!!
BOOL CListEditCtrl::PreTranslateMessage(MSG* pMsg)
{
/*
	if( pMsg->message == WM_KEYDOWN )	
	{		
		//if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_DELETE 
		//|| pMsg->wParam == VK_ESCAPE || GetKeyState( VK_CONTROL))
		if(pMsg->wParam == VK_DELETE || pMsg->wParam == VK_ESCAPE 
			|| (pMsg->wParam == VK_RETURN && !GetKeyState(VK_CONTROL)))
		{			
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			return 1;
		}	
	}
*/

	if( pMsg->message == WM_KEYDOWN )
    {
		SHORT sKey = GetKeyState( VK_CONTROL);
		if(pMsg->wParam == VK_RETURN
			|| pMsg->wParam == VK_DELETE
			|| pMsg->wParam == VK_ESCAPE
			|| sKey
			)
		{
			::TranslateMessage(pMsg);
			// Strange but true:
			//If the edit control has ES_MULTILINE and ESC
			//is pressed the parent is destroyed if the 
			//message is dispatched.  In this 
			//case the parent is the list control. 
			if( !(GetStyle() & ES_MULTILINE) || pMsg->wParam != VK_ESCAPE )
			{
			::DispatchMessage(pMsg);
			}
			return TRUE;                    // DO NOT process further
		}
    }
	
	return CEdit::PreTranslateMessage(pMsg);
}


void CListEditCtrl::OnKillFocus(CWnd* pNewWnd)
{	
	CEdit::OnKillFocus(pNewWnd);

	CString str;	GetWindowText(str);

	// Send Notification to parent of ListView ctrl	
	LV_DISPINFO lvDispInfo;
	lvDispInfo.hdr.hwndFrom = GetParent()->m_hWnd;
	lvDispInfo.hdr.idFrom = GetDlgCtrlID();	
	lvDispInfo.hdr.code = LVN_ENDLABELEDIT;
	lvDispInfo.item.mask = LVIF_TEXT;	
	lvDispInfo.item.iItem = m_iItem;
	lvDispInfo.item.iSubItem = m_iSubItem;
	lvDispInfo.item.pszText = m_bVK_ESCAPE ? NULL : LPTSTR((LPCTSTR)str);
	lvDispInfo.item.cchTextMax = str.GetLength();
	GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(),(LPARAM)&lvDispInfo);
	DestroyWindow();
}


void CListEditCtrl::OnNcDestroy()
{
	CEdit::OnNcDestroy();	
	delete this;
}


void CListEditCtrl::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if( nChar == VK_ESCAPE || nChar == VK_RETURN)	
	{		
		if( nChar == VK_ESCAPE)
			m_bVK_ESCAPE = 1;		
		GetParent()->SetFocus();
		return;	
	}

	CEdit::OnChar(nChar, nRepCnt, nFlags);
	
	// Resize edit control if needed
	CalcInPlaceSize();
}

void CListEditCtrl::CalcInPlaceSize()
{
	// Resize edit control if needed
	// Get text extent	
	CString str;	
	GetWindowText( str );	
	CWindowDC dc(this);
	CFont *pFont = GetParent()->GetFont();
	CFont *pFontDC = dc.SelectObject(pFont);
	CSize size;
	/*
	CSize size = dc.GetTextExtent(str);	
	dc.SelectObject(pFontDC);
	size.cx += 5; // add some extra buffer	
	*/

	// Get client rect
	CRect rect, rcParent;	
	GetClientRect(&rect);
	GetParent()->GetClientRect(&rcParent);

	// Transform rect to parent coordinates	
	ClientToScreen(&rect);
	GetParent()->ScreenToClient(&rect);


	if(str.IsEmpty())  // empty cell... (always multiline in our case)
	{
		size.cx = rect.Width();
		size.cy = rect.Height();
	}
	else if( !(GetStyle() & ES_MULTILINE ) )
	{
		size = dc.GetTextExtent( str );
		size.cy = rect.Height();  // single line edit...
		if(size.cx <= rect.Width())
			size.cx = rect.Width();
		else
		{
			if( size.cx + rect.left < rcParent.right-2 )
				rect.right = rect.left + size.cx;
			else
				rect.right = rcParent.right-2;
			//MoveWindow( &rect );
		}
		MoveWindow( &rect );
	}
	else
	{
		CRect rcTemp( rect );
		rcTemp.right = 100000;  // big number
		int cyHeight = dc.DrawText( str, &rcTemp, DT_CALCRECT|DT_NOPREFIX|DT_LEFT|DT_EXPANDTABS);
		size.cx = rect.Width();
		size.cy = cyHeight + 5;

		if( cyHeight > rect.Height() )
		{
			if( size.cy + rect.top < rcParent.bottom-2 )
				rect.bottom = rect.top + size.cy;
			else
			{
				rect.bottom = rcParent.bottom-2;
				//ShowScrollBar( SB_VERT );
			}
			rect.right += ::GetSystemMetrics(SM_CXVSCROLL);
			MoveWindow( &rect );
		}
	}

	dc.SelectObject( pFontDC );  // ??????????????
}



