#if !defined(AFX_MAINTOOLBAR_H__E0DF03E0_531B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_MAINTOOLBAR_H__E0DF03E0_531B_11D2_852E_00A024E0E339__INCLUDED_

#include "IceToolBar.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// MainToolBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMainToolBar window

//class CMainToolBar : public CToolBar
class CMainToolBar : public CIceToolBar
{
// Construction
public:
	CMainToolBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainToolBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainToolBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMainToolBar)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINTOOLBAR_H__E0DF03E0_531B_11D2_852E_00A024E0E339__INCLUDED_)
