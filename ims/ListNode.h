#if !defined(AFX_LISTNODE_H__EDFF2940_54B2_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_LISTNODE_H__EDFF2940_54B2_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ListNode.h : header file
//


// TODO: Include subitem information and node types, etc...

enum  ListNodeType {LNT_ROOT,       // [list root]
                    LNT_PROLOG,     // [list prolog: Parent of PI's]
                    LNT_DOCUMENT,   // DOM document root
                    //LNT_DOCUMEMNTFRAGMENT, 
                    //LNT_CHARACTERDATA,
                    LNT_PORCESSINGINSTRUCTIONS,
                    LNT_NOTATION,
                    LNT_COMMENT,
                    LNT_DOCUMENTTYPE,
                    LNT_ENTITY,
                    LNT_ENTITYREFERENCE,
                    LNT_ELEMENT,
                    LNT_TEXT,
                    LNT_CDATASECTION,
					LNT_ATTR,
					LNT_EPILOG};    // [list epilog: Parent of PI's]

typedef struct _tagNodeInfo
{
	CString      m_strItemName;  //first item
	ListNodeType m_nodeType;
	CStringArray m_SubItems;
}NODEINFO, *LPNODEINFO;

class  CIceListView;

/////////////////////////////////////////////////////////////////////////////
// CListNode

class CListNode : public CObject
{
	DECLARE_DYNCREATE(CListNode);

public:
	CListNode() : m_pIceListView(NULL),m_pParent(NULL),m_pList(NULL),m_bHideChildren(0),
				m_nIndex(-1),m_nIndent(-1){};

// Attributes
public:

// Operations
public:

	//the arg "CIceListView *pView" here is only for use with my viewclass, could have been a regular CListCtrl used in a dialog 
	static CListNode* Create(CIceListView *pView);//doesn't have to be static 
	void DoTheOpenCloseThing(CListNode *pSelItem, int nIndex, int iIndent);
	void HideChildren(CListNode *pItem, BOOL bHide, int iIndent);
	void UpdateTree(void);
	void DrawTreeItem(CDC* pDC, CListNode* pSelItem, int nListItem, const CSize& imageSize, int yDown, const  CRect& rcLabel, const CRect& rcBounds);
	void Collapse(CListNode *pItem);
	void Expand(CListNode* pSelItem, int nIndex);
	BOOL HasChildren() const { return m_listChild.GetCount() != 0;}
	BOOL IsChild(const CListNode* pChild) const;
	BOOL Delete(CListNode *pItem);// find pItem, remove it and all of its children
	BOOL IsHidden(){return m_bHideChildren;}
	int GetIndentLevel(void){return m_nIndent;}
	int GetCurIndex(void){return m_nIndex;}
	int NodeToIndex(CListNode *pNode);//get listviewindex from pNode
	CListNode* GetNext(CListNode *pItem, BOOL bInit = TRUE);
	CListNode* GetPrev(CListNode *pItem, BOOL bInit = TRUE);
	CListNode* FindNode(LPCTSTR pszItemName);//search by itemname
	//Insert item and return new parent node.
	CListNode* InsertItem(CListNode *pParent, LPNODEINFO lpInfo);
	CListNode* InsertItem(LPNODEINFO lpInfo);
	CListNode *Search(CListNode *pNode);//search by nodeptr
	void SetParent(CListNode* pParent){m_pParent=pParent;};
	CListNode* GetParent(void) {return m_pParent;};	
	const CString& GetItemName();//return first item text..not very usefull...what ever
	LPNODEINFO GetData(void) {return m_lpNodeInfo;}
	void UpdateData(LPNODEINFO lpInfo);
	CListCtrl* GetListCtrl(void){return m_pList;}
	void SetListCtrl(CListCtrl *pList){m_pList=pList;}


// Implementation
public:
	virtual ~CListNode();

private:
	CListNode* AddChild(LPNODEINFO lpNodeInfo);
	void CreateRoot(CIceListView *pView);
	void Empty();
	LPNODEINFO m_lpNodeInfo;
	CObList m_listChild;// list of children
	BOOL m_bHideChildren;
	int m_nIndex;
	int m_nIndent;
	CIceListView* m_pIceListView;
    CListNode* m_pParent;
	CListCtrl* m_pList;
};

/////////////////////////////////////////////////////////////////////////////


#endif // !defined(AFX_LISTNODE_H__EDFF2940_54B2_11D2_852E_00A024E0E339__INCLUDED_)
