#if !defined(AFX_IMSXSLVIEW_H__BB9136AE_5797_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_IMSXSLVIEW_H__BB9136AE_5797_11D2_852E_00A024E0E339__INCLUDED_

#include "IceListView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ImsXslView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImsXslView view

class CImsXslView : public CIceListView
{
protected:
	CImsXslView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CImsXslView)

// Attributes
public:
	CXslDoc*  GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsXslView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CImsXslView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CImsXslView)
	afx_msg void OnViewSource();
	afx_msg void OnUpdateViewSource(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CXslDoc* CImsXslView::GetDocument()
   { return (CXslDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMSXSLVIEW_H__BB9136AE_5797_11D2_852E_00A024E0E339__INCLUDED_)
