// XmlDoc.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "XmlDoc.h"

#include "ImsXmlView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXmlDoc

IMPLEMENT_DYNCREATE(CXmlDoc, COleServerDoc)

CXmlDoc::CXmlDoc()
{
	EnableAutomation();

	m_bSourcePresent = FALSE;
}

BOOL CXmlDoc::OnNewDocument()
{
	if (!COleServerDoc::OnNewDocument())
		return FALSE;
	return TRUE;
}

CXmlDoc::~CXmlDoc()
{
}

void CXmlDoc::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	COleServerDoc::OnFinalRelease();
}

COleServerItem* CXmlDoc::OnGetEmbeddedItem()
{
	// OnGetEmbeddedItem is called by the framework to get the COleServerItem
	//  that is associated with the document.  It is only called when necessary.

	// Instead of returning NULL, return a pointer to a new COleServerItem
	//  derived class that is used in conjunction with this document, then
	//  remove the ASSERT(FALSE) below.
	//  (i.e., return new CMyServerItem.)
	ASSERT(FALSE);			// remove this after completing the TODO
	return NULL;
}


BEGIN_MESSAGE_MAP(CXmlDoc, COleServerDoc)
	//{{AFX_MSG_MAP(CXmlDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CXmlDoc, COleServerDoc)
	//{{AFX_DISPATCH_MAP(CXmlDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IXmlDoc to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {BB9136A0-5797-11D2-852E-00A024E0E339}
static const IID IID_IXmlDoc =
{ 0xbb9136a0, 0x5797, 0x11d2, { 0x85, 0x2e, 0x0, 0xa0, 0x24, 0xe0, 0xe3, 0x39 } };

BEGIN_INTERFACE_MAP(CXmlDoc, COleServerDoc)
	INTERFACE_PART(CXmlDoc, IID_IXmlDoc, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXmlDoc diagnostics

#ifdef _DEBUG
void CXmlDoc::AssertValid() const
{
	COleServerDoc::AssertValid();
}

void CXmlDoc::Dump(CDumpContext& dc) const
{
	COleServerDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXmlDoc serialization

void CXmlDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{	// storing code

		/*
		CString strOut;
        strOut = "empty \n"; 
        ar.WriteString(strOut);
		*/

	}
	else
	{	// loading code

		/*
		char InBuff[10000]; // temporary !!!!!!!!!!!!!!!!!
		int nBuff = ar.Read(InBuff,10000);
		InBuff[nBuff] = '\0';
		CString strIn(InBuff);
		strIn.TrimLeft();
		strIn.TrimRight();
		AfxMessageBox(strIn);
		*/

	}


    // ????????
    ((CImsXmlView*) m_viewList.GetHead())->Serialize(ar);

}

/////////////////////////////////////////////////////////////////////////////
// CXmlDoc commands
