#if !defined(AFX_ICEBUTTON_H__F8C3EA40_551C_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEBUTTON_H__F8C3EA40_551C_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceButton window

class CIceButton : public CButton
{
// Construction
public:
	CIceButton();

// Attributes
public:
	BOOL m_bLBtnDown;
	COLORREF m_clrHotText;

protected:
		COLORREF m_clrFace;
		COLORREF m_clrText;
		CString strText;
		CRect m_rect;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceButton)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEBUTTON_H__F8C3EA40_551C_11D2_852E_00A024E0E339__INCLUDED_)
