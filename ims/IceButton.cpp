// IceButton.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceButton

CIceButton::CIceButton()
{
	m_clrText		= GetSysColor (COLOR_BTNTEXT);
	m_clrFace		= GetSysColor (COLOR_BTNFACE);
	m_clrHotText	= RGB(0,0,255);
	m_bLBtnDown		= FALSE;
	strText			= _T("x");
}

CIceButton::~CIceButton()
{
}


BEGIN_MESSAGE_MAP(CIceButton, CButton)
	//{{AFX_MSG_MAP(CIceButton)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceButton message handlers

void CIceButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	SetTimer (1,10,NULL);
	
	CButton::OnMouseMove(nFlags, point);
}

void CIceButton::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_bLBtnDown= TRUE;
	
	CButton::OnLButtonDown(nFlags, point);
}

void CIceButton::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	m_bLBtnDown= FALSE;
	
	CButton::OnLButtonUp(nFlags, point);
}

void CIceButton::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	POINT pt;
	static BOOL pPainted = FALSE;
	GetCursorPos(&pt);
	CRect rect;
	GetWindowRect (rect);

	CDC *pDC = GetDC ();
	
	if (m_bLBtnDown)
	{
		KillTimer (1);

		if (pPainted)
			InvalidateRect (NULL);

		pPainted = FALSE;
		return;
	}
	if (!rect.PtInRect (pt))
	{
		KillTimer (1);
	
		if (pPainted)
			InvalidateRect (NULL);
		pPainted = FALSE;

		return;
	}
	else
	{
		if (pPainted)
			return;
		else
			pPainted = TRUE;
		
		GetClientRect(rect);
		pDC->FillSolidRect(rect, m_clrFace);
		pDC->SetTextColor(m_clrHotText);
		pDC->DrawText(strText, m_rect, DT_LEFT|DT_CENTER|DT_VCENTER|DT_SINGLELINE);
		pDC->Draw3dRect(rect, GetSysColor(COLOR_3DHILIGHT), GetSysColor(COLOR_3DDKSHADOW));

		ReleaseDC (pDC);
	}	
	
	CButton::OnTimer(nIDEvent);
}

void CIceButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: Add your code to draw the specified item

	ASSERT(lpDrawItemStruct != NULL);	
	CDC* pDC = GetDC();
	
	m_rect.CopyRect(&lpDrawItemStruct->rcItem);	

	UINT state = lpDrawItemStruct->itemState;
    if ( (state & ODS_SELECTED) )
		pDC->Draw3dRect (m_rect,GetSysColor(COLOR_3DDKSHADOW), GetSysColor(COLOR_3DHILIGHT));
    else
		pDC->Draw3dRect (m_rect,m_clrFace, m_clrFace);

	pDC->SetBkMode( TRANSPARENT );
	pDC->SetTextColor( m_clrText );

	if (state & ODS_DISABLED)
	{
		CBrush grayBrush;
		grayBrush.CreateSolidBrush (GetSysColor(COLOR_GRAYTEXT));
		pDC->GrayString (&grayBrush,NULL,(LPARAM)((LPCSTR)strText),-1,m_rect.left, m_rect.top,m_rect.Width(),m_rect.Height());
	}
	else 
		pDC->DrawText(strText, m_rect, DT_CENTER|DT_VCENTER|DT_SINGLELINE);

}
