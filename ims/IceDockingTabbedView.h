#if !defined(AFX_ICEDOCKINGTABBEDVIEW_H__7FD20F60_52EF_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEDOCKINGTABBEDVIEW_H__7FD20F60_52EF_11D2_852E_00A024E0E339__INCLUDED_

#include <afxtempl.h>
#include "IceDockingControlBar.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceDockingTabbedView.h : header file
//

#define  IDC_DOCKINGTABBEDVIEW 6537

typedef struct
{
	CWnd *pWnd;
	char szLabel[32];
}TCB_ITEM;

/////////////////////////////////////////////////////////////////////////////
// CIceDockingTabbedView window

class CIceDockingTabbedView : public CIceDockingControlBar
{
// Construction
public:
	CIceDockingTabbedView();

// Attributes
public:
	CTabCtrl m_tabctrl;

protected:
	int m_nActiveTab;
	CView* m_pActiveView;
	CList <TCB_ITEM *,TCB_ITEM *> m_views;
	CImageList m_images;

// Operations
public:
	void SetActiveView(int nNewTab);
	void SetActiveView(CRuntimeClass *pViewClass);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceDockingTabbedView)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceDockingTabbedView();

	CView* GetActiveView();
	CView* GetView(int nView);
	CView* GetView(CRuntimeClass *pViewClass);
	BOOL AddView(LPCTSTR lpszLabel, CRuntimeClass *pViewClass, CCreateContext *pContext = NULL);
	void RemoveView(int nView);

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceDockingTabbedView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnTabSelChange(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTabSelChanging(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEDOCKINGTABBEDVIEW_H__7FD20F60_52EF_11D2_852E_00A024E0E339__INCLUDED_)
