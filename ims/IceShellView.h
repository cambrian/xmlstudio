#if !defined(AFX_ICESHELLVIEW_H__15B9B760_56F3_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICESHELLVIEW_H__15B9B760_56F3_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceShellView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceShellView view

class CIceShellView : public CRichEditView
{
protected: // create from serialization only
	CIceShellView();
	DECLARE_DYNCREATE(CIceShellView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceShellView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceShellView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CIceShellView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICESHELLVIEW_H__15B9B760_56F3_11D2_852E_00A024E0E339__INCLUDED_)
