// IceDockingExplorerView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceDockingExplorerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define BORDER		 2
#define IDC_COOLTREECTRL	1111
#define IDC_EXPLORERVIEW	1112


/////////////////////////////////////////////////////////////////////////////
// CIceDockingExplorerView

CIceDockingExplorerView::CIceDockingExplorerView()
{
	m_clrBtnHilight = ::GetSysColor(COLOR_BTNHILIGHT);
	m_clrBtnShadow	= ::GetSysColor(COLOR_BTNSHADOW);
	m_iPaneCaptionY = ::GetSystemMetrics(SM_CYCAPTION);

	m_pControl = NULL;
}

CIceDockingExplorerView::~CIceDockingExplorerView()
{
}


// this Create does not work!!!!
// Use ther other Create function below!!!!
BOOL CIceDockingExplorerView::Create(CWnd* pParentWnd, CSize sizeDefault, UINT nID, DWORD dwStyle) 
{
    ASSERT_VALID(pParentWnd);   // must have a parent
    ASSERT (!((dwStyle & CBRS_SIZE_FIXED) && (dwStyle & CBRS_SIZE_DYNAMIC)));

    // save the style
    m_dwStyle = dwStyle;

    dwStyle &= ~CBRS_ALL;
    dwStyle |= CCS_NOMOVEY|CCS_NODIVIDER|CCS_NORESIZE;

    m_sizeHorz = sizeDefault;
    m_sizeVert = sizeDefault;
    m_sizeFloat = sizeDefault;

	// without CS_BYTEALIGNWINDOW treectrl doesn't expand, 
    CString wndclass = AfxRegisterWndClass(CS_DBLCLKS|CS_BYTEALIGNWINDOW, 
		LoadCursor(NULL, IDC_ARROW),
        CreateSolidBrush(GetSysColor(COLOR_BTNFACE)), 0);

    if (!CWnd::Create(wndclass, NULL, dwStyle, CRect(0,0,0,0),
        pParentWnd, nID))
        return FALSE;


    return TRUE;
}


BOOL CIceDockingExplorerView::Create(LPCTSTR lpszWindowName, CWnd* pParentWnd,
                               CSize sizeDefault, BOOL bHasGripper,
                               UINT nID, DWORD dwStyle)
{
    ASSERT_VALID(pParentWnd);   // must have a parent
    ASSERT (!((dwStyle & CBRS_SIZE_FIXED)
        && (dwStyle & CBRS_SIZE_DYNAMIC)));

    // save the style
    SetBarStyle(dwStyle & CBRS_ALL);

    CString wndclass = ::AfxRegisterWndClass(CS_DBLCLKS,
        ::LoadCursor(NULL, IDC_ARROW),
        ::GetSysColorBrush(COLOR_BTNFACE), 0);

    dwStyle &= ~CBRS_ALL;
    dwStyle &= WS_VISIBLE | WS_CHILD;
    if (!CWnd::Create(wndclass, lpszWindowName, dwStyle, CRect(0,0,0,0),
        pParentWnd, nID))
        return FALSE;

    m_sizeHorz = sizeDefault;
    m_sizeVert = sizeDefault;
    m_sizeFloat = sizeDefault;

	// Do not set the caption here. It cannot be overridden in derived classes!!!!!!!!!1
	//// Default Caption window (IceStatic) title
	////if(m_pControl)
	//// m_pControl->m_stcCaption.SetWindowText(lpszWindowName);

	// bHasGripper should be always FALSE
    m_bHasGripper = FALSE;
    m_cyGripper = m_bHasGripper ? 12 : 0;

    return TRUE;
}


void CIceDockingExplorerView::PositionControls() 
{
	CRect rc;
    GetClientRect(rc);
	
	if (m_pControl)  
	{
   		m_pControl->MoveWindow(rc.left, rc.top, rc.right, rc.bottom);
		m_pControl->CalcLayout(TRUE);
	}	
}


BOOL CIceDockingExplorerView::ShowPane(BOOL bShow) 
{
	BOOL bRet;
	if (m_sizeVert.cx > 0)
		m_sizeStoreVert = m_sizeVert;
	if (m_sizeHorz.cy > 0)
		m_sizeStoreHorz = m_sizeHorz;

	if (!bShow)
	{
		if (IsHorzDocked())
			m_sizeHorz.cy = 0;
		else 
			m_sizeVert.cx = 0;

		bRet = FALSE;
	} 
	else 
	{
		m_sizeHorz = m_sizeStoreHorz;
		m_sizeVert = m_sizeStoreVert;

		bRet = TRUE;
	}
	
    m_pDockSite->DelayRecalcLayout();

	return bRet;
}


BEGIN_MESSAGE_MAP(CIceDockingExplorerView, CIceDockingControlBar)
	//{{AFX_MSG_MAP(CIceDockingExplorerView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_NCPAINT()
	ON_WM_PAINT()
	ON_MESSAGE(IEVN_CAPTIONDRAG, OnCaptionDrag)
	ON_MESSAGE(IEVN_CAPTIONDBLCLK, OnCaptionDblClk)
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_WINDOWPOSCHANGED()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceDockingExplorerView message handlers

int CIceDockingExplorerView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	m_ExplorerView.Create(CRect(0,0,0,0), this, IDC_EXPLORERVIEW);
	m_ExplorerView.CalcLayout(); // FALSE
	
	m_pControl = &m_ExplorerView;
	m_pControl->SubclassDlgItem(IDC_COOLTREECTRL, this);

	// default look
	//*//m_pControl->GetIceTreeCtrl()->SetItemHeight(35);
	//m_pControl->GetIceTreeCtrl()->SetBorder(TVSBF_XBORDER|TVSBF_YBORDER, 2, 0);
	//*//m_pControl->GetIceTreeCtrl()->SetBkColor(GetSysColor (COLOR_3DFACE));
	m_pControl->ShowSideBevels();
	m_pControl->ShowHeader();


	return 0;
}

void CIceDockingExplorerView::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingControlBar::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

    if (IsWindow(m_pControl->GetSafeHwnd()))
		PositionControls() ;
	
}

void CIceDockingExplorerView::OnNcPaint() 
{
	// TODO: Add your message handler code here

	CControlBar::EraseNonClient();

	// Do not call CIceDockingControlBar::OnNcPaint() for painting messages
}

void CIceDockingExplorerView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	
	CControlBar::OnPaint();

	// Do not call CIceDockingControlBar::OnPaint() for painting messages
}


void CIceDockingExplorerView::OnCaptionDrag(UINT nFlags, CPoint point) 
{
	CPoint pt; 
	GetCursorPos(&pt);
	ScreenToClient(&pt);

	CIceDockingControlBar::OnLButtonDown(nFlags, pt);
}

void CIceDockingExplorerView::OnCaptionDblClk(UINT nFlags, CPoint point) 
{
	CPoint pt; 
	GetCursorPos(&pt);
	ScreenToClient(&pt);

	CIceDockingControlBar::OnLButtonDblClk(nFlags, pt);
}

void CIceDockingExplorerView::OnWindowPosChanging(WINDOWPOS FAR* lpwndpos) 
{
	CIceDockingControlBar::OnWindowPosChanging(lpwndpos);
	
	// TODO: Add your message handler code here

}

void CIceDockingExplorerView::OnWindowPosChanged(WINDOWPOS FAR* lpwndpos) 
{
	CIceDockingControlBar::OnWindowPosChanged(lpwndpos);
	
	// TODO: Add your message handler code here

	// Trick!!!!
	if(IsFloating())
		m_pControl->ShowHeader(FALSE);
	else
		m_pControl->ShowHeader();  // default: TRUE

	/*
    if (!m_bInRecalcNC)
    {
        m_bInRecalcNC = TRUE;

        // Force recalc the non-client area
        SetWindowPos(NULL, 0, 0, 0, 0,
            SWP_NOMOVE | SWP_NOSIZE |
            SWP_NOACTIVATE | SWP_NOZORDER |
            SWP_FRAMECHANGED);

        m_bInRecalcNC = FALSE;
    }
	*/
}

