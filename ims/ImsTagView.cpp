// ImsTagView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ImsTagView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImsTagView

IMPLEMENT_DYNCREATE(CImsTagView, CListView)

CImsTagView::CImsTagView()
{
}

CImsTagView::~CImsTagView()
{
}


BEGIN_MESSAGE_MAP(CImsTagView, CListView)
	//{{AFX_MSG_MAP(CImsTagView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImsTagView drawing

void CImsTagView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CImsTagView diagnostics

#ifdef _DEBUG
void CImsTagView::AssertValid() const
{
	CListView::AssertValid();
}

void CImsTagView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImsTagView message handlers
