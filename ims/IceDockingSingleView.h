#if !defined(AFX_ICEDOCKINGSINGLEVIEW_H__7FD20F61_52EF_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICEDOCKINGSINGLEVIEW_H__7FD20F61_52EF_11D2_852E_00A024E0E339__INCLUDED_

#include "IceDockingControlBar.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceDockingSingleView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceDockingSingleView window

class CIceDockingSingleView : public CIceDockingControlBar
{
// Construction
public:
	CIceDockingSingleView();

// Attributes
public:

protected:
	CView* m_pView;

// Operations
public:
	BOOL   CreateView(CRuntimeClass *pViewClass, CCreateContext *pContext = NULL);
	CView* GetView();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceDockingSingleView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceDockingSingleView();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceDockingSingleView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICEDOCKINGSINGLEVIEW_H__7FD20F61_52EF_11D2_852E_00A024E0E339__INCLUDED_)
