#if !defined(AFX_DTDVIEW_H__BB9136B1_5797_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_DTDVIEW_H__BB9136B1_5797_11D2_852E_00A024E0E339__INCLUDED_

#include "IceRichEditView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// DtdView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDtdView view

class CDtdView : public CIceRichEditView
{
protected: // create from serialization only
	CDtdView();
	DECLARE_DYNCREATE(CDtdView)

// Attributes
public:
	CDtdDoc*  GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDtdView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDtdView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CDtdView)
	afx_msg void OnViewSource();
	afx_msg void OnUpdateViewSource(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CDtdDoc* CDtdView::GetDocument()
   { return (CDtdDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DTDVIEW_H__BB9136B1_5797_11D2_852E_00A024E0E339__INCLUDED_)
