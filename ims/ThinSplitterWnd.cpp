// ThinSplitterWnd.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ThinSplitterWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThinSplitterWnd

CThinSplitterWnd::CThinSplitterWnd()
{
	m_cxCur = 100;
	m_cyCur = 100;

	/*
	// This only works when user resizing is disabled...
	// ??????????????????
	m_cxSplitter = 3;
	m_cySplitter = 3;
	m_cxBorderShare = 1;
	m_cyBorderShare = 1;
	m_cxSplitterGap = 3 + 1 + 1;
	m_cySplitterGap = 3 + 1 + 1;
	*/
}

CThinSplitterWnd::~CThinSplitterWnd()
{
}


BEGIN_MESSAGE_MAP(CThinSplitterWnd, CIceSplitterWnd)
	//{{AFX_MSG_MAP(CThinSplitterWnd)
	ON_WM_MOUSEMOVE()
	ON_WM_SIZE()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CThinSplitterWnd message handlers

void CThinSplitterWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CIceSplitterWnd::OnMouseMove(nFlags, point);
    
	// No user resizing...
	//CWnd::OnMouseMove(nFlags, point);
}

BOOL CThinSplitterWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// TODO: Add your message handler code here and/or call default
	
	return CIceSplitterWnd::OnSetCursor(pWnd, nHitTest, message);
    
	// No user resizing...
	//return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

void CThinSplitterWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CIceSplitterWnd::OnLButtonDown(nFlags, point);
    
	// No user resizing...
	//CIceSplitterWnd::OnLButtonDown(nFlags, point);
}

// this splitter is used only as 1x2 or 2x1 !!!!!!!!!!!!!!!!!
void CThinSplitterWnd::OnSize(UINT nType, int cx, int cy) 
{
	//CIceSplitterWnd::OnSize(nType, cx, cy);
	
	if(GetColumnCount() > 1)
	{
		if (nType != SIZE_MINIMIZED && cx >= m_cxCur && cy > 0)
		{
			SetColumnInfo(0, cx - m_cxCur, 0);  // second column pane size = CXPALETWIDTH
			RecalcLayout();
		}
		else if (nType != SIZE_MINIMIZED && cx < m_cxCur && cy > 0)
		{
			SetColumnInfo(0, 0, 0);  // No first pane
			RecalcLayout();
		}
	}
	if(GetRowCount() > 1)
	{
		if (nType != SIZE_MINIMIZED && cx > 0 && cy >= m_cyCur)
		{
			SetRowInfo(0, m_cyCur, 0);  
			RecalcLayout();
		}
		else if (nType != SIZE_MINIMIZED && cx > 0 && cy < m_cyCur)
		{
			SetRowInfo(0, cy, 0);  
			RecalcLayout();
		}
	}

	CWnd::OnSize(nType, cx, cy);
	//CIceSplitterWnd::OnSize(nType, cx, cy);

	/*
	int nTemp, cxTemp;
	GetColumnInfo(0,cxTemp,nTemp);
	m_cxCur = cx - cxTemp;
	GetRowInfo(0,m_cyCur,nTemp);
	*/
}


void CThinSplitterWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	CIceSplitterWnd::OnLButtonUp(nFlags, point);

	int nTemp;
	if(GetColumnCount() > 1)
	{
		GetColumnInfo(1,m_cxCur,nTemp);
	}
	GetRowInfo(0,m_cyCur,nTemp);
}
