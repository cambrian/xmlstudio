#if !defined(AFX_ICERICHEDITCTRL_H__2535CF82_53E7_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICERICHEDITCTRL_H__2535CF82_53E7_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceRichEditCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceRichEditCtrl window

class CIceRichEditCtrl : public CRichEditCtrl
{
// Construction
public:
	CIceRichEditCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceRichEditCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceRichEditCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceRichEditCtrl)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICERICHEDITCTRL_H__2535CF82_53E7_11D2_852E_00A024E0E339__INCLUDED_)
