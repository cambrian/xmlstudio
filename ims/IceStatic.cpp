// IceStatic.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceStatic

CIceStatic::CIceStatic(LPCTSTR lpText, BOOL bDeleteOnDestroy)
{
	m_color = RGB(0,0,255);				// hot color
	m_bDeleteOnDestroy = bDeleteOnDestroy;	// delete object with window?
}

CIceStatic::~CIceStatic()
{
}


BEGIN_MESSAGE_MAP(CIceStatic, CStatic)
	//{{AFX_MSG_MAP(CIceStatic)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_LBUTTONDOWN()
	ON_WM_NCHITTEST()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceStatic message handlers

void CIceStatic::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class

	if (m_bDeleteOnDestroy)
		delete this;
	
	//CStatic::PostNcDestroy();
}

HBRUSH CIceStatic::CtlColor(CDC* pDC, UINT nCtlColor) 
{
	// TODO: Change any attributes of the DC here
	
	ASSERT(nCtlColor == CTLCOLOR_STATIC);
	DWORD dwStyle = GetStyle();
	
	HBRUSH hbr = NULL;
	if ((dwStyle & 0xFF) <= SS_RIGHT) {

		// this is a text control: set up font and colors
		if (!(HFONT)m_font) {
			// first time init: create font
			LOGFONT lf;
			GetFont()->GetObject(sizeof(lf), &lf);
		//	lf.lfUnderline = TRUE;
		//	lf.lfWeight = FW_BOLD;
		//	lf.lfItalic = TRUE;
			m_font.CreateFontIndirect(&lf);
		}

		// use underline font and visited/unvisited colors
		pDC->SelectObject(&m_font);
	//	pDC->SetTextColor(m_color);
		pDC->SetBkMode(TRANSPARENT);

		// return hollow brush to preserve parent background color
		hbr = (HBRUSH)::GetStockObject(HOLLOW_BRUSH);
	}
	return hbr;
	
	// TODO: Return a non-NULL brush if the parent's handler should not be called
	//return NULL;
}

void CIceStatic::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	// send IEVN_CAPTIONDRAG to parent and handle there
	CWnd* pParent = GetParent();
	ASSERT_VALID(pParent);
	if (pParent != NULL)
		pParent->SendMessage(IEVN_CAPTIONDRAG);
	
	//CStatic::OnLButtonDown(nFlags, point);
}

UINT CIceStatic::OnNcHitTest(CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	return HTCLIENT;
	
	//return CStatic::OnNcHitTest(point);
}

void CIceStatic::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	// send IEVN_CAPTIONDBLCLK to parent and handle there
	CWnd* pParent = GetParent();
	ASSERT_VALID(pParent);
	if (pParent != NULL)
		pParent->SendMessage(IEVN_CAPTIONDBLCLK);

	//CStatic::OnLButtonDblClk(nFlags, point);
}
