#if !defined(AFX_ICERICHEDITVIEW_H__D4AD6CC1_51ED_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICERICHEDITVIEW_H__D4AD6CC1_51ED_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceRichEditView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceRichEditView view

class CIceRichEditView : public CRichEditView
{
protected: // create from serialization only
	CIceRichEditView();
	DECLARE_DYNCREATE(CIceRichEditView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceRichEditView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceRichEditView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CIceRichEditView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICERICHEDITVIEW_H__D4AD6CC1_51ED_11D2_852E_00A024E0E339__INCLUDED_)
