// IceMenu.h: interface for the CIceMenu class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IceMenu_H__BB6F2F01_91FA_11D1_8B78_0000B43382FE__INCLUDED_)
#define AFX_IceMenu_H__BB6F2F01_91FA_11D1_8B78_0000B43382FE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CIceMenu
{
public:
	struct ToolBarData	
	{ // struct for toolbar resource; guess you already know it ;)
		WORD wVersion;
		WORD wWidth;			
		WORD wHeight;			
		WORD wItemCount;		
	};
	struct SpawnItem
	{
		int		iImageIdx;
		int		iCmd;
		char	cText[128];
	};
	struct ImageItem
	{
		int		iImageIdx;
		int		iCmd;
	};

// Construction
public:
	CIceMenu();
	CIceMenu(const bool _IsPopup);

// Attributes
public:
	int      m_nBarGripper;  // 0: no bars in gripper.  Valid values: 1 or 2

// Operations
public:
	bool IsIceMenu(CMenu * pMenu, const int iItem, const bool bByPos);
	bool GetMenuItemText(CString &csText, CMenu * pMenu, const int cmd, bool bByPos);
	void TransparentBlt(CDC * pDestDc, int x, int y, int w, int h, CBitmap * pBmp, int sx, int sy, COLORREF crTransparent);
	void SetBackBitmap(const int iRes, COLORREF crBackColor);
	void SetBackBitmap(const int iRes);
	void SetTextColor(const COLORREF crNormal, const COLORREF crSelected);
	bool FindKeyboardShortcut(UINT nChar, UINT nFlags, CMenu * pMenu, LRESULT &lRes);
	bool SetFont(LOGFONT * lf);
	
	HFONT	hMenuFont, hGuiFont;
	CSize szImage;
	bool MeasureItem(LPMEASUREITEMSTRUCT lpm);
	bool DrawItem(LPDRAWITEMSTRUCT lp);
	int FindImageItem(const int cmd);
	SpawnItem * AddSpawnItem(const char * txt, const int cmd);
	void RemapMenu(CMenu * pMenu);
	void AddImageItem(const int idx, WORD cmd);

	CImageList ilList;
	CImageList ilOther;

	COLORREF crMenuText, crMenuTextSel;
	COLORREF cr3dFace, crMenu, crHighlight, cr3dHilight, cr3dShadow, crGrayText;
	COLORREF m_clrBtnFace, m_clrBtnHilight, m_clrBtnShadow;

	int iSpawnItem;
	SpawnItem ** pSpawnItem;

	int iImageItem;
	ImageItem * pImageItem;

	bool bIsPopup;
	bool bBackBitmap;
	CBitmap	bmpBack;

	bool LoadToolBarResource(unsigned int resId);
	bool AddToolBarResource(unsigned int resId);
	void EnableMenuItems(CMenu * pMenu, CWnd * pParent);

//protected:
	virtual ~CIceMenu();

protected:
	void Init();

};

#endif // !defined(AFX_IceMenu_H__BB6F2F01_91FA_11D1_8B78_0000B43382FE__INCLUDED_)
