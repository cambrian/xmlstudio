// IceListCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceListCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIceListCtrl

CIceListCtrl::CIceListCtrl()
{
}

CIceListCtrl::~CIceListCtrl()
{
}


BEGIN_MESSAGE_MAP(CIceListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CIceListCtrl)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIceListCtrl message handlers
