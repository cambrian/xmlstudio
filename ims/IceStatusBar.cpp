// IceStatusBar.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "IceStatusBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CIceStatusBar *CIceStatusBar::aktBar = NULL;

/////////////////////////////////////////////////////////////////////////////
// CIceStatusBar

CIceStatusBar::CIceStatusBar()
{
	aktBar		= this;
	timerID		= 0;
}

CIceStatusBar::~CIceStatusBar()
{
}


BEGIN_MESSAGE_MAP(CIceStatusBar, CStatusBar)
	//{{AFX_MSG_MAP(CIceStatusBar)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MBUTTONDBLCLK()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDBLCLK()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CIceStatusBar message handlers

bool CIceStatusBar::CreateStatusBar(CWnd *pWnd, const UINT* lpIDArray, int nIDCount, UINT pane0Style)
{
	bool ret = (Create(pParent = pWnd) &&
				SetIndicators(lpIDArray, nIDCount));

	if (ret)	SetStyle(0, pane0Style);
	else		AfxMessageBox("Unable to create Statusbar.");

	return ret;
}

BOOL CIceStatusBar::SetIndicators(const UINT* lpIDArray, int nIDCount)
{
	BOOL ret = CStatusBar::SetIndicators(lpIDArray, nIDCount);

	ISBPaneInfo defaultInfo;
	paneInfo.SetSize(nIDCount);
	
	for (int i = 0; i < nIDCount; i++)
		paneInfo[i] = defaultInfo;

	return ret;
}

void CIceStatusBar::OnPaint()
{
	CPaintDC	dc(this);
	CRect		client;		GetClientRect(client);
	CDC			memDC;		memDC.CreateCompatibleDC(&dc);
	CBitmap		bitmap;		bitmap.CreateCompatibleBitmap(&dc, client.Width(), client.Height());
	CBitmap		*oldBitmap	= (CBitmap *) memDC.SelectObject(&bitmap);
	COLORREF	hell		= GetSysColor(COLOR_3DHILIGHT);
	COLORREF	dunkel		= GetSysColor(COLOR_3DSHADOW);

	memDC.FillSolidRect(client, GetSysColor(COLOR_3DFACE));

	//========================================================================
	// Zeichne alle Pane-Rechtecke mit ihrem Inhalt
	//========================================================================
	bool scroll = false;
	for (int i = 0, n = paneInfo.GetSize(); i < n; i++)
	{
		ISBPaneInfo &aktPane = paneInfo[i];

		CRect	rect;
		GetItemRect(i, rect);
		UINT style = GetPaneStyle(i);

		if (style & SBPS_POPOUT)			memDC.Draw3dRect(rect, hell, dunkel);
		else if (!(style & SBPS_NOBORDERS))	memDC.Draw3dRect(rect, dunkel, hell);

		on = (GetPaneStyle(i) & SBPS_DISABLED) == 0;

		rect.DeflateRect(1, 1);
		memDC.FillSolidRect(rect, aktPane.GetBkColor(on));

		CRgn clip;
		clip.CreateRectRgnIndirect(&rect);
		memDC.SelectClipRgn(&clip);

		switch (aktPane.GetMode() & XSB_MODE)
		{
		case XSB_TEXT:
		case XSB_NUMBER:	DrawTextPane(	 &memDC, i, rect, aktPane);	break;
		case XSB_BITMAP:	DrawBitmapPane(	 &memDC, i, rect, aktPane);	break;
		case XSB_PROGRESS:	DrawProgressPane(&memDC, i, rect, aktPane);	break;
		default:														break;
		}

		if (aktPane.GetMode() & XSB_SCROLL)	scroll = true;

		memDC.SelectClipRgn(NULL);
	}

		 if (scroll  && (timerID == 0))		timerID = SetTimer(IDC_JRLIB_STATUSBAR_TIMER, 100, NULL);
	else if (!scroll && (timerID != 0))		{ KillTimer(timerID); timerID = 0; }

	if (!GetParent()->IsZoomed()) DrawSizing(&memDC);

	dc.BitBlt(0, 0, client.Width(), client.Height(), &memDC, 0, 0, SRCCOPY);

	memDC.SelectObject(oldBitmap);
}

void CIceStatusBar::DrawSizing(CDC *pDC)
{
	CRect rect;
	GetWindowRect(&rect);
	rect.OffsetRect(-rect.left, -rect.top);

	CPen hellPen(PS_SOLID, 1, GetSysColor(COLOR_3DHILIGHT));
	CPen dunkelPen(PS_SOLID, 1, GetSysColor(COLOR_3DSHADOW));
	CPen *oldPen = pDC->SelectObject(&dunkelPen);
	for (int i = 1; i < 18; i++)
	{
		switch (i % 4)
		{
		case 0:		pDC->SelectObject(&dunkelPen);	break;
		case 1:		pDC->SelectObject(&hellPen);	break;
		case 2:		continue;
		case 3:		pDC->SelectObject(&dunkelPen);	break;
		}
		pDC->MoveTo(rect.right - i, rect.bottom);
		pDC->LineTo(rect.right,		rect.bottom - i);
	}

	pDC->SelectObject(oldPen);
}

void CIceStatusBar::DrawTextPane(CDC *pDC, int ix, CRect& rect, ISBPaneInfo& aktPane)
{
	CString		text		= aktPane.GetText(on);
	if (text.IsEmpty())		text = GetPaneText(ix);
	if (text.IsEmpty())		return;

	CFont		font;		font.CreateFontIndirect(&aktPane.GetFont());
	COLORREF	textColor	= aktPane.GetFgColor(on);
	CFont		*oldFont	= pDC->SelectObject(&font);
	int			oldBkMode	= pDC->SetBkMode(TRANSPARENT);
	COLORREF	oldFgColor	= pDC->SetTextColor(textColor);
	const int	mode		= aktPane.GetMode();
	const int	repeat		= mode & XSB_REPEAT;
	const int	hScroll		= mode & XSB_HSCROLL;
	const int	vScroll		= mode & XSB_VSCROLL;
	int			textAlign	= mode & XSB_ALIGN;
	if (hScroll)textAlign	&= ~(DT_CENTER | DT_RIGHT);

	if (repeat)
	{
		text += "  ";
		CSize s = pDC->GetTextExtent(text);
		if (hScroll)	aktPane.HScroll(rect, s.cx, 1);
		if (vScroll)	aktPane.VScroll(rect, s.cy, 1);

		int y = rect.top;
		if (hScroll && vScroll)
			for ( ; rect.left <= rect.right; rect.left += s.cx)
				for (rect.top = y ; rect.top <= rect.bottom; rect.top += s.cy)
					pDC->DrawText(text, rect, textAlign);
		else if (hScroll)
			for ( ; rect.left <= rect.right; rect.left += s.cx)
				pDC->DrawText(text, rect, textAlign);
		else if (vScroll)
			for ( ; rect.top <= rect.bottom; rect.top += s.cy)
				pDC->DrawText(text, rect, textAlign);
		else
			pDC->DrawText(text, rect, textAlign);
	}
	else
	{
		CSize s = pDC->GetTextExtent(text);
		if (hScroll)	aktPane.HScroll(rect, s.cx, -rect.Width());
		if (vScroll)	aktPane.VScroll(rect, s.cy, -rect.Height());

		pDC->DrawText(text, rect, textAlign);
	}

	pDC->SelectObject(oldFont);
	pDC->SetTextColor(oldFgColor);
	pDC->SetBkMode(oldBkMode);
}

void CIceStatusBar::DrawBitmapPane(CDC *pDC, int ix, CRect& rect, ISBPaneInfo& aktPane)
{
	CString		bmString	= aktPane.GetBitmap(on);
	if (bmString.IsEmpty())	return;
	CDC			memDC;		memDC.CreateCompatibleDC(pDC);
	CBitmap		bitmap;		bitmap.LoadBitmap(bmString);
	BITMAP		bm;			bitmap.GetBitmap(&bm);
	CBitmap		*oldBitmap	= memDC.SelectObject(&bitmap);
	const int	mode		= aktPane.GetMode();
	const int	hScroll		= mode & XSB_HSCROLL;
	const int	vScroll		= mode & XSB_VSCROLL;
	const int	repeat		= mode & XSB_REPEAT;
	const int	stretch		= mode & XSB_STRETCH;

	if (repeat)
	{
		if (hScroll)	aktPane.HScroll(rect, bm.bmWidth, 1);
		if (vScroll)	aktPane.VScroll(rect, bm.bmHeight, 1);

		if (hScroll && !vScroll)
			for (int x = rect.left; x < rect.right; x += bm.bmWidth)
				pDC->BitBlt(x, rect.top, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
		else if (vScroll && !hScroll)
			for (int y = rect.top; y < rect.bottom; y += bm.bmHeight)
				pDC->BitBlt(rect.left, y, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
		else
			for (int x = rect.left; x < rect.right; x += bm.bmWidth)
				for (int y = rect.top; y < rect.bottom; y += bm.bmHeight)
					pDC->BitBlt(x, y, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);

	}
	else
	{
		if (hScroll)	aktPane.HScroll(rect, bm.bmWidth, -rect.Width());
		if (vScroll)	aktPane.VScroll(rect, bm.bmHeight, -rect.Height());

		if (hScroll || vScroll)
			pDC->BitBlt(rect.left, rect.top, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
		else if (stretch)
			pDC->StretchBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
		else
		{
			if (mode & DT_RIGHT)	rect.left	= rect.right - bm.bmWidth;
			if (mode & DT_CENTER)	rect.left	= (rect.left + rect.right - bm.bmWidth) / 2;
			if (mode & DT_BOTTOM)	rect.top	= rect.bottom - bm.bmHeight;
			if (mode & DT_VCENTER)	rect.top	= (rect.top + rect.bottom - bm.bmHeight) / 2;
			pDC->BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
		}
	}

	memDC.SelectObject(oldBitmap);
}

void CIceStatusBar::DrawProgressPane(CDC *pDC, int ix, CRect& rect, ISBPaneInfo& aktPane)
{
	rect.InflateRect(1, 1);
	GetProgressCtrl(ix)->MoveWindow(rect, FALSE);
}

ISBPaneInfo& CIceStatusBar::GetISBPaneInfo(int ix)
{
	ASSERT(ix < GetCount());
	return paneInfo[ix];
}

void CIceStatusBar::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == timerID)	Invalidate(FALSE);
}

#define GET_PANE_INFO(ix)	ASSERT(ix < GetCount()); \
							UINT id, style; int width; \
							GetPaneInfo(ix, id, style, width)

UINT CIceStatusBar::SetStyle(int ix, UINT newStyle)
{ 
	GET_PANE_INFO(ix); 
	SetPaneInfo(ix, id, newStyle, width); 
	return style; 
}

int  CIceStatusBar::SetWidth(int ix, int newWidth)		
{ 
	GET_PANE_INFO(ix); 
	SetPaneInfo(ix, id, style, newWidth); 
	return width; 
}

UINT CIceStatusBar::GetStyle(int ix)					
{ 
	GET_PANE_INFO(ix); 
	return style;	
}

int  CIceStatusBar::GetWidth(int ix)					
{ 
	GET_PANE_INFO(ix); 
	return width;	
}

UINT CIceStatusBar::GetID(int ix)						
{ 
	GET_PANE_INFO(ix); 
	return id;	
}

#undef GET_PANE_INFO

void CIceStatusBar::OnDestroy() 
{
	if (timerID)	KillTimer(timerID);
	timerID = 0;

	CStatusBar::OnDestroy();
}

void CIceStatusBar::SavePane(int ix)
{
	ASSERT(ix < GetCount());

	ISBPaneInfo	value;
	if (!buffer.Lookup(ix, value))
		buffer[ix] = paneInfo[ix];
}

void CIceStatusBar::RestorePane(int ix)
{
	ASSERT(ix < GetCount());
	
	ISBPaneInfo	value;
	if (buffer.Lookup(ix, value))
	{
		paneInfo[ix] = value;
		buffer.RemoveKey(ix);
	}

	Invalidate(FALSE);
}

int CIceStatusBar::GetPaneAtPosition(CPoint& point)
{
	if (pParent->IsKindOf(RUNTIME_CLASS(CDialog)))
	{
		pParent->ClientToScreen(&point);
		ScreenToClient(&point);
	}

	CRect rect;
	for (int i = 0, n = GetCount(); i < n; i++)
	{
		GetItemRect(i, rect);
		if (rect.PtInRect(point))	return i;
	}

	return -1;
}

#define TO_PARENT(fkt, event)	\
	void CIceStatusBar::fkt(UINT nFlags, CPoint point) \
	{ \
		pParent->SendMessage(event, (WPARAM) nFlags, MAKELPARAM(point.x, point.y)); \
		CStatusBar::fkt(nFlags, point); \
	}

TO_PARENT(OnLButtonUp,		WM_LBUTTONUP);
TO_PARENT(OnMButtonUp,		WM_MBUTTONUP);
TO_PARENT(OnRButtonUp,		WM_RBUTTONUP);
TO_PARENT(OnLButtonDown,	WM_LBUTTONDOWN);
TO_PARENT(OnMButtonDown,	WM_MBUTTONDOWN);
TO_PARENT(OnRButtonDown,	WM_RBUTTONDOWN);
TO_PARENT(OnLButtonDblClk,	WM_LBUTTONDBLCLK);
TO_PARENT(OnMButtonDblClk,	WM_MBUTTONDBLCLK);
TO_PARENT(OnRButtonDblClk,	WM_RBUTTONDBLCLK);
TO_PARENT(OnMouseMove,		WM_MOUSEMOVE);

#undef TO_PARENT
