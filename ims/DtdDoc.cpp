// DtdDoc.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "DtdDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDtdDoc

IMPLEMENT_DYNCREATE(CDtdDoc, COleServerDoc)

CDtdDoc::CDtdDoc()
{
	EnableAutomation();

	m_bSourcePresent = FALSE;
}

BOOL CDtdDoc::OnNewDocument()
{
	if (!COleServerDoc::OnNewDocument())
		return FALSE;
	return TRUE;
}

CDtdDoc::~CDtdDoc()
{
}

void CDtdDoc::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	COleServerDoc::OnFinalRelease();
}

COleServerItem* CDtdDoc::OnGetEmbeddedItem()
{
	// OnGetEmbeddedItem is called by the framework to get the COleServerItem
	//  that is associated with the document.  It is only called when necessary.

	// Instead of returning NULL, return a pointer to a new COleServerItem
	//  derived class that is used in conjunction with this document, then
	//  remove the ASSERT(FALSE) below.
	//  (i.e., return new CMyServerItem.)
	ASSERT(FALSE);			// remove this after completing the TODO
	return NULL;
}


BEGIN_MESSAGE_MAP(CDtdDoc, COleServerDoc)
	//{{AFX_MSG_MAP(CDtdDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CDtdDoc, COleServerDoc)
	//{{AFX_DISPATCH_MAP(CDtdDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IDtdDoc to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {BB9136A9-5797-11D2-852E-00A024E0E339}
static const IID IID_IDtdDoc =
{ 0xbb9136a9, 0x5797, 0x11d2, { 0x85, 0x2e, 0x0, 0xa0, 0x24, 0xe0, 0xe3, 0x39 } };

BEGIN_INTERFACE_MAP(CDtdDoc, COleServerDoc)
	INTERFACE_PART(CDtdDoc, IID_IDtdDoc, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDtdDoc diagnostics

#ifdef _DEBUG
void CDtdDoc::AssertValid() const
{
	COleServerDoc::AssertValid();
}

void CDtdDoc::Dump(CDumpContext& dc) const
{
	COleServerDoc::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDtdDoc serialization

void CDtdDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDtdDoc commands
