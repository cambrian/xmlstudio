// ims.h : main header file for the IMS application
//

#if !defined(AFX_IMS_H__D4AD6C87_51ED_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_IMS_H__D4AD6C87_51ED_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////
////// strings for registry entry
static const TCHAR s_profileCompany[] = _T("Intelligent Software");


/////////////////////////////////////////////////////////////////////////////
// CImsApp:
// See ims.cpp for the implementation of this class
//

class CImsApp : public CWinApp
{
public:
	CImsApp();

public:
	CMultiDocTemplate* m_pXmlDocTemplate;
	CMultiDocTemplate* m_pDtdDocTemplate;
	CMultiDocTemplate* m_pXslDocTemplate;
	CMultiDocTemplate* m_pTexDocTemplate;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImsApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation

	//{{AFX_MSG(CImsApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMS_H__D4AD6C87_51ED_11D2_852E_00A024E0E339__INCLUDED_)
