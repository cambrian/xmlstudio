; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CXmlDoc
LastTemplate=CListView
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "ims.h"
LastPage=0

ClassCount=57
Class1=CBottomDockingBar
Class2=CChildFrame
Class3=CChildSpltFrame
Class4=CCoolTreeCtrl
Class5=CDefaultStatusBar
Class6=CDtdDoc
Class7=CDtdView
Class8=CExplorerDockingBar
Class9=CIceButton
Class10=CIceDockingControlBar
Class11=CIceDockingExplorerView
Class12=CIceDockingSingleView
Class13=CIceDockingTabbedView
Class14=CIceExplorerView
Class15=CIceListCtrl
Class16=CIceListView
Class17=CIceMDIChildWnd
Class18=CIceMDIFrameWnd
Class19=CIceRichEditCtrl
Class20=CIceRichEditView
Class21=CIceShellView
Class22=CIceSplitterWnd
Class23=CIceStatic
Class24=CIceStatusBar
Class25=CIceToolBar
Class26=CIceTreeCtrl
Class27=CIceTreeView
Class28=CImsApp
Class29=CAboutDlg
Class30=CImsDoc
Class31=CImsDtdView
Class32=CImsTagView
Class33=CImsTextView
Class34=CImsTexView
Class35=CImsView
Class36=CImsXmlView
Class37=CImsXslView
Class38=CInPlaceFrame
Class39=CLeftDockingBar
Class40=CListEditCtrl
Class41=CLogfileView
Class42=CMainFrame
Class43=CMainToolBar
Class44=COutputView
Class45=CPaletView
Class46=CParseView
Class47=CRightDockingBar
Class48=CScriptView
Class49=CTexDoc
Class50=CTexView
Class51=CThinSplitterWnd
Class52=CTreeMenuView
Class53=CXmlDoc
Class54=CXmlView
Class55=CXslDoc
Class56=CXslView

ResourceCount=14
Resource1=IDR_SYSTEMBAR (English (U.S.))
Resource2=IDR_IMSTYPE_SRVR_IP (English (U.S.))
Resource3=IDR_IMSTYPE (English (U.S.))
Resource4=IDR_RESERVOIR (English (U.S.))
Resource5=IDR_IMSTYPE_SRVR_EMB (English (U.S.))
Resource6=IDR_DTDTYPE (English (U.S.))
Resource7=IDR_XMLTYPE (English (U.S.))
Resource8=IDR_IMSTYPE_CNTR_IP (English (U.S.))
Resource9=IDR_TEXTYPE (English (U.S.))
Resource10=IDD_ABOUTBOX (English (U.S.))
Resource11=IDR_MRUFILE (English (U.S.))
Resource12=IDD_PALETVIEW (English (U.S.))
Resource13=IDR_XSLTYPE (English (U.S.))
Class57=CIceParseView
Resource14=IDR_MAINFRAME (English (U.S.))

[CLS:CBottomDockingBar]
Type=0
BaseClass=CIceDockingTabbedView
HeaderFile=BottomDockingBar.h
ImplementationFile=BottomDockingBar.cpp

[CLS:CChildFrame]
Type=0
BaseClass=CIceMDIChildWnd
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp

[CLS:CChildSpltFrame]
Type=0
BaseClass=CIceMDIChildWnd
HeaderFile=ChildSpltFrm.h
ImplementationFile=ChildSpltFrm.cpp

[CLS:CCoolTreeCtrl]
Type=0
BaseClass=CWnd
HeaderFile=CoolTreeCtrl.h
ImplementationFile=CoolTreeCtrl.cpp

[CLS:CDefaultStatusBar]
Type=0
BaseClass=CIceStatusBar
HeaderFile=DefaultStatusBar.h
ImplementationFile=DefaultStatusBar.cpp

[CLS:CDtdDoc]
Type=0
BaseClass=COleServerDoc
HeaderFile=DtdDoc.h
ImplementationFile=DtdDoc.cpp

[CLS:CDtdView]
Type=0
BaseClass=CIceRichEditView
HeaderFile=DtdView.h
ImplementationFile=DtdView.cpp

[CLS:CExplorerDockingBar]
Type=0
BaseClass=CIceDockingExplorerView
HeaderFile=ExplorerDockingBar.h
ImplementationFile=ExplorerDockingBar.cpp

[CLS:CIceButton]
Type=0
BaseClass=CButton
HeaderFile=IceButton.h
ImplementationFile=IceButton.cpp

[CLS:CIceDockingControlBar]
Type=0
BaseClass=CControlBar
HeaderFile=IceDockingControlBar.h
ImplementationFile=IceDockingControlBar.cpp

[CLS:CIceDockingExplorerView]
Type=0
BaseClass=CIceDockingControlBar
HeaderFile=IceDockingExplorerView.h
ImplementationFile=IceDockingExplorerView.cpp

[CLS:CIceDockingSingleView]
Type=0
BaseClass=CIceDockingControlBar
HeaderFile=IceDockingSingleView.h
ImplementationFile=IceDockingSingleView.cpp

[CLS:CIceDockingTabbedView]
Type=0
BaseClass=CIceDockingControlBar
HeaderFile=IceDockingTabbedView.h
ImplementationFile=IceDockingTabbedView.cpp

[CLS:CIceExplorerView]
Type=0
BaseClass=CWnd
HeaderFile=IceExplorerView.h
ImplementationFile=IceExplorerView.cpp

[CLS:CIceListCtrl]
Type=0
BaseClass=CListCtrl
HeaderFile=IceListCtrl.h
ImplementationFile=IceListCtrl.cpp

[CLS:CIceListView]
Type=0
BaseClass=CListView
HeaderFile=IceListView.h
ImplementationFile=IceListView.cpp
Filter=C
VirtualFilter=VWC
LastObject=CIceListView

[CLS:CIceMDIChildWnd]
Type=0
BaseClass=CMDIChildWnd
HeaderFile=IceMDIChildWnd.h
ImplementationFile=IceMDIChildWnd.cpp

[CLS:CIceMDIFrameWnd]
Type=0
BaseClass=CMDIFrameWnd
HeaderFile=IceMDIFrameWnd.h
ImplementationFile=IceMDIFrameWnd.cpp

[CLS:CIceRichEditCtrl]
Type=0
BaseClass=CRichEditCtrl
HeaderFile=IceRichEditCtrl.h
ImplementationFile=IceRichEditCtrl.cpp

[CLS:CIceRichEditView]
Type=0
BaseClass=CRichEditView
HeaderFile=IceRichEditView.h
ImplementationFile=IceRichEditView.cpp

[CLS:CIceShellView]
Type=0
BaseClass=CRichEditView
HeaderFile=IceShellView.h
ImplementationFile=IceShellView.cpp
Filter=C
VirtualFilter=VWC
LastObject=CIceShellView

[CLS:CIceSplitterWnd]
Type=0
BaseClass=CSplitterWnd
HeaderFile=IceSplitterWnd.h
ImplementationFile=IceSplitterWnd.cpp

[CLS:CIceStatic]
Type=0
BaseClass=CStatic
HeaderFile=IceStatic.h
ImplementationFile=IceStatic.cpp

[CLS:CIceStatusBar]
Type=0
BaseClass=CStatusBar
HeaderFile=IceStatusBar.h
ImplementationFile=IceStatusBar.cpp

[CLS:CIceToolBar]
Type=0
BaseClass=CToolBar
HeaderFile=IceToolBar.h
ImplementationFile=IceToolBar.cpp

[CLS:CIceTreeCtrl]
Type=0
BaseClass=CCoolTreeCtrl
HeaderFile=IceTreeCtrl.h
ImplementationFile=IceTreeCtrl.cpp

[CLS:CIceTreeView]
Type=0
BaseClass=CTreeView
HeaderFile=IceTreeView.h
ImplementationFile=IceTreeView.cpp

[CLS:CImsApp]
Type=0
BaseClass=CWinApp
HeaderFile=ims.h
ImplementationFile=ims.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=ims.cpp
ImplementationFile=ims.cpp
LastObject=CAboutDlg

[CLS:CImsDoc]
Type=0
BaseClass=COleServerDoc
HeaderFile=imsDoc.h
ImplementationFile=imsDoc.cpp

[CLS:CImsDtdView]
Type=0
BaseClass=CIceListView
HeaderFile=ImsDtdView.h
ImplementationFile=ImsDtdView.cpp

[CLS:CImsTagView]
Type=0
BaseClass=CListView
HeaderFile=ImsTagView.h
ImplementationFile=ImsTagView.cpp

[CLS:CImsTextView]
Type=0
BaseClass=CRichEditView
HeaderFile=ImsTextView.h
ImplementationFile=ImsTextView.cpp

[CLS:CImsTexView]
Type=0
BaseClass=CIceListView
HeaderFile=ImsTexView.h
ImplementationFile=ImsTexView.cpp

[CLS:CImsView]
Type=0
BaseClass=CIceListView
HeaderFile=imsView.h
ImplementationFile=imsView.cpp

[CLS:CImsXmlView]
Type=0
BaseClass=CIceListView
HeaderFile=ImsXmlView.h
ImplementationFile=ImsXmlView.cpp
LastObject=CImsXmlView
Filter=C
VirtualFilter=VWC

[CLS:CImsXslView]
Type=0
BaseClass=CIceListView
HeaderFile=ImsXslView.h
ImplementationFile=ImsXslView.cpp

[CLS:CInPlaceFrame]
Type=0
BaseClass=COleDocIPFrameWnd
HeaderFile=IpFrame.h
ImplementationFile=IpFrame.cpp

[CLS:CLeftDockingBar]
Type=0
BaseClass=CIceDockingSingleView
HeaderFile=LeftDockingBar.h
ImplementationFile=LeftDockingBar.cpp

[CLS:CListEditCtrl]
Type=0
BaseClass=CEdit
HeaderFile=ListEditCtrl.h
ImplementationFile=ListEditCtrl.cpp

[CLS:CLogfileView]
Type=0
BaseClass=CRichEditView
HeaderFile=LogfileView.h
ImplementationFile=LogfileView.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CIceMDIFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:CMainToolBar]
Type=0
BaseClass=CIceToolBar
HeaderFile=MainToolBar.h
ImplementationFile=MainToolBar.cpp

[CLS:COutputView]
Type=0
BaseClass=CRichEditView
HeaderFile=OutputView.h
ImplementationFile=OutputView.cpp

[CLS:CPaletView]
Type=0
BaseClass=CFormView
HeaderFile=PaletView.h
ImplementationFile=PaletView.cpp

[CLS:CParseView]
Type=0
BaseClass=CRichEditView
HeaderFile=ParseView.h
ImplementationFile=ParseView.cpp

[CLS:CRightDockingBar]
Type=0
BaseClass=CIceDockingSingleView
HeaderFile=RightDockingBar.h
ImplementationFile=RightDockingBar.cpp

[CLS:CScriptView]
Type=0
BaseClass=CIceShellView
HeaderFile=ScriptView.h
ImplementationFile=ScriptView.cpp

[CLS:CTexDoc]
Type=0
BaseClass=COleServerDoc
HeaderFile=TexDoc.h
ImplementationFile=TexDoc.cpp

[CLS:CTexView]
Type=0
BaseClass=CIceRichEditView
HeaderFile=TexView.h
ImplementationFile=TexView.cpp

[CLS:CThinSplitterWnd]
Type=0
BaseClass=CIceSplitterWnd
HeaderFile=ThinSplitterWnd.h
ImplementationFile=ThinSplitterWnd.cpp

[CLS:CTreeMenuView]
Type=0
BaseClass=CIceTreeView
HeaderFile=TreeMenuView.h
ImplementationFile=TreeMenuView.cpp

[CLS:CXmlDoc]
Type=0
BaseClass=COleServerDoc
HeaderFile=XmlDoc.h
ImplementationFile=XmlDoc.cpp
LastObject=CXmlDoc

[CLS:CXmlView]
Type=0
BaseClass=CIceRichEditView
HeaderFile=XmlView.h
ImplementationFile=XmlView.cpp

[CLS:CXslDoc]
Type=0
BaseClass=COleServerDoc
HeaderFile=XslDoc.h
ImplementationFile=XslDoc.cpp
LastObject=CXslDoc

[CLS:CXslView]
Type=0
BaseClass=CIceRichEditView
HeaderFile=XslView.h
ImplementationFile=XslView.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg

[DLG:IDD_PALETVIEW]
Type=1
Class=CPaletView

[TB:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_SEND_MAIL
Command7=ID_FILE_PRINT
Command8=ID_FILE_PRINT_PREVIEW
Command9=ID_FILE_PRINT_SETUP
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_EDIT_PASTE_SPECIAL
Command15=ID_VIEW_WORKSPACE_PANE
Command16=ID_VIEW_PALETTE_PANE
Command17=ID_VIEW_OUTPUT_PANE
Command18=ID_WINDOW_NEW
Command19=ID_WINDOW_CASCADE
Command20=ID_WINDOW_TILE_HORZ
Command21=ID_WINDOW_TILE_VERT
Command22=ID_WINDOW_ARRANGE
Command23=ID_HELP_FINDER
Command24=ID_HELP
Command25=ID_APP_ABOUT
Command26=ID_CONTEXT_HELP
CommandCount=26

[TB:IDR_IMSTYPE_SRVR_IP (English (U.S.))]
Type=1
Class=?
Command1=ID_EDIT_CUT
Command2=ID_EDIT_COPY
Command3=ID_EDIT_PASTE
Command4=ID_APP_ABOUT
Command5=ID_CONTEXT_HELP
CommandCount=5

[TB:IDR_SYSTEMBAR (English (U.S.))]
Type=1
Class=?
CommandCount=0

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_PRINT_SETUP
Command4=ID_FILE_MRU_FILE1
Command5=ID_APP_EXIT
Command6=ID_VIEW_TOOLBAR
Command7=ID_VIEW_STATUS_BAR
Command8=ID_VIEW_WORKSPACE_PANE
Command9=ID_VIEW_PALETTE_PANE
Command10=ID_VIEW_OUTPUT_PANE
Command11=ID_HELP_FINDER
Command12=ID_APP_ABOUT
CommandCount=12

[MNU:IDR_XMLTYPE (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_SEND_MAIL
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_CUT
Command14=ID_EDIT_COPY
Command15=ID_EDIT_PASTE
Command16=ID_EDIT_PASTE_SPECIAL
Command17=ID_OLE_INSERT_NEW
Command18=ID_OLE_EDIT_LINKS
Command19=ID_OLE_VERB_FIRST
Command20=ID_VIEW_TOOLBAR
Command21=ID_VIEW_STATUS_BAR
Command22=ID_VIEW_WORKSPACE_PANE
Command23=ID_VIEW_PALETTE_PANE
Command24=ID_VIEW_OUTPUT_PANE
Command25=ID_VIEW_SOURCE
Command26=ID_WINDOW_NEW
Command27=ID_WINDOW_CASCADE
Command28=ID_WINDOW_TILE_HORZ
Command29=ID_WINDOW_TILE_VERT
Command30=ID_WINDOW_ARRANGE
Command31=ID_HELP_FINDER
Command32=ID_APP_ABOUT
CommandCount=32

[MNU:IDR_IMSTYPE_CNTR_IP (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_SEND_MAIL
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_WINDOW_NEW
Command13=ID_WINDOW_CASCADE
Command14=ID_WINDOW_TILE_HORZ
Command15=ID_WINDOW_ARRANGE
Command16=ID_WINDOW_TILE_VERT
CommandCount=16

[MNU:IDR_IMSTYPE_SRVR_EMB (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_UPDATE
Command5=ID_FILE_SAVE_COPY_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_SEND_MAIL
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_CUT
Command14=ID_EDIT_COPY
Command15=ID_EDIT_PASTE
Command16=ID_EDIT_PASTE_SPECIAL
Command17=ID_OLE_INSERT_NEW
Command18=ID_OLE_EDIT_LINKS
Command19=ID_OLE_VERB_FIRST
Command20=ID_VIEW_TOOLBAR
Command21=ID_VIEW_STATUS_BAR
Command22=ID_VIEW_WORKSPACE_PANE
Command23=ID_VIEW_PALETTE_PANE
Command24=ID_VIEW_OUTPUT_PANE
Command25=ID_WINDOW_NEW
Command26=ID_WINDOW_CASCADE
Command27=ID_WINDOW_TILE_HORZ
Command28=ID_WINDOW_TILE_VERT
Command29=ID_WINDOW_ARRANGE
Command30=ID_HELP_FINDER
Command31=ID_APP_ABOUT
CommandCount=31

[MNU:IDR_IMSTYPE_SRVR_IP (English (U.S.))]
Type=1
Class=?
Command1=ID_EDIT_UNDO
Command2=ID_EDIT_CUT
Command3=ID_EDIT_COPY
Command4=ID_EDIT_PASTE
Command5=ID_EDIT_PASTE_SPECIAL
Command6=ID_OLE_INSERT_NEW
Command7=ID_OLE_EDIT_LINKS
Command8=ID_OLE_VERB_FIRST
Command9=ID_VIEW_TOOLBAR
Command10=ID_HELP_FINDER
Command11=ID_APP_ABOUT
CommandCount=11

[MNU:IDR_TEXTYPE (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_SEND_MAIL
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_CUT
Command14=ID_EDIT_COPY
Command15=ID_EDIT_PASTE
Command16=ID_EDIT_PASTE_SPECIAL
Command17=ID_OLE_INSERT_NEW
Command18=ID_OLE_EDIT_LINKS
Command19=ID_OLE_VERB_FIRST
Command20=ID_VIEW_TOOLBAR
Command21=ID_VIEW_STATUS_BAR
Command22=ID_VIEW_WORKSPACE_PANE
Command23=ID_VIEW_PALETTE_PANE
Command24=ID_VIEW_OUTPUT_PANE
Command25=ID_WINDOW_NEW
Command26=ID_WINDOW_CASCADE
Command27=ID_WINDOW_TILE_HORZ
Command28=ID_WINDOW_TILE_VERT
Command29=ID_WINDOW_ARRANGE
Command30=ID_HELP_FINDER
Command31=ID_APP_ABOUT
CommandCount=31

[MNU:IDR_IMSTYPE (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_SEND_MAIL
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_CUT
Command14=ID_EDIT_COPY
Command15=ID_EDIT_PASTE
Command16=ID_EDIT_PASTE_SPECIAL
Command17=ID_OLE_INSERT_NEW
Command18=ID_OLE_EDIT_LINKS
Command19=ID_OLE_VERB_FIRST
Command20=ID_VIEW_TOOLBAR
Command21=ID_VIEW_STATUS_BAR
Command22=ID_VIEW_WORKSPACE_PANE
Command23=ID_VIEW_PALETTE_PANE
Command24=ID_VIEW_OUTPUT_PANE
Command25=ID_VIEW_SPLIT_PALETTE
Command26=ID_WINDOW_NEW
Command27=ID_WINDOW_CASCADE
Command28=ID_WINDOW_TILE_HORZ
Command29=ID_WINDOW_TILE_VERT
Command30=ID_WINDOW_ARRANGE
Command31=ID_HELP_FINDER
Command32=ID_APP_ABOUT
CommandCount=32

[MNU:IDR_XSLTYPE (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_SEND_MAIL
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_CUT
Command14=ID_EDIT_COPY
Command15=ID_EDIT_PASTE
Command16=ID_EDIT_PASTE_SPECIAL
Command17=ID_OLE_INSERT_NEW
Command18=ID_OLE_EDIT_LINKS
Command19=ID_OLE_VERB_FIRST
Command20=ID_VIEW_TOOLBAR
Command21=ID_VIEW_STATUS_BAR
Command22=ID_VIEW_WORKSPACE_PANE
Command23=ID_VIEW_PALETTE_PANE
Command24=ID_VIEW_OUTPUT_PANE
Command25=ID_VIEW_SOURCE
Command26=ID_WINDOW_NEW
Command27=ID_WINDOW_CASCADE
Command28=ID_WINDOW_TILE_HORZ
Command29=ID_WINDOW_TILE_VERT
Command30=ID_WINDOW_ARRANGE
Command31=ID_HELP_FINDER
Command32=ID_APP_ABOUT
CommandCount=32

[MNU:IDR_DTDTYPE (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE
Command5=ID_FILE_SAVE_AS
Command6=ID_FILE_PRINT
Command7=ID_FILE_PRINT_PREVIEW
Command8=ID_FILE_PRINT_SETUP
Command9=ID_FILE_SEND_MAIL
Command10=ID_FILE_MRU_FILE1
Command11=ID_APP_EXIT
Command12=ID_EDIT_UNDO
Command13=ID_EDIT_CUT
Command14=ID_EDIT_COPY
Command15=ID_EDIT_PASTE
Command16=ID_EDIT_PASTE_SPECIAL
Command17=ID_OLE_INSERT_NEW
Command18=ID_OLE_EDIT_LINKS
Command19=ID_OLE_VERB_FIRST
Command20=ID_VIEW_TOOLBAR
Command21=ID_VIEW_STATUS_BAR
Command22=ID_VIEW_WORKSPACE_PANE
Command23=ID_VIEW_PALETTE_PANE
Command24=ID_VIEW_OUTPUT_PANE
Command25=ID_VIEW_SOURCE
Command26=ID_WINDOW_NEW
Command27=ID_WINDOW_CASCADE
Command28=ID_WINDOW_TILE_HORZ
Command29=ID_WINDOW_TILE_VERT
Command30=ID_WINDOW_ARRANGE
Command31=ID_HELP_FINDER
Command32=ID_APP_ABOUT
CommandCount=32

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
Command15=ID_CONTEXT_HELP
Command16=ID_HELP
Command17=ID_CANCEL_EDIT_CNTR
CommandCount=17

[ACL:IDR_IMSTYPE_CNTR_IP (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_NEXT_PANE
Command6=ID_PREV_PANE
Command7=ID_CONTEXT_HELP
Command8=ID_HELP
Command9=ID_CANCEL_EDIT_CNTR
CommandCount=9

[ACL:IDR_IMSTYPE_SRVR_IP (English (U.S.))]
Type=1
Class=?
Command1=ID_EDIT_UNDO
Command2=ID_EDIT_CUT
Command3=ID_EDIT_COPY
Command4=ID_EDIT_PASTE
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_CONTEXT_HELP
Command10=ID_HELP
Command11=ID_CANCEL_EDIT_SRVR
CommandCount=11

[ACL:IDR_IMSTYPE_SRVR_EMB (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_UPDATE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
Command15=ID_CONTEXT_HELP
Command16=ID_HELP
Command17=ID_CANCEL_EDIT_CNTR
CommandCount=17

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_PALETVIEW (English (U.S.))]
Type=1
Class=?
ControlCount=2
Control1=IDC_PALETVIEW_LISTCTRL,SysListView32,1342177285
Control2=IDC_PALETVIEW_RICHEDIT,RICHEDIT,1345327300

[TB:IDR_MRUFILE (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_MRU_FILE1
Command2=ID_FILE_MRU_FILE2
Command3=ID_FILE_MRU_FILE3
Command4=ID_FILE_MRU_FILE4
Command5=ID_FILE_MRU_FILE5
Command6=ID_FILE_MRU_FILE1
Command7=ID_FILE_MRU_FILE7
Command8=ID_FILE_MRU_FILE8
Command9=ID_FILE_MRU_FILE1
Command10=ID_FILE_MRU_FILE10
CommandCount=10

[TB:IDR_RESERVOIR (English (U.S.))]
Type=1
Class=?
Command1=ID_APP_EXIT
Command2=ID_VIEW_SOURCE
CommandCount=2

[CLS:CIceParseView]
Type=0
HeaderFile=IceParseView.h
ImplementationFile=IceParseView.cpp
BaseClass=CIceListView
Filter=C
VirtualFilter=VWC
LastObject=CIceParseView

