// LeftDockingBar.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "LeftDockingBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLeftDockingBar

CLeftDockingBar::CLeftDockingBar()
{
    m_sizeMin = CSize(32, 32);
    m_sizeHorz = CSize(200, 200);
    m_sizeVert = CSize(200, 200);
    m_sizeFloat = CSize(200, 200);
    m_bTracking = FALSE;
    m_bInRecalcNC = FALSE;
    m_cxEdge = 5;
    m_bDragShowContent = FALSE;
}

CLeftDockingBar::~CLeftDockingBar()
{
}


BEGIN_MESSAGE_MAP(CLeftDockingBar, CIceDockingSingleView)
	//{{AFX_MSG_MAP(CLeftDockingBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
//
void CLeftDockingBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}

/////////////////////////////////////////////////////////////////////////////
// CLeftDockingBar message handlers

int CLeftDockingBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingSingleView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here

	/*
    if (!m_wndProjectMenu.Create(TVS_EDITLABELS,
		CRect(0,0,50,50),
        this, ID_PROJECT_MENU))
        return -1;

    TV_INSERTSTRUCT  tvi;
    tvi.hParent = NULL;
    tvi.hInsertAfter = TVI_LAST;
    tvi.item.mask = //TVIF_IMAGE | TVIF_SELECTEDIMAGE | 
		TVIF_TEXT | TVIF_STATE | TVIF_CHILDREN;
    tvi.item.stateMask = TVIS_EXPANDED;

    tvi.item.cchTextMax = 20;
    //tvi.item.iSelectedImage = 0;   // 0 ????
    tvi.item.cChildren = 1;
    tvi.item.lParam = 0;
    //tvi.item.iImage = 1;
    tvi.item.hItem = NULL;
    tvi.item.state = TVIS_EXPANDED;
    tvi.item.pszText = "High Dan";

    HTREEITEM hHighD = m_wndProjectMenu.InsertItem(&tvi);
	*/

	return 0;
}

void CLeftDockingBar::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingSingleView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here

	/*
    CRect rc;
    GetClientRect(rc);
    rc.DeflateRect(4, 4);
	m_wndProjectMenu.MoveWindow(rc);
	*/
}
