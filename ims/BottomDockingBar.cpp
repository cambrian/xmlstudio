// BottomDockingBar.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "BottomDockingBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBottomDockingBar

CBottomDockingBar::CBottomDockingBar()
{
    m_sizeMin = CSize(32, 32);
    m_sizeHorz = CSize(200, 200);
    m_sizeVert = CSize(200, 200);
    m_sizeFloat = CSize(200, 200);
    m_bTracking = FALSE;
    m_bInRecalcNC = FALSE;
    m_cxEdge = 5;
    m_bDragShowContent = FALSE;
}

CBottomDockingBar::~CBottomDockingBar()
{
}


BEGIN_MESSAGE_MAP(CBottomDockingBar, CIceDockingTabbedView)
	//{{AFX_MSG_MAP(CBottomDockingBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
//
void CBottomDockingBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	UpdateDialogControls(pTarget, bDisableIfNoHndler);
}


/////////////////////////////////////////////////////////////////////////////
// CBottomDockingBar message handlers

int CBottomDockingBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CIceDockingTabbedView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	
	return 0;
}

void CBottomDockingBar::OnSize(UINT nType, int cx, int cy) 
{
	CIceDockingTabbedView::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	
}
