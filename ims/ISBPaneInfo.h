#if !defined(AFX_ISBPANEINFO_H__74150182_553F_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ISBPANEINFO_H__74150182_553F_11D2_852E_00A024E0E339__INCLUDED_

#include <afxtempl.h>

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ISBPaneInfo.h : header file
//

#define XSB_TOP				DT_TOP		// Styles werden aus DrawText �bernommen.
#define XSB_LEFT			DT_LEFT		// Theoretisch k�nnen auch alle anderen
#define XSB_CENTER			DT_CENTER	// Format-Styles aus Draw-Text benutzt
#define XSB_RIGHT			DT_RIGHT	// werden. Diese werden direkt an die
#define XSB_VCENTER			DT_VCENTER	// jeweiligen Panes �bergeben. F�r Bitmaps
#define XSB_BOTTOM			DT_BOTTOM	// werden jedoch nur diese Styles benutzt.

#define XSB_LEER			0x00000000
#define XSB_TEXT			0x00100000	// Darstellung eines Textes im Pane
#define XSB_NUMBER			0x00200000	// Darstellung eines Zahlenwertes im Pane
#define XSB_BITMAP			0x00400000	// Darstellung eines Bitmaps im Pane
#define XSB_PROGRESS		0x00800000	// Darstellung eines Progress-Bars im Pane
#define XSB_HSCROLL			0x01000000	// Horizontales Scrollen des Textes bzw. Bitmaps
#define XSB_VSCROLL			0x02000000	// Vertikales Scrollen des Textes bzw. Bitmaps
#define XSB_DSCROLL			0x03000000	// Diagonales Scrollen des Textes bzw. Bitmaps
#define XSB_REPEAT			0x10000000	// Mehrfaches hinter- bzw. untereinandersetzen
#define XSB_STRETCH			0x20000000	// Bitmap an die Panegr��e anpassen
#define XSB_SMOOTH			0x40000000	// Progress-Bar smooth darstellen

#define XSB_ALIGN			0x000fffff
#define XSB_MODE			0x00f00000
#define XSB_SCROLL			0x0f000000
#define XSB_MISC			0xf0000000

/////////////////////////////////////////////////////////////////////////////
// ISBPaneInfo window

class ISBPaneInfo : public CObject
{
// Construction
public:
	ISBPaneInfo();
	ISBPaneInfo(const ISBPaneInfo& paneInfo);

// Attributes
public:
	ISBPaneInfo		operator=(const ISBPaneInfo& paneInfo);
	void			SetDefault();

	void			HScroll(CRect& rect, int maxWidth, int nullValue);
	void			VScroll(CRect& rect, int maxHeight, int nullValue);

protected:							// 0 = aus, 1 = ein
	COLORREF		fgColor[2];		// Textfarbe
	COLORREF		bkColor[2];		// Hintergrundfarbe
	CString			string[2];		// Text, Nummer oder Bitmapname
	CProgressCtrl	*progress;		// Fortschrittskontrolle

	LOGFONT			font;			// Schriftart
	int				mode;			// Darstellungsart
	int				hScrollPos;		// Position f�r horizontales Scrollen
	int				vScrollPos;		// Position f�r vertikales Scrollen



// Operations
public:
	void			SetFgColor(COLORREF newOnColor, COLORREF newOffColor = -1);
	void			SetBkColor(COLORREF newOnColor, COLORREF newOffColor = -1);
	void			SetBitmap(LPCSTR newOnBitmap, LPCSTR newOffBitmap = "");
	void			SetText(LPCSTR onText, LPCSTR offText = "");
	void			SetNumber(int onValue, int offValue = 0);
	void			SetMode(int newMode);
	void			SetFont(LOGFONT& newFont)		{ font		= newFont;					}
	void			SetFont(CFont& newFont)			{ newFont.GetLogFont(&font);			}
	void			SetFont(CFont *newFont)			{ newFont->GetLogFont(&font);			}
	void			SetFontSize(int size)			{ font.lfHeight = size;					}
	void			SetFontName(LPCSTR name)		{ strcpy(font.lfFaceName, name);		}
	void			SetFont(LPCSTR name, int size);

	int				Increment(bool on = true)		{ int n = atoi(string[on ? 1 : 0]); string[on ? 1 : 0].Format("%d", ++n); return n; }
	int				Decrement(bool on = true)		{ int n = atoi(string[on ? 1 : 0]); string[on ? 1 : 0].Format("%d", --n); return n; }
	COLORREF		GetFgColor(bool on = true)		{ return fgColor[on ? 1 : 0];			}
	COLORREF		GetBkColor(bool on = true)		{ return bkColor[on ? 1 : 0];			}
	CString			GetText(bool on = true)			{ return string[on ? 1 : 0];			}
	CString			GetBitmap(bool on = true)		{ return string[on ? 1 : 0];			}
	int				GetNumber(bool on = true)		{ return atoi(string[on ? 1 : 0]);		}

	CProgressCtrl	*GetProgressCtrl()				{ return progress;						}

	LOGFONT&		GetFont()						{ return font;							}
	CString			GetFontName()					{ return font.lfFaceName;				}
	int				GetFontSize()					{ return font.lfHeight;					}
	int				GetMode()						{ return mode;							}
	bool			IsHScroll()						{ return ((mode & XSB_HSCROLL) != 0);	}
	bool			IsVScroll()						{ return ((mode & XSB_VSCROLL) != 0);	}

	void			SetRange(int nLower, int nUpper){ if (progress)	progress->SetRange(nLower, nUpper);		}
	int				SetPos(int nPos)				{ return (progress ? progress->SetPos(nPos) : -1);		}
	int				OffsetPos(int nPos)				{ return (progress ? progress->OffsetPos(nPos) : -1);	}
	int				SetStep(int nStep)				{ return (progress ? progress->SetStep(nStep) : -1);	}
	int				StepIt()						{ return (progress ? progress->StepIt() : -1);			}


// Implementation
public:
	virtual ~ISBPaneInfo();


#ifdef _DEBUG
	void Dump(CDumpContext& dc) const;
	void AssertValid() const;
	friend CDumpContext& AFXAPI operator<<(CDumpContext& dc, ISBPaneInfo& b);
#endif //_DEBUG
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ISBPANEINFO_H__74150182_553F_11D2_852E_00A024E0E339__INCLUDED_)
