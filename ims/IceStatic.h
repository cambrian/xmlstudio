#if !defined(AFX_ICESTATIC_H__F8C3EA41_551C_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_ICESTATIC_H__F8C3EA41_551C_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceStatic.h : header file
//

#define IEVN_CAPTIONDRAG			WM_USER + 2
#define IEVN_CAPTIONDBLCLK			WM_USER + 3

/////////////////////////////////////////////////////////////////////////////
// CIceStatic window

class CIceStatic : public CStatic
{
// Construction
public:
	CIceStatic(LPCTSTR lpText = NULL, BOOL bDeleteOnDestroy=FALSE);

// Attributes
public:
	COLORREF		m_color;

protected:
	CFont			m_font;			
	BOOL			m_bDeleteOnDestroy;	// delete object when window destroyed?

// Operations
public:
	
	BOOL SubclassDlgItem(UINT nID, CWnd* pParent) {
		return CStatic::SubclassDlgItem(nID, pParent);
	}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceStatic)
	protected:
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceStatic();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceStatic)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ICESTATIC_H__F8C3EA41_551C_11D2_852E_00A024E0E339__INCLUDED_)
