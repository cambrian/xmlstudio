#if !defined(AFX_IceToolBar_H__90EFF501_129F_11D1_B8FC_444553540000__INCLUDED_)
#define AFX_IceToolBar_H__90EFF501_129F_11D1_B8FC_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// IceToolBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIceToolBar window

class CIceToolBar : public CToolBar
{
//    DECLARE_DYNAMIC(CIceToolBar)

// Construction
public:
	CIceToolBar();

// Attributes
public:

protected:
	int      m_nBarGripper;  // 0: no bars in gripper.  Valid values: 1 or 2

	BOOL     m_bTimer;
	int      m_nHitPrev;
	HPEN     m_hPenGray;
	HPEN     m_hPenWhite;
	HPEN     m_hPenDgray;
	HBRUSH   m_hBrushGray;
	HBRUSH   m_hBrushDgray;
	HBRUSH   m_hBrushWhite;
	HBRUSH   m_hBrushDither;
	CString  m_szResourceName;


// Operations
public:
	int      GetItemCount() { return (int)DefWindowProc(TB_BUTTONCOUNT, 0, 0);}
	void     DrawButton(HDC hDC, BOOL bHorz);
	void     DrawButtonBorder(HDC hDC, CRect rcBorder, int iStyle);

    
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIceToolBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CIceToolBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIceToolBar)
	afx_msg void OnPaint();
	afx_msg void OnNcPaint();
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp);
	afx_msg void OnWindowPosChanging(WINDOWPOS FAR* lpwndpos);
	afx_msg UINT OnNcHitTest(CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnSysColorChange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IceToolBar_H__90EFF501_129F_11D1_B8FC_444553540000__INCLUDED_)
