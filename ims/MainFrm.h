// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__D4AD6C8C_51ED_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_MAINFRM_H__D4AD6C8C_51ED_11D2_852E_00A024E0E339__INCLUDED_

#include "IceMDIFrameWnd.h"
#include "MainToolBar.h"
#include "DefaultStatusBar.h"
#include "LeftDockingBar.h"
#include "RightDockingBar.h"
#include "BottomDockingBar.h"
#include "ExplorerDockingBar.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class CMainFrame : public CIceMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void RecalcLayout(BOOL bNotify = TRUE);
	virtual void ActivateFrame(int nCmdShow = -1);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CDefaultStatusBar  m_wndDefaultStatusBar;
	CMainToolBar       m_wndMainToolBar;

	CExplorerDockingBar  m_wndExplorerDockBar;
	//CLeftDockingBar    m_wndLeftDockBar;
	CRightDockingBar     m_wndRightDockBar;
	CBottomDockingBar    m_wndBottomDockBar;


// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__D4AD6C8C_51ED_11D2_852E_00A024E0E339__INCLUDED_)
