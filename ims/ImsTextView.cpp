// ImsTextView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ImsTextView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImsTextView

IMPLEMENT_DYNCREATE(CImsTextView, CRichEditView)

CImsTextView::CImsTextView()
{
}

CImsTextView::~CImsTextView()
{
}

BEGIN_MESSAGE_MAP(CImsTextView, CRichEditView)
	//{{AFX_MSG_MAP(CImsTextView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImsTextView diagnostics

#ifdef _DEBUG
void CImsTextView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CImsTextView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CImsTextView message handlers
