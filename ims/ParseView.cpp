// ParseView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"
#include "ParseView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParseView

IMPLEMENT_DYNCREATE(CParseView, CRichEditView)

CParseView::CParseView()
{
}

CParseView::~CParseView()
{
}

BEGIN_MESSAGE_MAP(CParseView, CRichEditView)
	//{{AFX_MSG_MAP(CParseView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParseView diagnostics

#ifdef _DEBUG
void CParseView::AssertValid() const
{
	CRichEditView::AssertValid();
}

void CParseView::Dump(CDumpContext& dc) const
{
	CRichEditView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CParseView message handlers
