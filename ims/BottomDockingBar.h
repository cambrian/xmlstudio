#if !defined(AFX_BOTTOMDOCKINGBAR_H__56D8A944_521B_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_BOTTOMDOCKINGBAR_H__56D8A944_521B_11D2_852E_00A024E0E339__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// BottomDockingBar.h : header file
//

#include "IceDockingTabbedView.h"

/////////////////////////////////////////////////////////////////////////////
// CBottomDockingBar window

class CBottomDockingBar : public CIceDockingTabbedView
{
// Construction
public:
	CBottomDockingBar();

// Attributes
public:

// Operations
public:


// Overridables
    virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBottomDockingBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CBottomDockingBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CBottomDockingBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BOTTOMDOCKINGBAR_H__56D8A944_521B_11D2_852E_00A024E0E339__INCLUDED_)
