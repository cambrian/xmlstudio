// DtdView.cpp : implementation file
//

#include "stdafx.h"
#include "ims.h"

#include "DtdDoc.h"
#include "DtdView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDtdView

IMPLEMENT_DYNCREATE(CDtdView, CIceRichEditView)

CDtdView::CDtdView()
{
}

CDtdView::~CDtdView()
{
}

BEGIN_MESSAGE_MAP(CDtdView, CIceRichEditView)
	//{{AFX_MSG_MAP(CDtdView)
	ON_COMMAND(ID_VIEW_SOURCE, OnViewSource)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SOURCE, OnUpdateViewSource)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDtdView diagnostics

#ifdef _DEBUG
void CDtdView::AssertValid() const
{
	CIceRichEditView::AssertValid();
}

void CDtdView::Dump(CDumpContext& dc) const
{
	CIceRichEditView::Dump(dc);
}

CDtdDoc* CDtdView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDtdDoc)));
	return (CDtdDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CDtdView message handlers

void CDtdView::OnViewSource() 
{
	GetParentFrame()->DestroyWindow();
}

void CDtdView::OnUpdateViewSource(CCmdUI* pCmdUI) 
{
	CMenu* pMenu = pCmdUI->m_pMenu;
	if (pMenu != NULL)
    {
		CString  strMenu("Hide Source");
        pMenu->ModifyMenu(pCmdUI->m_nIndex, MF_BYPOSITION, ID_VIEW_SOURCE, strMenu);
	}

	pCmdUI->Enable(TRUE);
}

void CDtdView::OnDestroy() 
{
	CIceRichEditView::OnDestroy();
	
	// TODO: Add your message handler code here

	CDtdDoc* pDoc = GetDocument();
	if(pDoc)
		pDoc->m_bSourcePresent = FALSE;	
}
