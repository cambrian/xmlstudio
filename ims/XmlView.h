#if !defined(AFX_XMLVIEW_H__40CCB921_51FD_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_XMLVIEW_H__40CCB921_51FD_11D2_852E_00A024E0E339__INCLUDED_

#include "IceRichEditView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// XmlView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CXmlView view

class CXmlView : public CIceRichEditView
{
protected: // create from serialization only
	CXmlView();
	DECLARE_DYNCREATE(CXmlView)

// Attributes
public:
	CXmlDoc*  GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXmlView)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CXmlView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CXmlView)
	afx_msg void OnViewSource();
	afx_msg void OnUpdateViewSource(CCmdUI* pCmdUI);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CXmlDoc* CXmlView::GetDocument()
   { return (CXmlDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XMLVIEW_H__40CCB921_51FD_11D2_852E_00A024E0E339__INCLUDED_)
