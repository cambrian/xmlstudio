#if !defined(AFX_TEXVIEW_H__40CCB924_51FD_11D2_852E_00A024E0E339__INCLUDED_)
#define AFX_TEXVIEW_H__40CCB924_51FD_11D2_852E_00A024E0E339__INCLUDED_

#include "IceRichEditView.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// TexView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTexView view

class CTexView : public CIceRichEditView
{
protected: // create from serialization only
	CTexView();
	DECLARE_DYNCREATE(CTexView)

// Attributes
public:
	CTexDoc*  GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTexView)
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTexView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Generated message map functions
	//{{AFX_MSG(CTexView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in imsView.cpp
inline CTexDoc* CTexView::GetDocument()
   { return (CTexDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXVIEW_H__40CCB924_51FD_11D2_852E_00A024E0E339__INCLUDED_)
