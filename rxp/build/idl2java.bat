@echo off

java org.openorb.compiler.IdlCompiler %1 -I ..\idl\dom2 -noprefix -noskeleton -notie -d ..\idlgen\client
java org.openorb.compiler.IdlCompiler %1 -I ..\idl\dom2 -noprefix -notie -d ..\idlgen\server
