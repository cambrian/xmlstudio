@echo off

:// Set classpath.
REM .\setcp

:// Compile idl files.
echo Compiling idl files...
for %%i in (..\idl\dom2\*.idl) do call idl2java.bat %%i

REM set SOURCEPATH=..\idlgen\client;..\idlgen\server;..\java
set SOURCEPATH=..\java

:// Compile generated java files.
echo Compiling generated java files...
javac -sourcepath ..\idlgen\client;%SOURCEPATH% -classpath ..\classes\client;%CLASSPATH% -g -deprecation -d ..\classes\client ..\idlgen\client\dom\*.java
javac -sourcepath ..\idlgen\client;%SOURCEPATH% -classpath ..\classes\client;%CLASSPATH% -g -deprecation -d ..\classes\client ..\idlgen\client\dom\views\*.java
javac -sourcepath ..\idlgen\client;%SOURCEPATH% -classpath ..\classes\client;%CLASSPATH% -g -deprecation -d ..\classes\client ..\idlgen\client\dom\events\*.java
javac -sourcepath ..\idlgen\client;%SOURCEPATH% -classpath ..\classes\client;%CLASSPATH% -g -deprecation -d ..\classes\client ..\idlgen\client\dom\traversal\*.java
javac -sourcepath ..\idlgen\client;%SOURCEPATH% -classpath ..\classes\client;%CLASSPATH% -g -deprecation -d ..\classes\client ..\idlgen\client\dom\ranges\*.java
javac -sourcepath ..\idlgen\client;%SOURCEPATH% -classpath ..\classes\client;%CLASSPATH% -g -deprecation -d ..\classes\client ..\idlgen\client\dom\stylesheets\*.java
javac -sourcepath ..\idlgen\client;%SOURCEPATH% -classpath ..\classes\client;%CLASSPATH% -g -deprecation -d ..\classes\client ..\idlgen\client\dom\css\*.java

javac -sourcepath ..\idlgen\server;%SOURCEPATH% -classpath ..\classes\server;%CLASSPATH% -g -deprecation -d ..\classes\server ..\idlgen\server\dom\*.java
javac -sourcepath ..\idlgen\server;%SOURCEPATH% -classpath ..\classes\server;%CLASSPATH% -g -deprecation -d ..\classes\server ..\idlgen\server\dom\views\*.java
javac -sourcepath ..\idlgen\server;%SOURCEPATH% -classpath ..\classes\server;%CLASSPATH% -g -deprecation -d ..\classes\server ..\idlgen\server\dom\events\*.java
javac -sourcepath ..\idlgen\server;%SOURCEPATH% -classpath ..\classes\server;%CLASSPATH% -g -deprecation -d ..\classes\server ..\idlgen\server\dom\traversal\*.java
javac -sourcepath ..\idlgen\server;%SOURCEPATH% -classpath ..\classes\server;%CLASSPATH% -g -deprecation -d ..\classes\server ..\idlgen\server\dom\ranges\*.java
javac -sourcepath ..\idlgen\server;%SOURCEPATH% -classpath ..\classes\server;%CLASSPATH% -g -deprecation -d ..\classes\server ..\idlgen\server\dom\stylesheets\*.java
javac -sourcepath ..\idlgen\server;%SOURCEPATH% -classpath ..\classes\server;%CLASSPATH% -g -deprecation -d ..\classes\server ..\idlgen\server\dom\css\*.java


:// Compile dom interfaces.
echo Compiling dom interfaces...
javac -sourcepath %SOURCEPATH% -classpath %CLASSPATH% -g -deprecation -d ..\classes ..\java\org\w3c\dom\*.java
javac -sourcepath %SOURCEPATH% -classpath %CLASSPATH% -g -deprecation -d ..\classes ..\java\org\w3c\dom\views\*.java
javac -sourcepath %SOURCEPATH% -classpath %CLASSPATH% -g -deprecation -d ..\classes ..\java\org\w3c\dom\events\*.java
javac -sourcepath %SOURCEPATH% -classpath %CLASSPATH% -g -deprecation -d ..\classes ..\java\org\w3c\dom\traversal\*.java
javac -sourcepath %SOURCEPATH% -classpath %CLASSPATH% -g -deprecation -d ..\classes ..\java\org\w3c\dom\ranges\*.java
javac -sourcepath %SOURCEPATH% -classpath %CLASSPATH% -g -deprecation -d ..\classes ..\java\org\w3c\dom\stylesheets\*.java
javac -sourcepath %SOURCEPATH% -classpath %CLASSPATH% -g -deprecation -d ..\classes ..\java\org\w3c\dom\css\*.java


:// Compile source files.



:// Make jar file.
cd ..\classes
echo Making jar file...
del dom.jar
jar cvf dom.jar org\w3c\dom
del domclient.jar
cd client
jar cvf ..\domclient.jar dom
cd ..
del domserver.jar
cd server
jar cvf ..\domserver.jar dom
cd ..

:...


cd ..\build

