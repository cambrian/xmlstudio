@echo off

for %%i in (H:\openorb\OpenORB\lib\*.jar) do call addcp.bat %%i
for %%i in (H:\openorb\NamingService\lib\*.jar) do call addcp.bat %%i
for %%i in (H:\openorb\InterfaceRepository\lib\*.jar) do call addcp.bat %%i
set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib\tools.jar
REM set CLASSPATH=..\classes;..\classes\client;..\classes\server;%CLASSPATH%
set CLASSPATH=..\classes;%CLASSPATH%

REM echo CLASSPATH=%CLASSPATH%
REM echo.
