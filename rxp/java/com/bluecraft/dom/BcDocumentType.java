package com.bluecraft.dom;

import org.w3c.dom.*;

public class BcDocumentType extends _DocumentTypeImplBase {
    public BcDocumentType() {
         super();
    }

    public short[] name()
    {
    }
    public NamedNodeMap entities()
    {
    }
    public NamedNodeMap notations()
    {
    }

    // node
    public short[] nodeName()
    {
    }
    public short[] nodeValue()
    {
    }
    public void nodeValue(short[] arg)
    {
    }
    public short nodeType()
    {
    }
    public Node parentNode()
    {
    }
    public NodeList childNodes()
    {
    }
    public Node firstChild()
    {
    }
    public Node lastChild()
    {
    }
    public Node previousSibling()
    {
    }
    public Node nextSibling()
    {
    }
    public NamedNodeMap attributes()
    {
    }
    public Document ownerDocument()
    {
    }
    public Node insertBefore(Node newChild, Node refChild)
        throws DOMException
    {
    }
    public Node replaceChild(Node newChild, Node oldChild)
        throws DOMException
    {
    }
    public Node removeChild(Node oldChild)
        throws DOMException
    {
    }
    public Node appendChild(Node newChild)
        throws DOMException
    {
    }
    public boolean hasChildNodes()
    {
    }
    public Node cloneNode(boolean deep)
    {
    }
}
