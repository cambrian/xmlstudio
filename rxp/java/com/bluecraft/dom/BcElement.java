package com.bluecraft.dom;

import org.w3c.dom.*;

public class BcElement extends _ElementImplBase {
    // Constructor
    public BcElement() {
         super();
    }

    public short[] tagName() 
    {
    }
    public short[] getAttribute(short[] name)
    {
    }
    public void setAttribute(short[] name, short[] value)
        throws DOMException
    {
    }
    public void removeAttribute(short[] name)
        throws DOMException
    {
    }
    public Attr getAttributeNode(short[] name)
    {
    }
    public Attr setAttributeNode(Attr newAttr)
        throws DOMException
    {
    }
    public Attr removeAttributeNode(Attr oldAttr)
        throws DOMException
    {
    }
    public NodeList getElementsByTagName(short[] name)
    {
    }
    public void normalize()
    {
    }

    // Node
    public short[] nodeName()
    {
    }
    public short[] nodeValue()
    {
    }
    public void nodeValue(short[] arg)
    {
    }
    public short nodeType()
    {
    }
    public Node parentNode()
    {
    }
    public NodeList childNodes()
    {
    }
    public Node firstChild()
    {
    }
    public Node lastChild()
    {
    }
    public Node previousSibling()
    {
    }
    public Node nextSibling()
    {
    }
    public NamedNodeMap attributes()
    {
    }
    public Document ownerDocument()
    {
    }
    public Node insertBefore(Node newChild, Node refChild)
        throws DOMException
    {
    }
    public Node replaceChild(Node newChild, Node oldChild)
        throws DOMException
    {
    }
    public Node removeChild(Node oldChild)
        throws DOMException
    {
    }
    public Node appendChild(Node newChild)
        throws DOMException
    {
    }
    public boolean hasChildNodes()
    {
    }
    public Node cloneNode(boolean deep)
    {
    }
}
