package com.bluecraft.dom;

import org.w3c.dom.*;

public class BcCDATASection extends _CDATASectionImplBase {
    public BcCDATASection() {
         super();
    }

    // Text
    public Text splitText(int offset)
        throws DOMException
    {
    }

    // CharacterData
    public short[] data()
    {
    }
    public void data(short[] arg)
    {
    }
    public int length()
    {
    }
    public short[] substringData(int offset, int count)
        throws DOMException
    {
    }
    public void appendData(short[] arg)
        throws DOMException
    {
    }
    public void insertData(int offset, short[] arg)
        throws DOMException
    {
    }
    public void deleteData(int offset, int count)
        throws DOMException
    {
    }
    public void replaceData(int offset, int count, short[] arg)
        throws DOMException
    {
    }

    // Node
    public short[] nodeName()
    {
    }
    public short[] nodeValue()
    {
    }
    public void nodeValue(short[] arg)
    {
    }
    public short nodeType()
    {
    }
    public Node parentNode()
    {
    }
    public NodeList childNodes()
    {
    }
    public Node firstChild()
    {
    }
    public Node lastChild()
    {
    }
    public Node previousSibling()
    {
    }
    public Node nextSibling()
    {
    }
    public NamedNodeMap attributes()
    {
    }
    public Document ownerDocument()
    {
    }
    public Node insertBefore(Node newChild, Node refChild)
        throws DOMException
    {
    }
    public Node replaceChild(Node newChild, Node oldChild)
        throws DOMException
    {
    }
    public Node removeChild(Node oldChild)
        throws DOMException
    {
    }
    public Node appendChild(Node newChild)
        throws DOMException
    {
    }
    public boolean hasChildNodes()
    {
    }
    public Node cloneNode(boolean deep)
    {
    }
}
