package com.bluecraft.dom;

import org.w3c.dom.*;

public class BcEntity extends _EntityImplBase {
    public BcEntity() {
        super();
    }

    public short[] publicId()
    {
    }
    public short[] systemId()
    {
    }
    public short[] notationName()
    {
    }

    // Node
    public short[] nodeName()
    {
    }
    public short[] nodeValue()
    {
    }
    public void nodeValue(short[] arg)
    {
    }
    public short nodeType()
    {
    }
    public Node parentNode()
    {
    }
    public NodeList childNodes()
    {
    }
    public Node firstChild()
    {
    }
    public Node lastChild()
    {
    }
    public Node previousSibling()
    {
    }
    public Node nextSibling()
    {
    }
    public NamedNodeMap attributes()
    {
    }
    public Document ownerDocument()
    {
    }
    public Node insertBefore(Node newChild, Node refChild)
        throws DOMException
    {
    }
    public Node replaceChild(Node newChild, Node oldChild)
        throws DOMException
    {
    }
    public Node removeChild(Node oldChild)
        throws DOMException
    {
    }
    public Node appendChild(Node newChild)
        throws DOMException
    {
    }
    public boolean hasChildNodes()
    {
    }
    public Node cloneNode(boolean deep)
    {
    }
}
