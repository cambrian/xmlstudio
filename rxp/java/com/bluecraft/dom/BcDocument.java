package com.bluecraft.dom;

import org.w3c.dom.*;

public class BcDocument extends _DocumentImplBase {
    public BcDocument() {
         super();
    }

    public DocumentType doctype()
    {
    }
    public DOMImplementation implementation()
    {
    }
    public Element documentElement()
    {
    }
    public Element createElement(short[] tagName)
        throws DOMException
    {
    }
    public DocumentFragment createDocumentFragment()
    {
    }
    public Text createTextNode(short[] data)
    {
    }
    public Comment createComment(short[] data)
    {
    }
    public CDATASection createCDATASection(short[] data)
        throws DOMException
    {
    }
    public ProcessingInstruction createProcessingInstruction(short[] target, short[] data)
        throws DOMException
    {
    }
    public Attr createAttribute(short[] name)
        throws DOMException
    {
    }
    public EntityReference createEntityReference(short[] name)
        throws DOMException
    {
    }
    public NodeList getElementsByTagName(short[] tagname)
    {
    }

    // node
    public short[] nodeName()
    {
    }
    public short[] nodeValue()
    {
    }
    public void nodeValue(short[] arg)
    {
    }
    public short nodeType()
    {
    }
    public Node parentNode()
    {
    }
    public NodeList childNodes()
    {
    }
    public Node firstChild()
    {
    }
    public Node lastChild()
    {
    }
    public Node previousSibling()
    {
    }
    public Node nextSibling()
    {
    }
    public NamedNodeMap attributes()
    {
    }
    public Document ownerDocument()
    {
    }
    public Node insertBefore(Node newChild, Node refChild)
        throws DOMException
    {
    }
    public Node replaceChild(Node newChild, Node oldChild)
        throws DOMException
    {
    }
    public Node removeChild(Node oldChild)
        throws DOMException
    {
    }
    public Node appendChild(Node newChild)
        throws DOMException
    {
    }
    public boolean hasChildNodes()
    {
    }
    public Node cloneNode(boolean deep)
    {
    }
}
