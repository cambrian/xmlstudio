//
// shortsniffer.js
//

// These variables are global to the included page
var bMSIE = false;
var bMSIE3 = false, bMSIE4 = false, bMSIE4_beta = false, bMSIE4_01 = false, bMSIE5 = false, bMSIE5b1 = false, bMSIE5b2 = false;
var bNetscape = false;
var bNetscape_2 = false, bNetscape_3 = false, bNetscape_4 = false;

function browser(){
    var ua = navigator.userAgent;
    var an = navigator.appName;    // Is it IE?
    bMSIE = (ua.indexOf("MSIE")>=1);    
    if (bMSIE)    {        // IE3
        bMSIE3 = (ua.indexOf("MSIE 3.0")>=1);        // IE4
        var iMSIE4 = ua.indexOf("MSIE 4.0");
        bMSIE4 = (iMSIE4>=1);
        if (bMSIE4)        {
            var sMinorVer = ua.charAt(iMSIE4+8);
            // Some folks are still running an IE4 beta!
            // (the Mac IE team used a 'p' to mark their beta)
            bMSIE4_beta = bMSIE4 && ((sMinorVer == "b") || (sMinorVer == "p"));

            // IE4.01
            bMSIE4_01 = bMSIE4 && (sMinorVer == "1");
        }
        // IE5
        bMSIE5 = (ua.indexOf("MSIE 5.0")>=1);
        bMSIE5b1 = (ua.indexOf("MSIE 5.0b1")>=1);
        bMSIE5b2 = (ua.indexOf("MSIE 5.0b2")>=1);
    }
    else if (an == "Netscape")    {
        bNetscape = true;
        appVer = parseInt(navigator.appVersion);
        if (appVer >= 4)
            bNetscape_4 = true;
        else if (appVer >= 3)
            bNetscape_3 = true;
        else
            bNetscape_2 = true;
   }
}


// Call the browser() function before you call this one!
// These variables can be used globally by the included page.
var bWin16 = false; // includes Win31 and WinNT 3.51
var bWin95 = false;
var bWin98 = false;
var bWinNT = false;
var bMac = false, bMac68K = false, bMacPPC = false;
var bMSSun = false;
function os(){
   var ua = navigator.userAgent;
   if (bMSIE)   {
      //IE supported OS's
      bWin95 = (ua.indexOf("Windows 95")>=1);
      bWin98 = (ua.indexOf("Windows 98")>=1);
      bWinNT = (ua.indexOf("Windows NT")>=1);
      bWin16 = (ua.indexOf("Windows 3.1")>=1);
      bMac = (ua.indexOf("Mac")>=1);
      bMac68K = (ua.indexOf("Mac_68000")>=1);
      bMacPPC = (ua.indexOf("Mac_PowerPC")>=1);
      bMSSun = (ua.indexOf("SunOS")>=1)
   }
   else if (bNetscape)   {
      // NSCP supporte OS's
      bWin95 = (ua.indexOf("Win95")>=1);
      bWin98 = (ua.indexOf("Win98")>=1);
      bWinNT = (ua.indexOf("WinNT")>=1);
      bWin16 = (ua.indexOf("Win16")>=1);
      bMac = (ua.indexOf("Mac")>=1);
      if (bMac)      {
         bMac68K = (ua.indexOf("68K")>=1);
         bMacPPC = (ua.indexOf("PPC")>=1);
      }
      // BUGBUG: this doesn't find any of Netscape's Unix versions
   }
}

